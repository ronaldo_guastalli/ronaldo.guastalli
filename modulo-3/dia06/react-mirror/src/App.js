import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import ListaEpisodio from './models/listaEpisodio'

class App extends Component {
  constructor( props ){
    super( props )
    this.listaEpisodios = new ListaEpisodio()
    this.state = {
      episodio: this.listaEpisodios.episodioAleatorio
    }
  }

  sortear(){
    const episodio = this.listaEpisodios.episodioAleatorio
    this.setState({
      episodio
    })
  }

  marcarComoAssitido(){
    const { episodio } = this.state
    this.listaEpisodios.marcarComoAssitido( episodio )
    this.setState({
      episodio
    })
  }

  render() {

    const { episodio } = this.state

    return (
      <div className="App">
        <header className="App-header">
          <h2> { episodio.nome }</h2>
          <img src={ episodio.thumbUrl } alt={ episodio.nome }></img>
          <span>{ episodio.assistido ? 'sim' : 'não' }</span>
          <button onClick={ this.sortear.bind( this ) }>Próximo</button>
          <button onClick={ this.marcarComoAssitido.bind( this ) }>Assitido</button>
        </header>
      </div>
    );
  }
}

export default App;
