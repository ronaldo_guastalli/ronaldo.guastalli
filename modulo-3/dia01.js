var hello = "hello estou no arquivo dia01.js"

function somar(a, b){
    return (a + b)
}

function imprimirOi (){
    console.log("Oi")
}

//arrow function
var somarFn = (a,b) => a+b
var resultadoFn = somarFn(8,2)
console.log(resultadoFn)

//variavel aponta para função
var add = somar
var resultado2 = add(3,3)
console.log(resultado2)

var resultado = somar(1,2)
console.log(resultado)