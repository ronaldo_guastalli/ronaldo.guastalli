/* Ex 01 */
console.log('----Ex01----')
var inteiro = "0"
var fracao = "0"
var separador = {
  milhar: '.',
  decimal: ','
}

const valor0 = 0
const valor1 = 3498.99
const valor2 = 2313477.0135
const valor3 = -2313477.0135
const valor4 = -2313477.998
const valor5 = -2313477.99999

/* £ - separado fracao ponto "." 
        separdor inteiro virgula ","
*/
/* € - separado fracao ponto "." 
        separdor inteiro virgula ","
*/
/* R$ - separado fracao virgula "," 
        separdor inteiro ponto "."
        */
function imprimirGBT(valor) {
  cifra = formatoCifra.GBT
  return virgulaMilharPontoDecimal(valor)
}
function imprimirFR(valor) {
  cifra = formatoCifra.FR
  return pontoMilharVirgulaDecimal(valor)
}
function imprimirBRL(valor) {
  cifra = formatoCifra.BR
  return pontoMilharVirgulaDecimal(valor)
}



var transformarPara = {
  'GBT': virgulaMilharPontoDecimal,
  'BR': pontoMilharVirgulaDecimal,
  'FR': pontoMilharVirgulaDecimal
}

var formatoCifra = {
  'GBT': '£',
  'BR': 'R$',
  'FR': '€'
}

//dependendo dos separadores
var virgulaMilharPontoDecimal = function (valor) {
  separador = {
    milhar: ',',
    decimal: '.'
  }
  return substituirSeparadores(valor, separador)
}

var pontoMilharVirgulaDecimal = function (valor) {
  separador = {
    milhar: '.',
    decimal: ','
  }
  return substituirSeparadores(valor, separador)
}

var substituirSeparadores = function (valor, separador) {
  if (valor === 0) {
    return cifra + ` 0${separador.decimal}00`//" 0,00"
  } else if (valor < 0) {
    valor *= -1
    cifra = "-" + cifra //valores < 0
  }
  //Boolean(separador) true: verificar o separador
  if (separador.milhar && separador) {
    var quebraValor = valor.toString().split('.')
  }
  inteiro = quebraValor[0].toString()
  fracao = quebraValor[1].toString()
  var fracaoFormatada = valorFracao(fracao, inteiro)
  var inteiroFomatado = novoValor(inteiro, fracaoFormatada)
  var novoNum = inteiroFomatado
  return cifra + novoNum
}

let valorFracao = (valor, milhar) => {
  let parteDecimal = valor
  if (parteDecimal.length > 2) {
    parteDecimal = (parseInt((parseInt(parteDecimal) / 100) + 1)).toString()
    if (parseInt(parteDecimal) < 10) {
      fracao = "0" + parteDecimal
      parteDecimal = "0" + parteDecimal
    } else if (parseInt(parteDecimal) >= 100 || parseInt(parteDecimal) == 10) {
      inteiro = (parseInt(milhar) + 1).toString()
      fracao = "00"
      parteDecimal = "00"
    }
  }
  return parteDecimal
}


// let valorFracao = (valor, inteiro) => {
//     let fracao = valor
//     if (fracao.length > 2) {
//         fracao = (parseInt((parseInt(fracao) / 100) + 1)).toString()
//         if (parseInt(fracao) < 10) {
//             fracao = "0" + fracao
//         } else if (parseInt(fracao) >= 100) {
//             this.inteiro = (parseInt(inteiro) + 1).toString()
//             fracao = "00"
//         }
//     }
//     return fracao
// }

let novoValor = (inteiro, fracao) => {
  var novoValor = ""
  var count = 0
  for (var i = inteiro.length - 1; i >= 0; i--) {
    if (count === 3) {
      count = 0
      novoValor = inteiro.charAt(i) + `${separador.milhar}` + novoValor//novoValor = inteiro.charAt(i) + "." + novoValor
    } else {
      novoValor = inteiro.charAt(i) + novoValor
    }
    count++
  }
  novoValor += `${separador.decimal}` + fracao //"," + fracao
  return novoValor
}

/*
    Testes
*/
console.log('####Brasil Testes####')
console.log(`${valor0} ---> ${imprimirBRL(valor0)}`)
console.log(`${valor1} ---> ${imprimirBRL(valor1)}`)
console.log(`${valor2} ---> ${imprimirBRL(valor2)}`)
console.log(`${valor3} ---> ${imprimirBRL(valor3)}`)
console.log(`${valor4} ---> ${imprimirBRL(valor4)}`)
console.log(`${valor5} ---> ${imprimirBRL(valor5)}`)

console.log('####Reino Unido Testes####')
console.log(`${valor0} ---> ${imprimirGBT(valor0)}`)
console.log(`${valor1} ---> ${imprimirGBT(valor1)}`)
console.log(`${valor2} ---> ${imprimirGBT(valor2)}`)
console.log(`${valor3} ---> ${imprimirGBT(valor3)}`)
console.log(`${valor4} ---> ${imprimirGBT(valor4)}`)
console.log(`${valor5} ---> ${imprimirGBT(valor5)}`)

console.log('####França Testes###')
console.log(`${valor0} ---> ${imprimirFR(valor0)}`)
console.log(`${valor1} ---> ${imprimirFR(valor1)}`)
console.log(`${valor2} ---> ${imprimirFR(valor2)}`)
console.log(`${valor3} ---> ${imprimirFR(valor3)}`)
console.log(`${valor4} ---> ${imprimirFR(valor4)}`)
console.log(`${valor5} ---> ${imprimirFR(valor5)}`)