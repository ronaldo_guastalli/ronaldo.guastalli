moment.locale('pt-BR')
var clicou = true
var intervalID
atualizar()

function atualizar() {
  if (clicou) {
    intervalID = window.setInterval(horario, 1000)
    horario()
    clicou = false
  } else {
    stop()
  }
}

function stop() {
  clearInterval(intervalID);
  clicou = true
}

function horario() {
  [
    'libano',
    'alemanha',
    'finlandia',
    'grecia',
    'japao'
  ].forEach( paisLabel => {
    atualizarTextoRelogio(paisLabel, atualizarRelogio(0, paisLabel))
  } )

}

function atualizarRelogio(valor, elementoPais) {
  const timezone = getTimezoneByLabel(elementoPais)
  if(valor === 0){
    return formatarMoment(timezone, ('HH:mm:ss Z'))
  }else if (valor === 1){
    stop()
    atualizarTextoRelogio(elementoPais, formatarMoment(timezone, 'LLL'))
  }
}

function atualizarTextoRelogio(elementoPais, texto) {
  const divPais = document.getElementById(elementoPais)
  const textoRelogio = divPais.querySelector('.card-title')
  textoRelogio.innerHTML = texto
}

function formatarMoment(timezone, formato) {
  return moment().tz(timezone).format(formato)
}

function getTimezoneByLabel(label) {
  return {
    'libano': 'Asia/Beirut',
    'japao': 'Asia/Tokyo',
    'grecia': 'Europe/Athens',
    'finlandia': 'Europe/Helsinki',
    'alemanha': 'Europe/Berlin'
  }[ label ]
}