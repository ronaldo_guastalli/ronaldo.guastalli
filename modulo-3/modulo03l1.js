// Exercicio 1
console.log("Ex01:")
var objeto1 = {
    raio: 10,
    tipoCalculo: "B"
}
function calculaCirculo(obj){
    if (obj.tipoCalculo === "A") {
        return (Math.PI * Math.pow(obj.raio, 2))
    }else if(obj.tipoCalculo === "C"){
        return 2*Math.PI*obj.raio
    }
    return null
}
console.log(calculaCirculo(objeto1))

// Exercicio 2
console.log("Ex02:")
var objeto2 = [2016, 2017, 2100]

function naoBissexto(num){
    var resultado = num % 4
    if (resultado === 0) {
        return false
    }else{
        return true
    }
}

objeto2.forEach(elemento => { 
    console.log(naoBissexto(elemento))
});

Object.keys(objeto2).forEach(function(key) {
    console.log(naoBissexto(key));
});

// Exercicio 03
// somarPares( [ 1, 56, 4.34, 6, -2 ] ) // 3.34
console.log("----Exe03----")
var arrayNum = [ 1, 56, 4.34, 6, -2 ] 

function somarPares(params) {
    var soma = 0.0
    for (var key in params) {
        if ( key % 2 === 0){
            soma += params[key];
        }
    }
    return soma
}

console.log(somarPares(arrayNum))

// Exercicio 04
console.log("----Ex04----")
//adicionar(3)(4) // 7
//adicionar(5642)(8749) // 14391
function adicionar(num1){
    function adicionar2(num2){
        var soma = num1 + num2
        return soma
    }
    return adicionar2
}
//var adicionar = op1 => op2 => op1 + op2
console.log(adicionar(3)(4))
console.log(adicionar(5642)(8749))

// Exercicio 05
console.log("----Ex05----")
// arredondamento para cima
var valor0 = 0
var valor1 = 3498.99
var valor2 = 2313477.0135
var valor3 = -2313477.0135
var valor4 = -2313477.998
var valor5 = -2313477.99999

let valorFracao = (valor, inteiro) => {
    var fracao = valor.toString().split(".",valor)[1]
    if(fracao.toString().length > 2){
        fracao = (parseInt((parseInt(fracao) / 100) + 1)).toString()
        if(parseInt(fracao) < 10){
            fracao = "0" + fracao
        }else if( parseInt(fracao) >= 100){
            inteiro = (parseInt(inteiro)+1).toString()
            fracao = "00"
        }
    }

    return fracao
}

let novoValor = (inteiro, fracao) => {
    var novoValor = ""
    var count = 0
    for(var i = inteiro.length - 1 ; i >=0 ; i-- ){
        if(count === 3){
            count = 0
            novoValor = inteiro.charAt(i) + "." + novoValor
        }else{
            novoValor = inteiro.charAt(i) + novoValor
        }
        count++
    }        
    novoValor += "," + fracao
    return novoValor
}

function imprimirBRL(valor){
    var cifra = "R$ "
    if(valor === 0){
        return "R$ 0,00"
    }else if(valor < 0){
        valor *= -1
        cifra = "-R$ "
    }
    var inteiro = parseInt(valor).toString()
    var fracao = valorFracao(valor, inteiro)
    // if(typeof valor === 'number'){
    //     valor = valor.toFixed(2)
    //     num = 'R$ ' + valor.toLocaleString('de-DE')
    // }
    return cifra + novoValor(inteiro, fracao)
}

console.log(imprimirBRL(valor0))
console.log(imprimirBRL(valor1))
console.log(imprimirBRL(valor2))
console.log(imprimirBRL(valor3))
console.log(imprimirBRL(valor4))
console.log(imprimirBRL(valor5))