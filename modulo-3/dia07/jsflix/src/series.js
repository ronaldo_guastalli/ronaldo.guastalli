var series = JSON.parse('[{"titulo":"Stranger Things","anoEstreia":2016,"diretor":["Matt Duffer","Ross Duffer"],"genero":["Suspense","Ficcao Cientifica","Drama"],"elenco":["Winona Ryder","David Harbour","Finn Wolfhard","Millie Bobby Brown","Gaten Matarazzo","Caleb McLaughlin","Natalia Dyer","Charlie Heaton","Cara Buono","Matthew Modine","Noah Schnapp"],"temporadas":2,"numeroEpisodios":17,"distribuidora":"Netflix"},{"titulo":"Game Of Thrones","anoEstreia":2011,"diretor":["David Benioff","D. B. Weiss","Carolyn Strauss","Frank Doelger","Bernadette Caulfield","George R. R. Martin"],"genero":["Fantasia","Drama"],"elenco":["Peter Dinklage","Nikolaj Coster-Waldau","Lena Headey","Emilia Clarke","Kit Harington","Aidan Gillen","Iain Glen ","Sophie Turner","Maisie Williams","Alfie Allen","Isaac Hempstead Wright"],"temporadas":7,"numeroEpisodios":67,"distribuidora":"HBO"},{"titulo":"The Walking Dead","anoEstreia":2010,"diretor":["Jolly Dale","Caleb Womble","Paul Gadd","Heather Bellson"],"genero":["Terror","Suspense","Apocalipse Zumbi"],"elenco":["Andrew Lincoln","Jon Bernthal","Sarah Wayne Callies","Laurie Holden","Jeffrey DeMunn","Steven Yeun","Chandler Riggs ","Norman Reedus","Lauren Cohan","Danai Gurira","Michael Rooker ","David Morrissey"],"temporadas":9,"numeroEpisodios":122,"distribuidora":"AMC"},{"titulo":"Band of Brothers","anoEstreia":20001,"diretor":["Steven Spielberg","Tom Hanks","Preston Smith","Erik Jendresen","Stephen E. Ambrose"],"genero":["Guerra"],"elenco":["Damian Lewis","Donnie Wahlberg","Ron Livingston","Matthew Settle","Neal McDonough"],"temporadas":1,"numeroEpisodios":10,"distribuidora":"HBO"},{"titulo":"The JS Mirror","anoEstreia":2017,"diretor":["Lisandro","Jaime","Edgar"],"genero":["Terror","Caos","JavaScript"],"elenco":["Daniela Amaral da Rosa","Antônio Affonso Vidal Pereira da Rosa","Gustavo Lodi Vidaletti","Bruno Artêmio Johann Dos Santos","Márlon Silva da Silva","Izabella Balconi de Moura","Diovane Mendes Mattos","Luciano Maciel Figueiró","Igor Ceriotti Zilio","Alexandra Peres","Vitor Emanuel da Silva Rodrigues","Raphael Luiz Lacerda","Guilherme Flores Borges","Ronaldo José Guastalli","Vinícius Marques Pulgatti"],"temporadas":1,"numeroEpisodios":40,"distribuidora":"DBC"},{"titulo":"10 Days Why","anoEstreia":2010,"diretor":["Brendan Eich"],"genero":["Caos","JavaScript"],"elenco":["Brendan Eich","Bernardo Bosak"],"temporadas":10,"numeroEpisodios":10,"distribuidora":"JS"},{"titulo":"Mr. Robot","anoEstreia":2018,"diretor":["Sam Esmail"],"genero":["Drama","Techno Thriller","Psychological Thriller"],"elenco":["Rami Malek","Carly Chaikin","Portia Doubleday","Martin Wallström","Christian Slater"],"temporadas":3,"numeroEpisodios":32,"distribuidora":"USA Network"},{"titulo":"Narcos","anoEstreia":2015,"diretor":["Paul Eckstein","Mariano Carranco","Tim King","Lorenzo O Brien"],"genero":["Documentario","Crime","Drama"],"elenco":["Wagner Moura","Boyd Holbrook","Pedro Pascal","Joann Christie","Mauricie Compte","André Mattos","Roberto Urbina","Diego Cataño","Jorge A. Jiménez","Paulina Gaitán","Paulina Garcia"],"temporadas":3,"numeroEpisodios":30,"distribuidora":null},{"titulo":"Westworld","anoEstreia":2016,"diretor":["Athena Wickham"],"genero":["Ficcao Cientifica","Drama","Thriller","Acao","Aventura","Faroeste"],"elenco":["Anthony I. Hopkins","Thandie N. Newton","Jeffrey S. Wright","James T. Marsden","Ben I. Barnes","Ingrid N. Bolso Berdal","Clifton T. Collins Jr.","Luke O. Hemsworth"],"temporadas":2,"numeroEpisodios":20,"distribuidora":"HBO"},{"titulo":"Breaking Bad","anoEstreia":2008,"diretor":["Vince Gilligan","Michelle MacLaren","Adam Bernstein","Colin Bucksey","Michael Slovis","Peter Gould"],"genero":["Acao","Suspense","Drama","Crime","Humor Negro"],"elenco":["Bryan Cranston","Anna Gunn","Aaron Paul","Dean Norris","Betsy Brandt","RJ Mitte"],"temporadas":5,"numeroEpisodios":62,"distribuidora":"AMC"}]')
console.log(series)


//{"titulo":"Stranger Things","anoEstreia":2016,"diretor":["Matt Duffer","Ross Duffer"],"genero":["Suspense","Ficcao Cientifica","Drama"],"elenco":["Winona Ryder","David Harbour","Finn Wolfhard","Millie Bobby Brown","Gaten Matarazzo","Caleb McLaughlin","Natalia Dyer","Charlie Heaton","Cara Buono","Matthew Modine","Noah Schnapp"],"temporadas":2,"numeroEpisodios":17,"distribuidora":"Netflix"}

//Object.values(series[7]).some(k => k ===null || typeof k ==== 'undefined')

//[3, 4, 3].reduce((acc, elem)=> acc + elem)

Array.prototype.seriesInvalidas = function () {
  const invalidas = this.filter(serie => {
    const algumCampoInvalido = Object.value(serie)
      .some(v => v === null || typeof v === 'undefined')
    const anoEstreiInvalido = serie.anoEstreia > new Date().getFullYear()
    return algumCampoInvalido || anoEstreiInvalido
  })
  return `Series Invalidas: ${invalidas.map(s => s.titulo.join('-'))}`
}

var implementaAlgumaCoisa = function () {
  let resposta = []
  series.forEach(element => { element.titulo === null ? resposta.push(element.titulo) : false })
  series.forEach(element => { element.anoEstreia === null ? resposta.push(element.titulo) : false })
  series.forEach(element => { element.diretor === null ? resposta.push(element.titulo) : false })
  series.forEach(element => { element.genero === null ? resposta.push(element.titulo) : false })
  series.forEach(element => { element.elenco === null ? resposta.push(element.titulo) : false })
  series.forEach(element => { element.temporadas === null ? resposta.push(element.titulo) : false })
  series.forEach(element => { element.numeroEpisodios === null ? resposta.push(element.titulo) : false })
  series.forEach(element => { element.distribuidora === null ? resposta.push(element.titulo) : false })
  isInvalidaPorAno()
  function isInvalidaPorAno() {
    anoAtual = 2019
    series.forEach(e => {
      if (e.anoEstreia > anoAtual) {
        resposta.push(e.titulo)
      }
    })
  }
  var resp = "Séries Inválidas: ".concat(resposta)
  return resp
}
Array.prototype.invalidas = implementaAlgumaCoisa
console.log(series.invalidas())



var funcFiltrarPorAno = function (ano) {
  let resposta = []
  series.forEach(e => {
    if (e.anoEstreia >= ano) {
      resposta.push(e.titulo)
    }
  })
  return resposta
}
Array.prototype.filtrarPorAno = funcFiltrarPorAno
console.log(series.filtrarPorAno(2017))



var funcProcurarPorNome = function (nome) {
  const nomeEncontrado = series.forEach(e => {
    e.elenco.find(e => e === nome)
  })
  return nomeEncontrado !== 'undefined' ? true : false
}
Array.prototype.procurarPorNome = funcProcurarPorNome
console.log(series.procurarPorNome("Ronaldo"))

//series.map(s => s.numeroEpisodios).reduce((acc, elem) => acc + elem.numeroEpisodios, 0) / series.length
var funcMediaDeEpisodios = function () {
 return this.map(s => s.numeroEpisodios).reduce((acc, elem) => acc + elem, 0) / this.length
  // let soma = 0.0
  // let qtdSeriesNoArray = 0
  // this.map(e => {
  //   soma += parseInt(e.numeroEpisodios)
  //   qtdSeriesNoArray++
  // })
  // return media = soma / qtdSeriesNoArray
}
Array.prototype.mediaDeEpisodios = funcMediaDeEpisodios
console.log(`Média dos episodios: ${series.mediaDeEpisodios()}`)

//scores.sort((a, b) => a-b)
//series.sort((a, b) => a.titulo.localeCompare(b.titulo))

var funcTotalSalarios = function (index) {
  const salarioDiretores = 100000
  const salarioOperarios = 40000
  let contaDiretores = 0
  let contaOperarios = 0
  let salarios = 0.0
  let selecionado = series[index]
  contaDiretores = selecionado.diretor.length
  contaOperarios = selecionado.elenco.length
  salarios = (contaDiretores * salarioDiretores) + (contaOperarios * salarioOperarios)
  return salarios
}
Array.prototype.totalSalarios = funcTotalSalarios
console.log(`Valor gasto na serie na posição: ${series.totalSalarios(0)}`)


//"genero":["Suspense","Ficcao Cientifica","Drama"]
Array.prototype.queroGenero = function (genero) {
  let selecao = []
  series.forEach(e => {
    e.genero.find(f => f === genero ? selecao.push(e.titulo) : false)
  })
  return selecao
}
console.log(series.queroGenero("Caos"))

//"titulo":"Stranger Things"
Array.prototype.queroTitulo = function (titulo) {
  let selecao = []
  series.map(element => {
    let a = element.titulo.split(" ")
    a.find(i => i === titulo ? selecao.push(element.titulo) : false)
  })
  return selecao
}
console.log(series.queroTitulo("The"))


Array.prototype.hashtagSecreta = function () {
  //   series.elenco.map(e => {
  //   if(e.split(" ") > 2){
  //     e.map(f => {
  //       for(let i = 1; i< f.length-1; i++){
  //         return f[i].split(" ")[0].join(".")
  //       }
  //     })
  //     return false
  //   }
  // })
}
console.log(series.hashtagSecreta())