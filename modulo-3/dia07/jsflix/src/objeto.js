class ElementoSerie {
  constructor(serie) {
    this.titulo = serie.titulo
    this.anoEstreia = serie.anoEstreia
    this.diretor = serie.diretor//arr
    this.genero = serie.genero //arr
    this.elenco = serie.elenco//arr
    this.temporadas = serie.temporadas//numeric
    this.numeroEpisodios = serie.numeroEpisodios//numeric
    this.distribuidora = serie.distribuidora
  }

  isInvalidaPorAno(anoAtual) {
    if(this.anoEstreia > anoAtual){
      return this.titulo
    }
  }

  isInvalidaPorCampoNullOuUndefined(){
    this.diretor.forEach(element =>{
      if(element === null || element === undefined){
        return this.titulo
      }
    })
  }

}