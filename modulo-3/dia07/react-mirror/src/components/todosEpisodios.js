import React, {Component} from 'react'
import EpisodiosApi from '../models/episodiosApi'
import './telaDetalheEpisodio.css'
import ListaEpisodiosUi from './shared/listaEpisodiosUi';

export default class TodosEpisodios extends Component{
  render(){
    const {listaEpisodios} = this.props.location.state
    return (
      <React.Fragment>
        <h1>Todos episódios</h1>
        <ListaEpisodiosUi listaEpisodios={listaEpisodios} />
      </React.Fragment>
    )
  }
}