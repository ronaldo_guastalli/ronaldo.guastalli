import Nota from "../models/nota";

export default class ListaNotas {
    constructor(notasDoServidor) {
        this.todasAsNotas = notasDoServidor.map(e => new Nota(e.nota, e.episodioId, e.id)) 
      }      

      get notas() {
        return this.todasAsNotas
      } 
      
           
    }