import React, { Component } from 'react'
import MeuCard from './shared/meuCard'
import EpisodiosApi from '../models/episodiosApi'
import './telaDetalheEpisodio.css'

export default class TelaDetalhesEpisodio extends Component {
  constructor(props) {
    super(props)
    this.episodiosApi = new EpisodiosApi()
    this.state = {
      detalhes: {},
      id: ""
    }

  }

  componentDidMount() {
    this.setState({
      id: this.props.match.params
    })

    this.episodiosApi.obterEpisodioDetalhes(this.props.location.state.episodio.id)
      .then(episodiosDoServidor => {
        this.setState({
          detalhes: episodiosDoServidor.data
        })
      })
  }

  formatarData(){

  }
  render() {
    const { detalhes } = this.state
    const { episodio, nota } = this.props.location.state
    return (
      <React.Fragment>
        <div className="texto-detalhe">
          <MeuCard
            tituloDoCard={episodio.nome}
            imagemPath={episodio.thumbUrl}
            conteudo={
              `Temporada: ${episodio.temporada}/${episodio.ordemEpisodio} 
              Duração: ${episodio.duracao}min 
              Sinopse: ${detalhes.sinopse} 
              Data estréia: ${new Date(detalhes.dataEstreia).toLocaleDateString()}
              Minha nota: ${nota} 
              Nota IMDB: ${detalhes.notaImdb/2}`
            }
          ></MeuCard>
        </div>
      </React.Fragment>
    )
  }

}


