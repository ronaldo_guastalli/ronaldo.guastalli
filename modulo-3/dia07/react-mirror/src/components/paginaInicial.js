import React, { Component } from 'react'

import ListaEpisodios from '../models/listaEpisodios'
import EpisodiosApi from '../models/episodiosApi'

import EpisodioUi from './episodioUi';

import MensagemFlash from '../components/shared/mensagemFlash'
import MeuInputNumero from '../components/shared/meuInputNumero';
import MeuBotao from '../components/shared/meuBotao';
import ListaNotas from '../components/listaNotas'

//TODO: menssagens.js e PaginaInicial.css

export default class PaginaInicial extends Component {
  constructor(props) {
    super(props)
    this.episodiosApi = new EpisodiosApi()
    // this.episodiosApi.obterNotas()
    //   .then(notas => {
    //   this.listaNotas = new ListaNotas(notas.data)})
     
    this.state = {
      //episodio: this.listaEpisodios.episodioAleatorio,
      deveExibirCampoNaTela: false,
      deveExibirMensagemNaTela: false,
      mensagem: '',
      corDaMensagem: '',
      segundos: 3,
      deveExibirErro: false,
      visivel: false
    }
  }

  componentDidMount() {
    const requisicoes = [this.episodiosApi.buscar(), this.episodiosApi.obterNotas()]

    Promise.all(requisicoes).then(resposta => {
      const episodiosDoServidor = resposta[0]
      const notas = resposta[1].data
      this.listaEpisodios = new ListaEpisodios(episodiosDoServidor.data) 
      this.listaNotas = new ListaNotas(notas)
      this.setState({
        episodio: this.listaEpisodios.episodioAleatorio
      })
    })
    // this.episodiosApi.buscar()
    //   .then(episodiosDoServidor => {
    //     this.listaEpisodios = new ListaEpisodios(episodiosDoServidor.data)
    //     this.setState({ episodio: this.listaEpisodios.episodioAleatorio })
    //   })
  }

  sortear = () => {
    const episodio = this.listaEpisodios.episodioAleatorio
    this.setState({
      episodio,
      visivel: false
    })
  }

  marcarComoAssitido = () => {
    const { episodio } = this.state
    this.listaEpisodios.marcarComoAssitido(episodio)

    this.setState({
      episodio,
      visivel: true
    })
  }

  validarCampoDeNotas = (evt) => {
    var valorInput = parseInt(evt.target.value)
    if ((valorInput >= 0) && (valorInput <= 5)) {
      this.registrarNota(valorInput)
      this.setState({
        mensagem: 'Registramos sua nota!',
        corDaMensagem: 'verde',
        deveExibirMensagemNaTela: true,
        segundos: 3,
        deveExibirErro: false,
        visivel: false
      })
    } else {
      this.setState({
        mensagem: 'Informar uma nota válida (entre 1 e 5)',
        deveExibirMensagemNaTela: true,
        corDaMensagem: 'vermelho',
        segundos: 5,
        visivel: true
      })
    }
  }

  registrarNota = (valorInput) => {
    const { episodio } = this.state
    episodio.avaliar(valorInput)
    this.setState({
      visivel: false,
    })
  }

  atualizarMensagem = devoExibir => {
    this.setState({
      deveExibirMensagemNaTela: devoExibir, //msg do filho
    })
  }

  atualizarErro = deveExibirErro => {
    this.setState({
      deveExibirErro //msg do filho
    })
  }

  inputQuerValidarCampo = evt => {
    this.validarCampoDeNotas(evt)
  }

  render() {
    const { episodio, deveExibirMensagemNaTela, mensagem, deveExibirErro,
      corDaMensagem, segundos, visivel} = this.state
    const { listaEpisodios, listaNotas} = this
    return (
      !episodio ? (
        <h3>Aguarde...</h3>
      ) : (
          <React.Fragment>
            <MensagemFlash
              atualizarMensagem={this.atualizarMensagem}
              deveExibirMensagemNaTela={deveExibirMensagemNaTela}
              mensagem={mensagem}
              color={corDaMensagem}
              segundos={segundos}
            />
            <header className="App-header">
              <EpisodioUi episodio={episodio} notas={{ listaNotas }} />
              <MeuInputNumero
                placeholder="1 a 5"
                mensagemCampo={'Qual a sua nota para este episódio?'}
                obrigatorio={true}
                atualizarErro={this.atualizarErro}
                deveExibirErro={deveExibirErro}
                visivel={visivel} //{episodio && episodio.assistido || false}
                //visivel={episodio.assistido}
                inputQuerValidarCampo={this.inputQuerValidarCampo}
              />
              <div className="botoes">
                <MeuBotao
                  textoBotao={'Próximo'}
                  corBtn={'verde'}
                  aoClicar={this.sortear} />
                <MeuBotao
                  textoBotao={'Assistido'}
                  corBtn={'azul'}
                  aoClicar={this.marcarComoAssitido} />
                <MeuBotao
                  textoBotao={'Ver notas'}
                  corBtn={'vermelho'}
                  linkTo='/avaliacoes'
                  dadosNavegacao={{ listaEpisodios ,listaNotas}}/>
                <MeuBotao
                  textoBotao={'Todos episódios'}
                  corBtn={'azul'}
                  linkTo='/todos'
                  dadosNavegacao={{listaEpisodios}}/>
              </div>
            </header>
          </React.Fragment>
        )
    )
  }
}
