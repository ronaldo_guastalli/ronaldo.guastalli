import React, { Component } from 'react'

import MeuCard from './shared/meuCard'

import './listaAvaliacao.css'


export default class ListaAvaliacoes extends Component {

  episodiosIdComNotas = (listaEpisodios, listaNotas) => {
    var notasAvalidas = listaNotas.todasAsNotas.filter(e => e.nota > 0)
    var episodioIdAvaliados = notasAvalidas.map(f => f.episodioId)
    var listaEpisodiosAvaliados = episodioIdAvaliados.flatMap(g => listaEpisodios.todos.filter(h => h.id === g))
    
    return listaEpisodiosAvaliados
  }

  notasAvalidas(listaNotas){
    return listaNotas.todasAsNotas.filter(e => e.nota > 0)
  }

  // eslint-disable-next-line no-undef
  notaSelecionada(id, listaNotas){
    return ( listaNotas.find( f => f.episodioId === id ) || {} ).nota
  }

  render() {
    const {listaEpisodios, listaNotas} = this.props.location.state
    const episodiosSelecionados = this.episodiosIdComNotas(listaEpisodios, listaNotas) 
    if (episodiosSelecionados === undefined) {
      return <h4>Não há itens avaliados :)</h4>
    } else {
      
      return (
        <React.Fragment>
          <ul className="list">
            {episodiosSelecionados.map(e =>
              <li key={e.id}>
                <MeuCard
                  imagemPath={e.thumbUrl}
                  tituloDoCard={e.nome}
                  conteudo = {`minha nota: ${this.notaSelecionada(e.id, listaNotas.todasAsNotas)}`}
                  linkTo={`/episodio/${e.id}`}
                  dadosNavegacao={{episodio: e, nota: this.notaSelecionada(e.id, listaNotas.todasAsNotas) }}
                  textoBotao='Saber mais'
                ></MeuCard>
              </li>
            )}
          </ul>
        </React.Fragment>
      )
    }
  }

}


//var result1 = listaNotas.todasAsNotas.filter(e => e.nota > 0).map(f => f.episodioId).flatMap(g => listaEpisodios.todos.filter(h => h.id === g))
  //var result4 = result1.concat(e => e.map(f => f.push(listaNotas.todasAsNotas.filter(e => e.nota > 0)))) 
  //result1.push(...listaNotas.todasAsNotas.filter(e => e.nota > 0))
