import React, { Component } from 'react'
import './meuCard.css'
import { Link } from 'react-router-dom'

class MeuCard extends Component {
  render() {
    const { imagemPath, tituloDoCard, conteudo, linkTo, textoBotao, dadosNavegacao} = this.props
    return (
      <React.Fragment>
        <div className="row">
          <div className="col s12 m7">
            <div className="card small">
              <div className="card-image">
                <img src={imagemPath} />
                <span className="card-title">{tituloDoCard}</span>
              </div>
              <div className="card-content">
                <p>{conteudo}</p>
              </div>
              <div className="card-action">
                {
                  linkTo ? <Link to={{ pathname: linkTo, state: dadosNavegacao }}>{ textoBotao }</Link> : textoBotao
                }
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    )
  }
}
export default MeuCard