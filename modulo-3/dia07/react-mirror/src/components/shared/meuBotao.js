import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import './meuBotao.css'

const MeuBotao = ({ corBtn, textoBotao, aoClicar, linkTo,  dadosNavegacao}) =>
    <React.Fragment>
        <button
            className={`btn ${corBtn}`}
            onClick={aoClicar}>
            {linkTo ? <Link to={{ pathname: linkTo, state: dadosNavegacao }}>{ textoBotao }</Link> : textoBotao}      
        </button>
    </React.Fragment>


MeuBotao.propTypes = {
  textoBotao: PropTypes.string.isRequired,
  aoClicar: PropTypes.func,
  corBtn: PropTypes.oneOf(['verde', 'azul', 'vermelho']),
  linkTo: PropTypes.string,
  dadosNavegacao: PropTypes.object
}

MeuBotao.defaultProps = {
  corBtn: 'verde'
}
export default MeuBotao


// export default class MeuBotao extends Component {
//     render() {
//         const { textoBotao, corBtn, aoClicar } = this.props
//         return (
//             <React.Fragment>
//                 <button
//                     className={`btn ${corBtn}`}
//                     onClick={aoClicar}>
//                     {textoBotao}
//                 </button>
//             </React.Fragment>
//         )
//     }
// }