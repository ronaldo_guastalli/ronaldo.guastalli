import React, { Component } from 'react'
import './mensagemFlash.css'
import './meuInputNumero'
import PropTypes from 'prop-types'

export default class MensagemFlash extends Component {
  constructor(props){
    super(props)
    this.idsTimeouts =[]
    this.animacao = ''
  }

  fechar = () => {
    this.props.atualizarMensagem(false)
  }

  limparTimeouts(){
    this.idsTimeouts.forEach( clearTimeout )
  }

  componentWillMount(){
    this.limparTimeouts()
  }

  componentDidUpdate( prevProps){
    const {deveExibirMensagemNaTela} = this.props
    if( prevProps.deveExibirMensagemNaTela !== deveExibirMensagemNaTela){
      //limpar os timeout
      this.limparTimeouts()
     const novoIdTimeout =  setTimeout(() => {
      this.fechar()
      }, this.props.segundos * 1000)
      this.idsTimeouts.push(novoIdTimeout)
    }
  }
  
  render() {
    const { 
      deveExibirMensagemNaTela,
      mensagem, 
      color} = this.props

      if(this.animacao || deveExibirMensagemNaTela){
        this.animacao = deveExibirMensagemNaTela ? 'fade-in' : 'fade-out'
      }

    return (
      <>
        <span onClick={this.fechar} className={`flash ${color}
        ${this.animacao}`}>{mensagem}</span>
      </>
    )
  }
}

MensagemFlash.propTypes = {
  mensagem : PropTypes.string,
  deveExibirMensagemNaTela: PropTypes.bool.isRequired,
  cor: PropTypes.oneOf(['verde', 'vermelho']),
  atualizarMensagem: PropTypes.func.isRequired,
  segundos: PropTypes.number
}

MensagemFlash.defaultProps = {
  cor: 'verde',
  segundos: 3
}
