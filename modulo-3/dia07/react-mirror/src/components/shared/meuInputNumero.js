import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './meuInputNumero.css'

export default class MeuInputNumero extends Component {
  perderFoco = evt => {
    if (this.props.obrigatorio && !evt.target.value) {
      this.props.atualizarErro(true)

    }else{
      this.props.atualizarErro(false)
      this.validaOValorDesteCampo(evt)
    }
  }

  validaOValorDesteCampo = (evt) => {
    this.props.inputQuerValidarCampo(evt)
  }

  render() {
    const { placeholder, visivel, mensagemCampo, deveExibirErro } = this.props
    return (
      <React.Fragment>
        {
          mensagemCampo && <span>{mensagemCampo}</span>

        }
        {
          visivel &&
          <input
            type="number"
            className={ deveExibirErro ? 'erro' : '' }
            placeholder={placeholder}
            onBlur={this.perderFoco}>
          </input>
        }
        {
          deveExibirErro && 
          <span className="mensagem-erro">
          * obrigatório
          </span>
        }
      </React.Fragment>
    )
  }
}

MeuInputNumero.propTypes = {
  visivel: PropTypes.bool.isRequired,
  atualizarErro: PropTypes.func.isRequired,
  obrigatorio: PropTypes.bool,
  mensagemCampo: PropTypes.string,
  placeholder: PropTypes.string,
  deveExibirErro: PropTypes.bool.isRequired
}

MeuInputNumero.defaultProps = {
  obrigatorio: false
}
