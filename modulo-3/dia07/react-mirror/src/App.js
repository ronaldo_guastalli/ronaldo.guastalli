import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom'
import './App.css'

import PaginaInicial from './components/paginaInicial';
import ListaAvaliacoes from './components/listaAvaliacoes'
import TelaDetalhesEpisodio from './components/telaDetalheEpisodio';
import TodosEpisodios from './components/todosEpisodios';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Router>
          <React.Fragment>
            <header className="App-header">
              <Route path="/" exact component={PaginaInicial}/>
              <Route path="/avaliacoes" component={ListaAvaliacoes}/>
              <Route path="/episodio/:id" component={TelaDetalhesEpisodio}/>
              <Route path="/todos" component={TodosEpisodios}/>
            </header>
          </React.Fragment>
        </Router>

      </div>
    );
  }
}
export default App;
