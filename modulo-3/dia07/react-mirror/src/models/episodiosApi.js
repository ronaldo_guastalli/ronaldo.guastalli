
import axios from 'axios'

export default class EpisodiosApi {
  buscar() {
    return axios.get( 'http://localhost:9000/api/episodios' )
  }
  // registrarNota( { nota, episodioId } ) {
  //   return axios.post( 'http://localhost:9000/api/notas', { nota, episodioId } )
  // }
  registrarNota( nota, episodioId ) {
    return axios.patch( `http://localhost:9000/api/notas/${episodioId}`, {nota} )
  }

  obterNotas(){
    return axios.get('http://localhost:9000/api/notas')
  }

  obterEpisodioDetalhes(episodioId){
    return axios.get(`http://localhost:9000/api/detalhes/${episodioId}`)
  }
  // obter nota de episódio: http://localhost:9000/api/notas?episodioId=11
}
// buscar(){
//     return fetch('http://localhost:9000/api/episodios')
//         .then(e => e.json())
// }


