import '../utils/number-prototypes'
import EpisodiosApi from "./episodiosApi"
// TODO: extrair para arquivo de prototypes
// Number.prototype.estaEntre = function ( a, b ) {
//     return a <= this && this <= b
// }

export default class Episodio {
    constructor( id, nome, duracao, temporada, ordemEpisodio, thumbUrl, qtdVezesAssistido){
        this.id = id
        this.nome = nome
        this.duracao = duracao
        this.temporada = temporada
        this.ordemEpisodio = ordemEpisodio
        this.thumbUrl = thumbUrl
        this.qtdVezesAssistido = qtdVezesAssistido || 0
        this.episodioApi = new EpisodiosApi()
    }

    validarNota( nota ){
        nota = parseInt( nota )
        //return 1 >= nota && nota <= 5
        return nota.estaEntre( 1, 5 )
    }
    
    avaliar( nota ){
        this.nota = parseInt( nota )
        this.assistido = true
        return this.episodioApi.registrarNota(this.nota, this.id )
        // return this.episodioApi.registrarNota({nota: this.nota, episodioId: this.id})
    }

    get duracaoEmMinutos() {
        return `${this.duracao}min`
    }

    get temporadaEpisodio() {
        return `${this.temporada.toString().padStart(2,'0')}/${this.ordemEpisodio.toString().padStart(2,'0')}`
    }

    

}