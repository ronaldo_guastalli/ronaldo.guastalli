// a partir desta implementação,
// todos objetos criados com a função String
// poderão chamar esta função. Ex:
// "oi".shimeji()
// var texto = "Kamisama pode ser cruel"
// texto.shimeji()
String.prototype.shimeji = function(uppercase = false) {
    var texto = this + " Shimeji!!!"
    return uppercase ? texto.toUpperCase() : texto
  }
  console.log("melhor cogumelo é:".shimeji(true))
  console.log("melhor cogumelo é:".shimeji(false))