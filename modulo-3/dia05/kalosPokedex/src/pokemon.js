// eslint-disable-next-line no-unused-vars
class Pokemon {
  constructor( objVindoDaApi ) {
    this.nome = objVindoDaApi.name
    this.thumbUrl = objVindoDaApi.sprites.front_default
    this.id = objVindoDaApi.id
    this._altura = objVindoDaApi.height
    this._peso = objVindoDaApi.weight
    this._tipos = objVindoDaApi.types
    this._estatisticas = objVindoDaApi.stats
  }

  get altura() {
    return this._altura * 10
  }

  get peso() {
    return ( this._peso * 0.1 ).toFixed( 2 )
  }

  get tipos() {
    const arrTipos = []
    this._tipos.forEach( elemento => {
      arrTipos.push( elemento.type.name )
    } )
    return arrTipos;
  }

  get estatisticas() {
    const arrEstatisticas = []
    this._estatisticas.forEach( elemento => {
      const objeto = {
        nome: elemento.stat.name,
        percentual: elemento.base_stat,
      }
      arrEstatisticas.push( objeto )
    } )
    return arrEstatisticas
  }
}
