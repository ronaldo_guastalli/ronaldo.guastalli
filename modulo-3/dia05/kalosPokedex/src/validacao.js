/* eslint-disable no-unused-vars */
/* eslint-disable radix */
/* eslint-disable consistent-return */
/* eslint-disable no-var */
class Validacao {
  static validacaoDoInput( valor ) {
    var valorInt = parseInt( valor );
    if ( valorInt < 0 ) {
      return alert( 'Valor inválido, número maior que ZERO!' );
    }
    // eslint-disable-next-line no-restricted-globals
    if ( isNaN( valorInt ) ) {
      return alert( 'Valor inválido, somente números!' );
    }
    if ( !Number.isInteger( parseFloat( valor ) ) && parseInt( valor ) ) {
      return alert( 'Valor inválido, somente números inteiros' );
    } if ( typeof valorInt === 'number' ) {
      return true;
    }
  }
}
