/* eslint-disable no-unused-expressions */
/* eslint-disable no-undef */
/* eslint-disable prefer-const */
/* eslint-disable no-use-before-define */
const parametros = 'input.input-id'
// eslint-disable-next-line no-use-before-define
const elemento = elementos( parametros )

ouvindoEventosNoInput( 'DOMContentLoaded', elemento[0] )
ouvindoEventosNoInput( 'change', elemento[0] )

// eslint-disable-next-line no-shadow
function elementos( parametros ) {
  return document.querySelectorAll( parametros )
}

function processarEvento( event ) {
  // eslint-disable-next-line prefer-const
  let valorIdPassadoInput = event.target.value
  let render = new Renderiza( valorIdPassadoInput )
  render.newPoke
}

function ouvindoEventosNoInput( tipoEvento, campoAplicaEvento ) {
  if ( tipoEvento === 'DOMContentLoaded' ) {
    let valorIdPassadoInput = 1
    let render = new Renderiza( valorIdPassadoInput )
    render.newPoke
  }
  campoAplicaEvento.addEventListener( tipoEvento, processarEvento, false )
}
