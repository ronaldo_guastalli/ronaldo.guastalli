class Pokemon{
  constructor( objVindoDaApi ){
    this.nome = objVindoDaApi.name
    this.thumbUrl = objVindoDaApi.sprites.front_default
    this.altura = objVindoDaApi.height
  }

  get altura(){
    //transformar altura para cm
    return this._altura * 10
  }
}