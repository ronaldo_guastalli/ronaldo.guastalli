-- ---
-- INSERT dos dados na tabela
-- ---

 INSERT INTO USUARIO (id,nome,apelido,senha,cpf) VALUES
 (USUARIO_SEQ.NEXTVAL,'Priscila','pri','123456','45698745623');
  INSERT INTO USUARIO (id,nome,apelido,senha,cpf) VALUES
 (USUARIO_SEQ.NEXTVAL,'Pedro','pJunior','157896','10569345878');
   INSERT INTO USUARIO (id,nome,apelido,senha,cpf) VALUES
 (USUARIO_SEQ.NEXTVAL,'Ronaldo','rjg','12587','10532678954112');

 INSERT INTO ENDERECO (id,logradouro,numero,complemento,bairro,cidade) VALUES
 (ENDERECO_SEQ.NEXTVAL,'Rua Pedro II',12,'ap204','São João','Porto Alegre');
  INSERT INTO ENDERECO (id,logradouro,numero,bairro,cidade) VALUES
 (ENDERECO_SEQ.NEXTVAL,'Rua Pedro Jose Zanetti',145,'Igara','Canoas');


 INSERT INTO USUARIO_ENDERECO (id,id_usuario,id_endereco) VALUES
 (USUARIO_ENDERECO_SEQ.NEXTVAL,1,2);
  INSERT INTO USUARIO_ENDERECO (id,id_usuario,id_endereco) VALUES
 (USUARIO_ENDERECO_SEQ.NEXTVAL,2,1);
  INSERT INTO USUARIO_ENDERECO (id,id_usuario,id_endereco) VALUES
 (USUARIO_ENDERECO_SEQ.NEXTVAL,3,2);

 INSERT INTO TIPO_CONTATO (id,nome) VALUES
 (TIPO_CONTATO_SEQ.NEXTVAL,'Celular');
  INSERT INTO tipo_contato (id,nome) VALUES
 (TIPO_CONTATO_SEQ.NEXTVAL,'Whatsapp');
  INSERT INTO tipo_contato (id,nome,quantidade) VALUES
 (TIPO_CONTATO_SEQ.NEXTVAL,'Emial', 3);

 INSERT INTO CONTATO (id,id_usuario,id_tipo_contato,valor) VALUES
 (CONTATO_SEQ.NEXTVAL,1,1,'5189635241');
  INSERT INTO CONTATO (id,id_usuario,id_tipo_contato,valor) VALUES
 (CONTATO_SEQ.NEXTVAL,2,2,'5189635241');
  INSERT INTO CONTATO (id,id_usuario,id_tipo_contato,valor) VALUES
 (CONTATO_SEQ.NEXTVAL,3,3,'rjg@gmail.com');

 INSERT INTO CARTAO_CREDITO (id,id_usuario,bandeira,numero,nome_escrito,codigo_seguranca,validade) VALUES
 (CARTAO_CREDITO_SEQ.NEXTVAL,1,'Visa','12345','Priscila Silva','123','2021');
  INSERT INTO CARTAO_CREDITO (id,id_usuario,bandeira,numero,nome_escrito,codigo_seguranca,validade) VALUES
 (CARTAO_CREDITO_SEQ.NEXTVAL,2,'Visa','12345','Pedro Rison','111','2025');
  INSERT INTO CARTAO_CREDITO (id,id_usuario,bandeira,numero,nome_escrito,codigo_seguranca,validade) VALUES
 (CARTAO_CREDITO_SEQ.NEXTVAL,3,'Master','12345','Ronaldo Silva','123','2030');

 INSERT INTO STATUS (id,nome) VALUES
 (STATUS_SEQ.NEXTVAL,'RECEM_CRIADO');
  INSERT INTO STATUS (id,nome) VALUES
 (STATUS_SEQ.NEXTVAL,'FERIDO');
  INSERT INTO STATUS (id,nome) VALUES
 (STATUS_SEQ.NEXTVAL,'MORTO');

  INSERT INTO RACA (id,nome,dano_inicio,vida_inicio,limitador) VALUES
 (RACA_SEQ.NEXTVAL,'Elfo',10,10,2);
 INSERT INTO RACA (id,nome,dano_inicio,vida_inicio,limitador) VALUES
 (RACA_SEQ.NEXTVAL,'Cinzento',1,1,2);
 INSERT INTO RACA (id,nome,dano_inicio,vida_inicio,limitador) VALUES
 (RACA_SEQ.NEXTVAL,'hobbits',5,10,3);

 INSERT INTO INVENTARIO (id,qtd_maxima) VALUES
 (INVENTARIO_SEQ.NEXTVAL,10);
  INSERT INTO INVENTARIO (id,qtd_maxima) VALUES
 (INVENTARIO_SEQ.NEXTVAL,5);
  INSERT INTO INVENTARIO (id,qtd_maxima) VALUES
 (INVENTARIO_SEQ.NEXTVAL,8);

 INSERT INTO PERSONAGEM (id,id_usuario,id_status,id_inventario,id_raca,nome,vida,dano,xp) VALUES
 (PERSONAGEM_SEQ.NEXTVAL,1,1,1,1,'Legolas',4,5,5);
  INSERT INTO PERSONAGEM (id,id_usuario,id_status,id_inventario,id_raca,nome,vida,dano,xp) VALUES
 (PERSONAGEM_SEQ.NEXTVAL,2,1,2,2,'Gandalf',4,6,5);
  INSERT INTO PERSONAGEM (id,id_usuario,id_status,id_inventario,id_raca,nome,vida,dano,xp) VALUES
 (PERSONAGEM_SEQ.NEXTVAL,3,1,3,1,'Aragorn',3,3,4);


 INSERT INTO ITEM (id,descricao,qtd_maxima) VALUES
 (ITEM_SEQ.NEXTVAL,'Flecha',20);
 INSERT INTO ITEM (id,descricao,qtd_maxima) VALUES
 (ITEM_SEQ.NEXTVAL,'Escudo',2);
  INSERT INTO ITEM (id,descricao,qtd_maxima) VALUES
 (ITEM_SEQ.NEXTVAL,'Martelo',2);

 INSERT INTO LISTA_ITENS_INVENTARIO (id,id_inventario,id_item,quantidade) VALUES
 (LISTA_ITENS_INVENTARIO_SEQ.NEXTVAL,1,1,2);
 INSERT INTO LISTA_ITENS_INVENTARIO (id,id_inventario,id_item,quantidade) VALUES
 (LISTA_ITENS_INVENTARIO_SEQ.NEXTVAL,2,2,2);
 INSERT INTO LISTA_ITENS_INVENTARIO (id,id_inventario,id_item,quantidade) VALUES
 (LISTA_ITENS_INVENTARIO_SEQ.NEXTVAL,3,3,2);

 INSERT INTO TIPO_RACA (id,id_raca,nome,bonus_vida,bonus_danos) VALUES
 (TIPO_RACA_SEQ.NEXTVAL,1,'Elfo Verde',15,10);
 INSERT INTO TIPO_RACA (id,id_raca,nome,bonus_vida,bonus_danos) VALUES
 (TIPO_RACA_SEQ.NEXTVAL,1,'Mago',15,10);
 INSERT INTO TIPO_RACA (id,id_raca,nome,bonus_vida,bonus_danos) VALUES
 (TIPO_RACA_SEQ.NEXTVAL,1, 'Guardião',10,10);

 INSERT INTO TIPO_RACA_ITEM (id,id_tipo_raca,id_item) VALUES
 (TIPO_RACA_ITEM_SEQ.NEXTVAL,1,1);
  INSERT INTO TIPO_RACA_ITEM (id,id_tipo_raca,id_item) VALUES
 (TIPO_RACA_ITEM_SEQ.NEXTVAL,2,2);
   INSERT INTO TIPO_RACA_ITEM (id,id_tipo_raca,id_item) VALUES
 (TIPO_RACA_ITEM_SEQ.NEXTVAL,3,3);

 INSERT INTO MATERIAL (id,nome,unidade) VALUES
 (MATERIAL_SEQ.NEXTVAL,'Madeira',2);
  INSERT INTO MATERIAL (id,nome,unidade) VALUES
 (MATERIAL_SEQ.NEXTVAL,'Ferro',1);
  INSERT INTO MATERIAL (id,nome,unidade) VALUES
 (MATERIAL_SEQ.NEXTVAL,'Pedra',12);

 INSERT INTO ITEM_MATERIAL (id,id_item,id_material,quantidade) VALUES
 (ITEM_MATERIAL_SEQ.NEXTVAL,1,1,10);
  INSERT INTO ITEM_MATERIAL (id,id_item,id_material,quantidade) VALUES
 (ITEM_MATERIAL_SEQ.NEXTVAL,2,2,15);
  INSERT INTO ITEM_MATERIAL (id,id_item,id_material,quantidade) VALUES
 (ITEM_MATERIAL_SEQ.NEXTVAL,3,3,20);

 

 