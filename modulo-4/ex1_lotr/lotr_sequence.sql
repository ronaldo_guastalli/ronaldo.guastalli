-- ---
-- DROP das sequence antigas
-- ---
DROP SEQUENCE PERSONAGEM_SEQ;
DROP SEQUENCE ENDERECO_SEQ;
DROP SEQUENCE  USUARIO_ENDERECO_SEQ;
DROP SEQUENCE USUARIO_SEQ;
DROP SEQUENCE  CONTATO_SEQ;
DROP SEQUENCE  TIPO_CONTATO_SEQ;
DROP SEQUENCE  CARTAO_CREDITO_SEQ;
DROP SEQUENCE  STATUS_SEQ;
DROP SEQUENCE  INVENTARIO_SEQ;
DROP SEQUENCE  LISTA_ITENS_INVENTARIO_SEQ;
DROP SEQUENCE  ITEM_SEQ;
DROP SEQUENCE  ITEM_MATERIAL_SEQ;
DROP SEQUENCE  MATERIAL_SEQ;
DROP SEQUENCE  TIPO_RACA_ITEM_SEQ;
DROP SEQUENCE  TIPO_RACA_SEQ;
DROP SEQUENCE  RACA_SEQ;

--PERSONAGEM_SEQ
CREATE SEQUENCE PERSONAGEM_SEQ
START WITH 1
INCREMENT BY 1;

--ENDERECO_SEQ
CREATE SEQUENCE ENDERECO_SEQ
START WITH 1
INCREMENT BY 1;

--usuario_ENDERECO_SEQ
CREATE SEQUENCE USUARIO_ENDERECO_SEQ
START WITH 1
INCREMENT BY 1;

--usuario
CREATE SEQUENCE USUARIO_SEQ
START WITH 1
INCREMENT BY 1;

--CONTATO_SEQ
CREATE SEQUENCE CONTATO_SEQ
START WITH 1
INCREMENT BY 1;

--tipo_CONTATO_SEQ
CREATE SEQUENCE TIPO_CONTATO_SEQ
START WITH 1
INCREMENT BY 1;

--cartao_credito_seq
CREATE SEQUENCE CARTAO_CREDITO_SEQ
START WITH 1
INCREMENT BY 1;

--status
CREATE SEQUENCE STATUS_SEQ
START WITH 1
INCREMENT BY 1;

--INVENTARIO_SEQ
CREATE SEQUENCE INVENTARIO_SEQ
START WITH 1
INCREMENT BY 1;

--lista_itens_INVENTARIO_SEQ
CREATE SEQUENCE LISTA_ITENS_INVENTARIO_SEQ
START WITH 1
INCREMENT BY 1;

--ITEM_SEQ
CREATE SEQUENCE ITEM_SEQ
START WITH 1
INCREMENT BY 1;

--ITEM_MATERIAL_SEQ
CREATE SEQUENCE ITEM_MATERIAL_SEQ
START WITH 1
INCREMENT BY 1;

--material
CREATE SEQUENCE MATERIAL_SEQ
START WITH 1
INCREMENT BY 1;

--tipo_RACA_ITEM_SEQ
CREATE SEQUENCE TIPO_RACA_ITEM_SEQ
START WITH 1
INCREMENT BY 1;

--tipo_raca
CREATE SEQUENCE TIPO_RACA_SEQ
START WITH 1
INCREMENT BY 1;

--raca
CREATE SEQUENCE RACA_SEQ
START WITH 1
INCREMENT BY 1;
