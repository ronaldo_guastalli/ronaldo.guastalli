-- TABLES
-- ---
-- DROP em todas as tabelas
-- ---
DROP TABLE PERSONAGEM CASCADE CONSTRAINTS;
DROP TABLE ENDERECO CASCADE CONSTRAINTS;
DROP TABLE  USUARIO_ENDERECO CASCADE CONSTRAINTS;
DROP TABLE USUARIO CASCADE CONSTRAINTS;
DROP TABLE  CONTATO CASCADE CONSTRAINTS;
DROP TABLE  TIPO_CONTATO CASCADE CONSTRAINTS;
DROP TABLE  CARTAO_CREDITO CASCADE CONSTRAINTS;
DROP TABLE  STATUS CASCADE CONSTRAINTS;
DROP TABLE  INVENTARIO CASCADE CONSTRAINTS;
DROP TABLE  LISTA_ITENS_INVENTARIO CASCADE CONSTRAINTS;
DROP TABLE  ITEM CASCADE CONSTRAINTS;
DROP TABLE  ITEM_MATERIAL CASCADE CONSTRAINTS;
DROP TABLE  MATERIAL CASCADE CONSTRAINTS;
DROP TABLE  TIPO_RACA_ITEM CASCADE CONSTRAINTS;
DROP TABLE  TIPO_RACA CASCADE CONSTRAINTS;
DROP TABLE  RACA CASCADE CONSTRAINTS;
-- ---
-- Table 'personagem'
-- ---
CREATE TABLE PERSONAGEM (
  ID NUMBER NOT NULL PRIMARY KEY,
  ID_USUARIO NUMBER NOT NULL,
  ID_STATUS NUMBER NOT NULL,
  ID_INVENTARIO NUMBER NOT NULL,
  ID_RACA NUMBER NOT NULL,
  NOME VARCHAR(100) NOT NULL,
  VIDA NUMBER(10) NOT NULL,
  DANO NUMBER(10) NOT NULL,
  XP NUMBER(10) NOT NULL
);
-- ---
-- Table 'usuario'
-- ---
CREATE TABLE USUARIO(
  ID NUMBER NOT NULL PRIMARY KEY,
  NOME VARCHAR(100) NOT NULL,
  APELIDO VARCHAR(15) NOT NULL,
  SENHA VARCHAR(15) NOT NULL,
  CPF NUMBER NOT NULL
);
-- ---
-- Table 'cartao_credito'
-- ---
CREATE TABLE CARTAO_CREDITO(
  ID NUMBER NOT NULL PRIMARY KEY,
  ID_USUARIO NUMBER NOT NULL,
  BANDEIRA VARCHAR(50) NOT NULL,
  NUMERO NUMBER(5) NOT NULL,
  NOME_ESCRITO VARCHAR(100) NOT NULL,
  CODIGO_SEGURANCA NUMBER(5) NOT NULL,
  VALIDADE NUMBER(5) NOT NULL  
);
-- ---
-- Table 'endereco'
-- ---
CREATE TABLE ENDERECO(
  ID NUMBER NOT NULL PRIMARY KEY,
  LOGRADOURO VARCHAR(100) NOT NULL,
  NUMERO NUMBER(5) NOT NULL,
  COMPLEMENTO VARCHAR(50),
  BAIRRO VARCHAR(100) NOT NULL,
  CIDADE VARCHAR(100) NOT NULL
);
-- ---
-- Table 'usuario_endereco'
-- ---
CREATE TABLE USUARIO_ENDERECO(
  ID NUMBER NOT NULL PRIMARY KEY,
  ID_USUARIO NUMBER NOT NULL,
  ID_ENDERECO NUMBER NOT NULL
);
-- ---
-- Table 'status'
-- ---
CREATE TABLE STATUS(
  ID NUMBER NOT NULL PRIMARY KEY,
  NOME VARCHAR(50) NOT NULL
);
-- ---
-- Table 'inventario'
-- ---
CREATE TABLE INVENTARIO(
  ID NUMBER NOT NULL PRIMARY KEY,
  QTD_MAXIMA NUMBER NOT NULL
);
-- ---
-- Table 'lista_itens_inventario'
-- ---
CREATE TABLE ITEM(
  ID NUMBER NOT NULL PRIMARY KEY,
  DESCRICAO VARCHAR(50) NOT NULL,
  QTD_MAXIMA NUMBER(5) DEFAULT 1 NOT NULL
);
-- ---
-- Table 'lista_itens_inventario'
-- ---
CREATE TABLE LISTA_ITENS_INVENTARIO(
  ID NUMBER NOT NULL PRIMARY KEY,
  ID_INVENTARIO NUMBER NOT NULL,
  ID_ITEM NUMBER NOT NULL,
  QUANTIDADE NUMBER(10) NOT NULL
);
-- ---
-- Table 'material'
-- ---
CREATE TABLE MATERIAL(
  ID NUMBER NOT NULL PRIMARY KEY,
  NOME VARCHAR(50) NOT NULL,
  UNIDADE NUMBER(5) NOT NULL
);
-- ---
-- Table 'item_material'
-- ---
CREATE TABLE ITEM_MATERIAL(
  ID NUMBER NOT NULL PRIMARY KEY,
  ID_ITEM NUMBER NOT NULL,
  ID_MATERIAL NUMBER NOT NULL,
  QUANTIDADE NUMBER(5) NOT NULL
);
-- ---
-- Table 'raca'
-- ---
CREATE TABLE RACA(
  ID NUMBER NOT NULL PRIMARY KEY,
  NOME VARCHAR(100) NOT NULL,
  DANO_INICIO NUMBER(10) NOT NULL,
  VIDA_INICIO NUMBER(5) NOT NULL,
  LIMITADOR NUMBER(5) NOT NULL
);
-- ---
-- Table 'tipo_raca'
-- ---
CREATE TABLE TIPO_RACA(
  ID NUMBER NOT NULL PRIMARY KEY,
  ID_RACA NUMBER NOT NULL,
  NOME VARCHAR(50) NOT NULL,
  BONUS_VIDA NUMBER(5) NOT NULL,
  BONUS_DANOS NUMBER(5) DEFAULT 0 NOT NULL
);
-- ---
-- Table 'tipo_raca_item'
-- ---
CREATE TABLE TIPO_RACA_ITEM(
  ID NUMBER NOT NULL PRIMARY KEY,
  ID_TIPO_RACA NUMBER NOT NULL,
  ID_ITEM NUMBER NOT NULL
);
-- ---
-- Table 'tipo_contato'
-- ---
CREATE TABLE TIPO_CONTATO(
  ID NUMBER NOT NULL PRIMARY KEY,
  NOME VARCHAR(100) NOT NULL,
  QUANTIDADE NUMBER(5) DEFAULT 1 NOT NULL  
);
-- ---
-- Table 'contato'
-- ---
CREATE TABLE CONTATO(
  ID NUMBER NOT NULL PRIMARY KEY,
  VALOR VARCHAR(100) NOT NULL,
  ID_USUARIO NUMBER NOT NULL,
  ID_TIPO_CONTATO NUMBER NOT NULL
);

-- ---
-- Foreign Keys 
-- ---
ALTER TABLE cartao_credito ADD FOREIGN KEY (id_usuario) REFERENCES usuario(id);
ALTER TABLE usuario_endereco ADD FOREIGN KEY (id_usuario) REFERENCES usuario(id);
ALTER TABLE usuario_endereco ADD FOREIGN KEY (id_endereco) REFERENCES endereco(id);
ALTER TABLE lista_itens_inventario ADD FOREIGN KEY (id_inventario) REFERENCES inventario(id);
ALTER TABLE lista_itens_inventario ADD FOREIGN KEY (id_item) REFERENCES item(id);
ALTER TABLE item_material ADD FOREIGN KEY (id_item) REFERENCES item(id);
ALTER TABLE item_material ADD FOREIGN KEY (id_material) REFERENCES material(id);
ALTER TABLE tipo_raca ADD FOREIGN KEY (id_raca) REFERENCES raca(id);
ALTER TABLE tipo_raca_item ADD FOREIGN KEY (id_tipo_raca) REFERENCES tipo_raca(id);
ALTER TABLE tipo_raca_item ADD FOREIGN KEY (id_item) REFERENCES item(id);
ALTER TABLE personagem ADD FOREIGN KEY (id_usuario) REFERENCES usuario(id);
ALTER TABLE personagem ADD FOREIGN KEY (id_status) REFERENCES status(id);
ALTER TABLE personagem ADD FOREIGN KEY (id_inventario) REFERENCES inventario(id);
ALTER TABLE personagem ADD FOREIGN KEY (id_raca) REFERENCES raca(id);
ALTER TABLE contato ADD FOREIGN KEY (id_tipo_contato) REFERENCES tipo_contato(id);


