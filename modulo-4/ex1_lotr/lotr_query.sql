--todos usuarios
select usuario.*
from usuario;
--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
--todos usuario e personagem
select 
    usuario.nome,
    usuario.apelido,
    personagem.nome,
    personagem.vida,
    personagem.dano,
    personagem.xp,
    raca.nome,
    raca.dano_inicio,
    raca.vida_inicio,
    raca.limitador,
    inventario.qtd_maxima,
    status.nome
from usuario
join personagem on personagem.id_usuario = usuario.id
join raca on raca.id = personagem.id_raca
join inventario on inventario.id = personagem.id_inventario
join status on status.id = personagem.id_status;

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
--O personagem deve possuir 2 editar
select 
    personagem.nome,
    status.nome
from personagem
join status on status.id = personagem.id_status
where personagem.id_status = 1;
update 
    personagem
set personagem.id_status = 2
where personagem.id = 1;
--voltando para valor antigo
update 
    personagem
set personagem.id_status = 1
where personagem.id = 1;
--e deve haver 1 delete de cada tabela que tenha vinculo com outra resolvendo todos os conflitos desse vinculo.
  --delete
  --1.chech
  select personagem.id, status.nome
  from personagem
  join status on status.id = personagem.id_status
  where status.nome like upper('%morto%');
  --2 delete
  delete 
      personagem
  where 
  personagem.id = (select personagem.id
  from personagem
  join status on status.id = personagem.id_status
  where status.nome like upper('%morto%'));

  --3 insere novamente
  INSERT INTO PERSONAGEM (id,id_usuario,id_status,id_inventario,id_raca,nome,vida,dano,xp) VALUES
  (PERSONAGEM_SEQ.NEXTVAL,1,1,1,1,'Legolas',4,5,5);


--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
   --outro delete
 /*deletar todos os usuarios que estao com personagem mortos e com xp < ou igual a 4*/
 select 
    usuario.id as id_user,
    usuario.nome as nome_usuario,
    usuario.apelido as apelido_usuario,
    personagem.nome as nome_personagem,
    personagem.vida,
    personagem.dano,
    personagem.xp,
    raca.nome as raca,
    raca.dano_inicio,
    raca.vida_inicio,
    raca.limitador,
    inventario.qtd_maxima,
    status.nome as status
from usuario
join personagem on personagem.id_usuario = usuario.id
join raca on raca.id = personagem.id_raca
join inventario on inventario.id = personagem.id_inventario
join status on status.id = personagem.id_status;
--passar para status morto usuario id = 1
update 
    personagem
set personagem.id_status = (select status.id from status where status.nome like upper('%morto%'))
where personagem.id = 3;

--excluir os registros na ordem das suas dependências, primeiro os filhos e depois os pais
select * from contato;
delete contato 
where contato.id_usuario = (
select usuario.id
from usuario
join personagem on personagem.id_usuario = usuario.id
join status on status.id = personagem.id_status
where status.nome = 'MORTO' and personagem.xp <= 4);

select * from cartao_credito;
delete cartao_credito 
where cartao_credito.id_usuario = (
select usuario.id
from usuario
join personagem on personagem.id_usuario = usuario.id
join status on status.id = personagem.id_status
where status.nome = 'MORTO' and personagem.xp <= 4);

select * from usuario_endereco;
delete usuario_endereco 
where usuario_endereco.id_usuario = (
select usuario.id
from usuario
join personagem on personagem.id_usuario = usuario.id
join status on status.id = personagem.id_status
where status.nome = 'MORTO' and personagem.xp <= 4);

select * from personagem;
delete personagem 
where personagem.id_usuario = (
select usuario.id
from usuario
join personagem on personagem.id_usuario = usuario.id
join status on status.id = personagem.id_status
where status.nome = 'MORTO' and personagem.xp <= 4);

--E para finalizar quero saber como eu visualizo os resultados de cada tabela.


--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
