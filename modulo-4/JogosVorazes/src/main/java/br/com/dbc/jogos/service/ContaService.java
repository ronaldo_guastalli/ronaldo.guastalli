
package br.com.dbc.jogos.service;

import br.com.dbc.jogos.dao.ContaDAO;
import br.com.dbc.jogos.dto.ContaDTO;




public class ContaService {
    private static final ContaDAO CONTA_DAO = new ContaDAO();
    
    public void deletarConta(ContaDTO contaDTO){
        CONTA_DAO.delete(contaDTO.getIdConta());
        
    }
}
