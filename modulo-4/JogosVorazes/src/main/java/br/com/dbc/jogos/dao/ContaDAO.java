
package br.com.dbc.jogos.dao;

import br.com.dbc.jogos.entity.Conta;


public class ContaDAO extends AbstractDAO<Conta>{

    @Override
    protected Class<Conta> getEntityClass() {
        return Conta.class;
    }
    
}
