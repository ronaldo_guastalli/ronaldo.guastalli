/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.jogos;

import br.com.dbc.jogos.dao.ContaDAO;
import br.com.dbc.jogos.dto.ClienteDTO;
import br.com.dbc.jogos.dto.ContaDTO;
import br.com.dbc.jogos.dto.EnderecoDTO;
import br.com.dbc.jogos.entity.Cliente;
import br.com.dbc.jogos.entity.Conta;
import br.com.dbc.jogos.entity.HibernateUtil;
import br.com.dbc.jogos.service.ClienteService;
import br.com.dbc.jogos.service.ContaService;
import org.hibernate.Session;

/**
 *
 * @author tiago
 */
public class Main {
    public static void main(String[] args) {
        ClienteDTO dto = new ClienteDTO();
        dto.setNomeCliente("Cliente 1");
        dto.setCpfCliente("123");
        EnderecoDTO eDTO = new EnderecoDTO();
        eDTO.setLogradouroEndereco("Rua 1");
        eDTO.setNumeroEndereco(1);
        eDTO.setBairroEndereco("Bairro 1");
        eDTO.setCidadeEndereco("Cidade 1");
        dto.setEnderecoDTO(eDTO);
        
        Cliente cliente = new Cliente();
        cliente.setNome("pejnsdbajhf");
        cliente.setCpf("545"); 
        
        Conta conta = new Conta();
        conta.setAgencia("111");
        conta.setBanco("BB");
        conta.setNumero("222");
        conta.setCliente(cliente);
        
        
        Session session = HibernateUtil.getSession(true);
        session.save(conta);
        HibernateUtil.commitTransaction();
       ContaService contaService = new ContaService();
        ContaDTO dtoc = new ContaDTO();
        dtoc.setIdConta(conta.getId());
       contaService.deletarConta(dtoc);
        System.exit(0);
    }
}
