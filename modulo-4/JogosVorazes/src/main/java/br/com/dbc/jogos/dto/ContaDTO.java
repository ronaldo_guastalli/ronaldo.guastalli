
package br.com.dbc.jogos.dto;


public class ContaDTO {
    
    private Integer idConta;

    public Integer getIdConta() {
        return idConta;
    }

    public void setIdConta(Integer idConta) {
        this.idConta = idConta;
    }
    
    
}
