
package br.com.dbc.contabasica.dto;

/**
 *
 * @author ronaldoguastalli
 */
public class ClienteDTO {
    private Integer idCliente;
    private String nomeCliente;
    private String cpfCliente; 

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public String getNomeCliente() {
        return nomeCliente;
    }

    public void setNomeCliente(String nomeCliente) {
        this.nomeCliente = nomeCliente;
    }

    public String getCpfCliente() {
        return cpfCliente;
    }

    public void setCpfCliente(String cpfCliente) {
        this.cpfCliente = cpfCliente;
    }
    
    
}
