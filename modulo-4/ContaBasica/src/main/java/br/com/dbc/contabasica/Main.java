package br.com.dbc.contabasica;

import br.com.dbc.contabasica.dao.ClienteDAO;
import br.com.dbc.contabasica.dto.ClienteDTO;
import br.com.dbc.contabasica.entity.Cliente;
import br.com.dbc.contabasica.entity.Conta;
import br.com.dbc.contabasica.entity.Endereco;
import br.com.dbc.contabasica.entity.HibernateUtil;
import br.com.dbc.contabasica.service.ClienteService;
import org.hibernate.Session;

public class Main {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Cliente cliente = new Cliente();
        cliente.setNome("Joao Paulo");
        cliente.setCpf("111111111111");
        
        ClienteDTO clienteDTO = new ClienteDTO();
        clienteDTO.setNomeCliente("Pedro da Silva");
        clienteDTO.setCpfCliente("13456789790");
        
        ClienteService cService = new ClienteService();
        cService.salvarCliente(clienteDTO);
               
        
//        Endereco endereco = new Endereco();
//        endereco.setLogradouro("Dom Pedro");
//        endereco.setNumero(345);
//        endereco.setBairro("Igara");
//        endereco.setCidade("Canoas");
//        
//        Conta conta = new Conta();
//        conta.setAgencia("111");
//        conta.setBanco("BB");
//        conta.setNumero("222");
//        //conta.setCliente(cliente);
//                
//        Session session = HibernateUtil.getSession(true);
//        session.save(cliente);
//        session.save(endereco);
//        session.save(conta);
//        
//        HibernateUtil.commitTransaction();
    }
}
