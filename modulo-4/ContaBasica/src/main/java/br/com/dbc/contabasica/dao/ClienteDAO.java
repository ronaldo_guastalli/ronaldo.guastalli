package br.com.dbc.contabasica.dao;

import br.com.dbc.contabasica.dto.ClienteDTO;
import br.com.dbc.contabasica.entity.Cliente;

/**
 *
 * @author ronaldoguastalli
 */
public class ClienteDAO extends AbstractDAO{
    
    public Cliente from(ClienteDTO dto){
        Cliente c = new Cliente();
        c.setNome(dto.getNomeCliente());
        c.setCpf(dto.getCpfCliente());
        return c;
    }

    @Override
    protected Cliente getEntityCliente() {
        Cliente Cliente = null;
        return Cliente;
    }
    
}
