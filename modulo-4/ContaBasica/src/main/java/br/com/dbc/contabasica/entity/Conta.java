package br.com.dbc.contabasica.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

/**
 *
 * @author ronaldoguastalli
 * herança:https://www.devmedia.com.br/tipos-de-heranca-no-hibernate/28641
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)//Identifica que a estratégia de herança será “JOINED”, ou seja, será feita uma junção através de chaves estrangeiras.
public class Conta {

    @Id
    @SequenceGenerator(allocationSize = 1, sequenceName = "CONTA_SEQ", name = "CONTA_SEQ")
    @GeneratedValue(generator = "CONTA_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    @Column(nullable = false, length = 50)
    private String banco;
    
    @Column(nullable = false, length = 10)
    private String agencia;
    
    @Column(nullable = false, length = 10)
    private String numero;
    
    //dono da associação tabela que vai ter a FK
    @OneToOne(mappedBy = "conta")
    @JoinColumn(name = "id_cliente" , nullable = false)
    private Cliente cliente;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public String getAgencia() {
        return agencia;
    }

    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }       
    

}
