package br.com.dbc.contabasica.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.PrimaryKeyJoinColumn;

/**
 *
 * @author ronaldoguastalli
 */
@Entity
@PrimaryKeyJoinColumn(name = "id_conta")// Identifica qual campo fará essa “junção” entre a tabela conta_poupanca e a tabela conta.
public class ContaPoupanca extends Conta {
    
    @Column(name = "deposito_mensal", nullable = false, precision = 10, scale = 2)
    private Double depositoMensal;
    
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private ContaType tipo = ContaType.POUPANCA;

    public Double getDepositoMensal() {
        return depositoMensal;
    }

    public void setDepositoMensal(Double depositoMensal) {
        this.depositoMensal = depositoMensal;
    }
    
    

}
