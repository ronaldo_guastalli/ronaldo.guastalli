package br.com.dbc.contabasica.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.PrimaryKeyJoinColumn;

/**
 *
 * @author ronaldoguastalli
 */
@Entity
@PrimaryKeyJoinColumn(name = "id_conta")// Identifica qual campo fará essa “junção” entre a tabela conta_corrente e a tabela conta.
public class ContaCorrente extends Conta{

    @Column(nullable = false, precision = 10, scale = 2)
    private Double limite;
    
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private ContaType tipo = ContaType.CORRENTE;

    public Double getLimite() {
        return limite;
    }

    public void setLimite(Double limite) {
        this.limite = limite;
    }
    
    

}
