package br.com.dbc.contabasica.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.PrimaryKeyJoinColumn;

/**
 *
 * @author ronaldoguastalli
 */
@Entity
@PrimaryKeyJoinColumn(name = "id_conta")
public class ContaSalario extends Conta {

    @Column(nullable = false, precision = 10, scale = 2)
    private Double salario;
    
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private ContaType tipo = ContaType.SALARIO;

    public Double getSalario() {
        return salario;
    }

    public void setSalario(Double salario) {
        this.salario = salario;
    }    

}
