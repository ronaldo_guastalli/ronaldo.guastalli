
package br.com.dbc.contabasica.entity;

/**
 *
 * @author ronaldoguastalli
 */
public enum ContaType {
    SALARIO, CORRENTE, POUPANCA;
}
