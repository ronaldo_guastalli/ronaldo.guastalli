
package br.com.dbc.contabasica.dao;

import br.com.dbc.contabasica.entity.Cliente;
import br.com.dbc.contabasica.entity.HibernateUtil;
import org.hibernate.Session;

/**
 *
 * @author ronaldoguastalli
 */
public abstract class AbstractDAO {
    protected abstract Cliente getEntityCliente(); 

    //generalizando ?????
    
    //CRUD
    public void saveOrUpdate(Cliente c){
        Session session = HibernateUtil.getSession(true);
        session.save(c);
        HibernateUtil.commitTransaction();
    }    
    
    
}
