package br.com.dbc.contabasica.service;

import br.com.dbc.contabasica.dao.ClienteDAO;
import br.com.dbc.contabasica.dto.ClienteDTO;
import br.com.dbc.contabasica.entity.Cliente;
import br.com.dbc.contabasica.entity.HibernateUtil;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Session;

/**
 *
 * @author ronaldoguastalli
 */
public class ClienteService {

    private static final Logger LOG = Logger.getLogger(ClienteService.class.getName());
    private static final ClienteDAO CLIENTE_DAO = new ClienteDAO();

    public void salvarCliente(ClienteDTO dto) {
        Session session = HibernateUtil.getSession(true);
        try {
            Cliente cliente = CLIENTE_DAO.from(dto); //from metodo de conexão entre DAO e DTO
            CLIENTE_DAO.saveOrUpdate(cliente); //saveOrUpdate esta na AbstractDAO herdado por todas as DAO's
            HibernateUtil.commitTransaction(); //commit no BD
        } catch (Exception e) {
            LOG.log(Level.SEVERE, e.getMessage(), e);
            session.getTransaction().rollback();// algum problema retorno ao estado inicial com rollback
            throw new RuntimeException(e.getMessage(), e);
        }
    }
}
