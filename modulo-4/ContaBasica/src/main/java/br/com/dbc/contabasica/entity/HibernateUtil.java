/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.contabasica.entity;

import org.hibernate.Session;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.SessionFactory;

/**
 * Hibernate Utility class with a convenient method to get Session Factory
 * object.
 *
 * @author ronaldoguastalli
 */
public class HibernateUtil {

    private static final SessionFactory sessionFactory;
    private static final Session session;

    static {
        try {
            // Create the SessionFactory from standard (hibernate.cfg.xml) 
            // config file.
            sessionFactory = new AnnotationConfiguration().configure().buildSessionFactory();
            session = sessionFactory.openSession();
        } catch(Throwable ex) {
            // Log the exception. 
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static Session getSession() {
        return session;
    }

    private static Integer transactionCount = 0;

    public static Session getSession(boolean transaction) {
        if (transaction) {
            transactionCount++;
            if (session.getTransaction() == null || !session.getTransaction().isActive()) {
                session.beginTransaction();
            }
        }
        return session;
    }

    public static void commitTransaction() {
        if (--transactionCount == 0) {
            session.getTransaction().commit();
        }
    }
}
