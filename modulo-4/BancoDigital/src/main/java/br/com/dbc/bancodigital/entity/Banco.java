package br.com.dbc.bancodigital.entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "banco")
@DiscriminatorValue("banco")
public class Banco {
    
    @Id
    @SequenceGenerator(allocationSize = 1, name = "banco_seq", sequenceName = "banco_seq")
    @GeneratedValue(generator = "banco_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    private String nome;
    
    
    private String codigo;
    
    @OneToMany(mappedBy = "banco")
    private List<Agencia> agencias = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public List<Agencia> getAgencias() {
        return agencias;
    }

    public void pushAgencias(Agencia... agencias) {
        this.agencias.addAll(Arrays.asList(agencias));
    }
    
    
    
   
}
