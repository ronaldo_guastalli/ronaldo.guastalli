package br.com.dbc.bancodigital.entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "endereco")
@DiscriminatorValue("endereco")
public class Endereco {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "endereco_seq", sequenceName = "endereco_seq")
    @GeneratedValue(generator = "endereco_seq", strategy = GenerationType.SEQUENCE)
    private Integer id; 
    
    @Column(name = "rua")
    private String rua;

    @Column(name = "numero")
    private String numero;

    @Column(name = "complemento")
    private String complemento;
    
    @Column(name = "bairro")
    private String bairro;

    @Column(name = "codigo_postal")
    private String codigoPostal;
    
    @ManyToOne
    @JoinColumn(name = "id_cidade")
    private Cidade cidade;    
    
    @ManyToMany(mappedBy = "enderecos")
    private List<Usuario> usuarios = new ArrayList<>();
    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRua() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }    
    

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    public void pushUsuarios(Usuario... usuarios) {
        this.usuarios.addAll(Arrays.asList(usuarios));
    }

    @Override
    public String toString() {
        return "Endereco{" + "id=" + id + ", rua=" + rua + ", numero=" + numero + ", complemento=" + complemento + ", bairro=" + bairro + ", codigoPostal=" + codigoPostal + ", cidade=" + cidade + '}';
    }
    
    
    
}
