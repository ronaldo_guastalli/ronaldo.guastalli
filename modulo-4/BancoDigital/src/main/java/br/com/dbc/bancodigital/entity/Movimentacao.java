package br.com.dbc.bancodigital.entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "movimentacao")
@DiscriminatorValue("movimentacao")
public class Movimentacao {
    
    @Id
    @SequenceGenerator(allocationSize = 1, name = "movimentacao_seq", sequenceName = "movimentacao_seq")
    @GeneratedValue(generator = "movimentacao_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    private String data;
    
    
    private double valor;
    
    @ManyToOne
    @JoinColumn(name = "id_conta_origem")
    private Conta contaOrigem;
    
    @ManyToOne
    @JoinColumn(name = "id_conta_destino")
    private Conta contaDestino;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public Conta getContaOrigem() {
        return contaOrigem;
    }

    public void setContaOrigem(Conta contaOrigem) {
        this.contaOrigem = contaOrigem;
    }

    public Conta getContaDestino() {
        return contaDestino;
    }

    public void setContaDestino(Conta contaDestino) {
        this.contaDestino = contaDestino;
    }
    
    

  
}
