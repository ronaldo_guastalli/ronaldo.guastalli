
package br.com.dbc.bancodigital.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name = "documento")
@SequenceGenerator(allocationSize = 1, name = "documento_seq", sequenceName = "documento_seq" )
public class Documento {
    
    @Id
    @GeneratedValue(generator = "documento_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    private String valor;
    
    @ManyToOne
    @JoinColumn(name = "id_usuario")
    private Usuario usuario;
    
    @ManyToOne
    @JoinColumn(name = "id_documento_tipo")
    private DocumentoTipo documentoTipo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public DocumentoTipo getDocumentoTipo() {
        return documentoTipo;
    }

    public void setDocumentoTipo(DocumentoTipo documentoTipo) {
        this.documentoTipo = documentoTipo;
    }

    @Override
    public String toString() {
        return "Documento{" + "id=" + id + ", valor=" + valor + ", usuario=" + usuario + ", documentoTipo=" + documentoTipo + '}';
    }
    
    
    
}
