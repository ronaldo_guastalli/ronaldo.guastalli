
package br.com.dbc.bancodigital.entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "contato_tipo")
@SequenceGenerator(allocationSize = 1, name = "contato_tipo_seq", sequenceName = "contato_tipo_seq")
public class ContatoTipo {
    
    @Id
    @GeneratedValue(generator = "contato_tipo_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    private String nome;
    
    @OneToMany(mappedBy = "contatoTipo")
    private List<Contato> contatos = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Contato> getContatos() {
        return contatos;
    }

    public void pushContatos(Contato... contatos) {
        this.contatos.addAll(Arrays.asList(contatos));
    }

    
    
    
    
    
}
