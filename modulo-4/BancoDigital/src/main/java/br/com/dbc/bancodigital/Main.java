package br.com.dbc.bancodigital;

import br.com.dbc.bancodigital.entity.Agencia;
import br.com.dbc.bancodigital.entity.Banco;
import br.com.dbc.bancodigital.entity.Cidade;
import br.com.dbc.bancodigital.entity.UsuarioCliente;
import br.com.dbc.bancodigital.entity.Conta;
import br.com.dbc.bancodigital.entity.ContaTipo;
import br.com.dbc.bancodigital.entity.Contato;
import br.com.dbc.bancodigital.entity.ContatoTipo;
import br.com.dbc.bancodigital.entity.Documento;
import br.com.dbc.bancodigital.entity.DocumentoTipo;
import br.com.dbc.bancodigital.entity.Endereco;
import br.com.dbc.bancodigital.entity.Estado;
import br.com.dbc.bancodigital.entity.HibernateUtil;
import br.com.dbc.bancodigital.entity.Movimentacao;
import br.com.dbc.bancodigital.entity.Pais;
import br.com.dbc.bancodigital.entity.Usuario;
import br.com.dbc.bancodigital.entity.UsuarioTipo;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

public class Main {

    private static final Logger LOG = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            Pais pais = new Pais();
            pais.setNome("Brasil");
            pais.setSigla("BRL");
            session.save(pais);

            Estado estado = new Estado();
            estado.setNome("Rio Grande do Sul");
            estado.setSigla("RS");
            estado.setPais(pais);
            pais.pushEstados(estado);
            session.save(estado);

            Cidade cidade = new Cidade();
            cidade.setNome("Canoas");
            cidade.setEstado(estado);
            estado.pushCidades(cidade);
            session.save(cidade);

            Endereco endereco = new Endereco();
            endereco.setRua("Pedro II");
            endereco.setNumero("410");
            endereco.setCodigoPostal("41854897");
            endereco.setComplemento("Ap204");
            endereco.setBairro("Igara");
            endereco.setCidade(cidade);
            cidade.pusEnderecos(endereco);
            session.save(endereco);

            Usuario usuario = new Usuario();
            usuario.setNome("Priscila Vilas Lobos");
            usuario.setSenha("123456");
            usuario.setTipo(UsuarioTipo.PESSOA_FISICA);
            endereco.pushUsuarios(usuario);
            usuario.pushEnderecos(endereco);
            session.save(usuario);

            DocumentoTipo documentoTipo1 = new DocumentoTipo();
            documentoTipo1.setNome("CPF");
            DocumentoTipo documentoTipo2 = new DocumentoTipo();
            documentoTipo2.setNome("RG");
            session.save(documentoTipo1);
            session.save(documentoTipo2);

            Documento documento1 = new Documento();
            documento1.setUsuario(usuario);
            documento1.setDocumentoTipo(documentoTipo2);
            documento1.setValor("34567812390");
            session.save(documento1);

            Documento documento2 = new Documento();
            documento2.setUsuario(usuario);
            documento2.setDocumentoTipo(documentoTipo1);
            documento2.setValor("438769073");
            DocumentoTipo documentoTipo = new DocumentoTipo();
            documentoTipo.pusDocumentos(documento1, documento2);
            session.save(documento2);

            ContatoTipo contatoTipo = new ContatoTipo();
            contatoTipo.setNome("Celular");
            session.save(contatoTipo);

            Contato contato = new Contato();
            contato.setUsuario(usuario);
            contato.setContatoTipo(contatoTipo);
            contato.setValor("51845231234");
            contatoTipo.pushContatos(contato);
            usuario.pushContatos(contato);
            session.save(contato);

            ContaTipo contaTipo = new ContaTipo();
            contaTipo.setNome("Conta Corrente");
            ContaTipo contaTipo2 = new ContaTipo();
            contaTipo2.setNome("Conta Poupança");
            ContaTipo contaTipo3 = new ContaTipo();
            contaTipo3.setNome("Conta Conjunta");
            session.save(contaTipo);
            session.save(contaTipo2);
            session.save(contaTipo3);            
            
            Banco banco = new Banco();
            banco.setNome("Banco do Brasil SA");
            banco.setCodigo("001");
            session.save(banco);
            
            Agencia agencia = new Agencia();
            agencia.setBanco(banco);
            agencia.setNome("Bairro Dom Pedro");
            agencia.setNumero("14232");            
            banco.pushAgencias(agencia);
            session.save(agencia);

            Conta conta = new Conta();
            conta.setAgencia(agencia);
            conta.setNumero("45679");
            conta.setSaldo(500.00);
            conta.setContaTipo(contaTipo);
            conta.setContaTipo(contaTipo3);
            Conta conta2 = new Conta();
            conta2.setAgencia(agencia);
            conta2.setNumero("11111");
            conta2.setSaldo(1000.00);
            conta2.setContaTipo(contaTipo2);
            agencia.pushContas(conta, conta2);
            session.save(conta);
            session.save(conta2);

            UsuarioCliente usuarioCliente = new UsuarioCliente();
            usuarioCliente.setConta(conta);
            usuarioCliente.setUsuario(usuario);
            conta.pushClientes(usuarioCliente);
            usuario.pushClientes(usuarioCliente);
            session.save(usuarioCliente);

            Movimentacao movimentacao = new Movimentacao();
            movimentacao.setData("15/03/2019");
            movimentacao.setValor(100.00);
            movimentacao.setContaOrigem(conta);
            movimentacao.setContaDestino(conta2);
            conta.pushMovimentacoes(movimentacao);
            conta2.pushMovimentacoes(movimentacao);
            session.save(movimentacao);
            
            
            Criteria criteria1 = session.createCriteria(Usuario.class);
            criteria1.createAlias("enderecos", "endereco");                     
            criteria1.add(Restrictions.and(
                    Restrictions.ilike("endereco.bairro", "%ga%"),
                    Restrictions.ilike("endereco.rua", "%dro%")
            ));
            List<Usuario> usuarios1 = criteria1.list();            
            usuarios1.forEach(System.out::println);
            
//            Criteria criteria2 = session.createCriteria(Usuario.class);
//            criteria2.createAlias("", string1);
            
                               
            List<Endereco> usuarios2 = session.createQuery(" select u.enderecos from Usuario u  ").list();
            usuarios2.forEach(System.out::println);
            
            Criteria criteria3 = session.createCriteria(Usuario.class);
            criteria3.setProjection(Projections.rowCount());
            System.out.println(String.format("Form encontrados %s registro(s)", criteria3.uniqueResult()));

            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        } finally {
            if (session != null) {
                session.close();
            }
            System.exit(0);

        }

    }
}
