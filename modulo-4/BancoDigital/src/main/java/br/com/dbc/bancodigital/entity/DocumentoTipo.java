
package br.com.dbc.bancodigital.entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "documento_tipo")
@SequenceGenerator(allocationSize = 1, name = "documento_tipo_seq", sequenceName = "documento_tipo_seq")
public class DocumentoTipo {
    
    @Id
    @GeneratedValue(generator = "documento_tipo_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    private String nome;
    
    @OneToMany(mappedBy = "documentoTipo")
    private List<Documento> documentos = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Documento> getDocumentos() {
        return documentos;
    }

    public void pusDocumentos(Documento... documentos) {
        this.documentos.addAll(Arrays.asList(documentos));
    }
    
    
    
    
}
