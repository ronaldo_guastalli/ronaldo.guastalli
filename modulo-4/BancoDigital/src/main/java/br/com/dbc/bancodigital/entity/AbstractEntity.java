
package br.com.dbc.bancodigital.entity;

import java.io.Serializable;

public abstract class AbstractEntity implements Serializable {
    public abstract Integer getId(); //retornar a chave primária das entidades
}
