
package br.com.dbc.bancodigital.dao;

import br.com.dbc.bancodigital.entity.AbstractEntity;
import br.com.dbc.bancodigital.entity.HibernateUtil;
import org.hibernate.Session;

/*
Esta classe será responsável por centralizar as lógicas de persistência em comum
entre o grupo de entidades do contrato EntidadeBase
*/
public abstract class AbstractDAO<E extends AbstractEntity> {
    public void Criar(E entity){
        Session session = HibernateUtil.getSession();
        session.save(entity);
    }
}
