
package br.com.dbc.bancodigital.entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "conta_tipo")
@SequenceGenerator(allocationSize = 1, name = "conta_tipo_seq", sequenceName = "conta_tipo_seq")
public class ContaTipo {
    
    @Id
    @GeneratedValue(generator = "conta_tipo_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    private String nome;
    
    @OneToMany(mappedBy = "contaTipo")
    private List<Conta> contas = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Conta> getContas() {
        return contas;
    }

    public void pushContas(Conta... contas) {
        this.contas.addAll(Arrays.asList(contas));
    }
    
    
   
    
}
