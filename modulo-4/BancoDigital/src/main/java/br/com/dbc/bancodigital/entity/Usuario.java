package br.com.dbc.bancodigital.entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "usuario")
@SequenceGenerator(allocationSize = 1, name = "usuario_seq", sequenceName = "usuario_seq")
public class Usuario {

    @Id
    @GeneratedValue(generator = "usuario_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;

    private String nome;

    @Enumerated(EnumType.STRING)
    private UsuarioTipo tipo;

    private String senha;

    @ManyToMany
    @SequenceGenerator(allocationSize = 1, name = "usuario_endereco_seq", sequenceName = "usuario_endereco_seq")
    @GeneratedValue(generator = "usuario_endereco_seq", strategy = GenerationType.SEQUENCE)
    @JoinTable(name = "usuario_endereco",
            joinColumns
            = {@JoinColumn(name = "id_usuario")},
            inverseJoinColumns
            = {@JoinColumn(name = "id_endereco")})
    private List<Endereco> enderecos = new ArrayList<>();
    
    @OneToMany(mappedBy = "usuario")
    private List<Documento> documentos = new ArrayList<>();
    
    @OneToMany(mappedBy = "usuario")
    private List<Contato> contatos = new ArrayList<>();
    
    @OneToMany(mappedBy = "usuario")
    private List<UsuarioCliente> usuariosCliente = new ArrayList<>();
    
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public UsuarioTipo getTipo() {
        return tipo;
    }

    public void setTipo(UsuarioTipo tipo) {
        this.tipo = tipo;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public List<Endereco> getEnderecos() {
        return enderecos;
    }

    public void pushEnderecos(Endereco... enderecos) {
        this.enderecos.addAll(Arrays.asList(enderecos));
    }

    public List<Documento> getDocumentos() {
        return documentos;
    }

    public void pushDocumentos(Documento... documentos) {
        this.documentos.addAll(Arrays.asList(documentos));
    }

    public List<Contato> getContatos() {
        return contatos;
    }

    public void pushContatos(Contato... contatos) {
        this.contatos.addAll(Arrays.asList(contatos));
    }   

    public List<UsuarioCliente> getClientes() {
        return usuariosCliente;
    }

    public void pushClientes(UsuarioCliente... clientes) {
        this.usuariosCliente.addAll(Arrays.asList(clientes));
    }

    @Override
    public String toString() {
        return "Usuario{" + "id=" + id + ", nome=" + nome + ", tipo=" + tipo + ", senha=" + senha + '}';
    }

    

    
    
    
    
    


}
