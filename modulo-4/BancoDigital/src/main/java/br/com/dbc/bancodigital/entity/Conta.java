package br.com.dbc.bancodigital.entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "conta")
@DiscriminatorValue("conta")
public class Conta {
    
    @Id
    @SequenceGenerator(allocationSize = 1, name = "conta_seq", sequenceName = "conta_seq")
    @GeneratedValue(generator = "conta_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    private String numero;
    
    
    private double saldo;
    
    @ManyToOne
    @JoinColumn(name = "id_tipo")
    private ContaTipo contaTipo;
    
    @ManyToOne
    @JoinColumn(name = "id_agencia")
    private Agencia agencia;
    
    @OneToMany(mappedBy = "conta")
    private List<UsuarioCliente> contasCliente = new ArrayList<>();    
    
    @OneToMany(mappedBy = "contaOrigem")
    private List<Movimentacao> movimentacoes = new ArrayList<>(); 
    
    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public List<UsuarioCliente> getClientes() {
        return contasCliente;
    }

    public void pushClientes(UsuarioCliente clientes) {
        this.contasCliente.addAll(Arrays.asList(clientes));
    }

    public List<UsuarioCliente> getContasCliente() {
        return contasCliente;
    }

    public void pushContasCliente(UsuarioCliente... contasCliente) {
        this.contasCliente.addAll(Arrays.asList(contasCliente));
    }

    public Agencia getAgencia() {
        return agencia;
    }

    public void setAgencia(Agencia agencia) {
        this.agencia = agencia;
    }

    public ContaTipo getContaTipo() {
        return contaTipo;
    }

    public void setContaTipo(ContaTipo contaTipo) {
        this.contaTipo = contaTipo;
    }

    public List<Movimentacao> getMovimentacoes() {
        return movimentacoes;
    }

    public void pushMovimentacoes(Movimentacao... movimentacoes) {
        this.movimentacoes.addAll(Arrays.asList(movimentacoes));
    }
    
    
  
}
