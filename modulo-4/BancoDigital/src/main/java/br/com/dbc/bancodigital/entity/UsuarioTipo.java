package br.com.dbc.bancodigital.entity;

public enum UsuarioTipo {
    PESSOA_FISICA, PESSOA_JURIDICA; 
}
