--endereco completo
select pessoa.nome as nome_cliente, endereco.*, cidade.*, estado.*, pais.* 
from cliente
join pessoa on pessoa.id = cliente.id_pessoa
join pessoa_endereco on pessoa_endereco.id_pessoa = pessoa.id
join endereco on endereco.id = pessoa_endereco.id_endereco
join cidade on cidade.id = endereco.id_cidade
join estado on estado.id = cidade.id_estado
join pais on pais.id = estado.id_pais; 

--informações bancárias
select pessoa.*, conta.*, tipo.*, agencia.*, banco.*
from cliente
join pessoa on pessoa.id = cliente.id
join cliente_conta on cliente_conta.id_cliente = cliente.id
join conta on conta.id = cliente_conta.id_conta
join tipo on tipo.id = conta.id_tipo
join agencia on agencia.id = conta.id_agencia
join banco on banco.id = agencia.id_banco;