-- ---
-- Test Data
-- ---

INSERT INTO pais (id,nome,sigla) VALUES
(pais_seq.nextval,'Brasil','BRL');

INSERT INTO estado (id,nome,id_pais,sigla) VALUES
(estado_seq.nextval,'Rio Grande do Sul',1,'RS');

INSERT INTO cidade (id,nome,id_estado) VALUES
(cidade_seq.nextval,'Canoas',1);

INSERT INTO banco (id,nome,codigo) VALUES
(banco_seq.nextval,'Banco do Brasil',1);

INSERT INTO pessoa (id,nome) VALUES
(pessoa_seq.nextval,'Paulo da Silva');
INSERT INTO pessoa (id,nome) VALUES
(pessoa_seq.nextval,'Maria Fernandes');

INSERT INTO tipo (id,nome) VALUES
(tipo_seq.nextval,'Conta Corrente');
INSERT INTO tipo (id,nome) VALUES
(tipo_seq.nextval,'Conta Salário');
INSERT INTO tipo (id,nome) VALUES
(tipo_seq.nextval,'Conta Conjunta');

INSERT INTO agencia (id,nome,numero,id_banco) VALUES
(agencia_seq.nextval,'Agencia Vila Madalena',22222,1);

INSERT INTO conta (id,numero,id_tipo,id_agencia,saldo) VALUES
(conta_seq.nextval,'12345',1,1,500.00);
INSERT INTO conta (id,numero,id_tipo,id_agencia,saldo) VALUES
(conta_seq.nextval,'11111',2,1,200.00);

INSERT INTO movimentacao (id,data,valor,id_conta_origem,id_conta_destino) VALUES
(movimentacao_seq.nextval,TO_DATE('2019/02/03 21:02:44', 'yyyy/mm/dd hh24:mi:ss'),100.00,1,2);

INSERT INTO tipo_documento (id,nome) VALUES
(tipo_documento_seq.nextval,'RG');

INSERT INTO cliente (id,id_pessoa) VALUES
(cliente_seq.nextval,1);
INSERT INTO cliente (id,id_pessoa) VALUES
(cliente_seq.nextval,2);

INSERT INTO documento (id,id_tipo_documento,id_cliente,valor) VALUES
(documento_seq.nextval,1,1,'123459082');
INSERT INTO documento (id,id_tipo_documento,id_cliente,valor) VALUES
(documento_seq.nextval,1,2,'333333333');

INSERT INTO endereco (id,id_cidade,nome,bairro,codigo_postal,numero,complemento) VALUES
(endereco_seq.nextval,1,'Dom Pedro II','São João','87210123',123,'apt209B');
INSERT INTO endereco (id,id_cidade,nome,bairro,codigo_postal,numero) VALUES
(endereco_seq.nextval,1,'Rua Paraguai','São João','56666666',100);

INSERT INTO pessoa_endereco (id,id_pessoa,id_endereco) VALUES
(pessoa_endereco_seq.nextval,1,1);
INSERT INTO pessoa_endereco (id,id_pessoa,id_endereco) VALUES
(pessoa_endereco_seq.nextval,2,2);

INSERT INTO tipo_contato (id,nome) VALUES
(tipo_contato_seq.nextval,'Celular/Whatsapp');
INSERT INTO tipo_contato (id,nome) VALUES
(tipo_contato_seq.nextval,'E-mail pessoal');
INSERT INTO tipo_contato (id,nome) VALUES
(tipo_contato_seq.nextval,'E-mail profissional');

INSERT INTO contato (id,id_cliente,valor,id_tipo_contato) VALUES
(contato_seq.nextval,1,'51876564534',1);
INSERT INTO contato (id,id_cliente,valor,id_tipo_contato) VALUES
(contato_seq.nextval,2,'sq@gmail.com',2);


INSERT INTO funcao (id,nome) VALUES
(funcao_seq.nextval,'Gerente');
INSERT INTO funcao (id,nome) VALUES
(funcao_seq.nextval,'Gerente Geral');
INSERT INTO funcao (id,nome) VALUES
(funcao_seq.nextval,'Supervisor');

INSERT INTO usuario (id,id_pessoa,id_banco,id_funcao) VALUES
(usuario_seq.nextval,2,1,1);

INSERT INTO cliente_conta (id,id_cliente,id_conta) VALUES
(cliente_conta_seq.nextval,1,1);

INSERT INTO emprestimo (id,nome,valor) VALUES
(emprestimo_seq.nextval,'Bom pagador', 10000.00);

INSERT INTO situacao (id,valor) VALUES
(situacao_seq.nextval,'Aprovado');
INSERT INTO situacao (id,valor) VALUES
(situacao_seq.nextval,'Negado');
INSERT INTO situacao (id,valor) VALUES
(situacao_seq.nextval,'Aguardando assinaturas');

INSERT INTO cliente_conta_emprestimo (id,id_cliente_conta,id_emprestimo,id_situacao) VALUES
(cliente_conta_emprestimo_seq.nextval,1,1,1);


INSERT INTO usuario_aprovador (id,id_usuario,id_emprestimo) VALUES
(usuario_aprovador_seq.nextval,1,1);