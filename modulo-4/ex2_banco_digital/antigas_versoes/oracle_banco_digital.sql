-- ---
-- Table 'pais'
-- 
-- ---

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE pais';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
		
CREATE TABLE pais (
  id NUMBER NOT NULL,
  nome VARCHAR2(100) NOT NULL,
  sigla VARCHAR2(10) NOT NULL,
  PRIMARY KEY (id)
);

-- Generate ID using sequence and trigger
-- Generete ID usando sequence
CREATE SEQUENCE pais_seq START WITH 1 INCREMENT BY 1;

-- ---
-- Table 'cidade'
-- 
-- ---

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE cidade';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
		
CREATE TABLE cidade (
  id NUMBER NOT NULL,
  nome VARCHAR2(100) NOT NULL,
  id_estado NUMBER NOT NULL,
  PRIMARY KEY (id)
);

-- Generate ID using sequence and trigger
CREATE SEQUENCE cidade_seq START WITH 1 INCREMENT BY 1;

-- ---
-- Table 'estado'
-- 
-- ---

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE estado';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
		
CREATE TABLE estado (
  id NUMBER NOT NULL,
  nome VARCHAR2(100) NOT NULL,
  id_pais NUMBER NOT NULL,
  sigla VARCHAR2(10) NOT NULL,
  PRIMARY KEY (id)
);

-- Generate ID using sequence and trigger
CREATE SEQUENCE estado_seq START WITH 1 INCREMENT BY 1;


-- ---
-- Table 'banco'
-- 
-- ---

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE banco';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
		
CREATE TABLE banco (
  id NUMBER NOT NULL,
  nome VARCHAR2(100) NOT NULL,
  codigo NUMBER(10) NOT NULL,
  PRIMARY KEY (id)
);

-- Generate ID using sequence and trigger
CREATE SEQUENCE banco_seq START WITH 1 INCREMENT BY 1;


-- ---
-- Table 'pessoa'
-- 
-- ---

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE pessoa';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
		
CREATE TABLE pessoa (
  id NUMBER NOT NULL,
  nome VARCHAR2(100) NOT NULL,
  PRIMARY KEY (id)
);

-- Generate ID using sequence and trigger
CREATE SEQUENCE pessoa_seq START WITH 1 INCREMENT BY 1;


-- ---
-- Table 'conta'
-- 
-- ---

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE conta';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
		
CREATE TABLE conta (
  id NUMBER NOT NULL,
  numero NUMBER(10) NOT NULL,
  id_tipo NUMBER NOT NULL,
  id_agencia NUMBER NOT NULL,
  saldo BINARY_DOUBLE DEFAULT 0.00 NOT NULL,
  PRIMARY KEY (id)
);

-- Generate ID using sequence and trigger
CREATE SEQUENCE conta_seq START WITH 1 INCREMENT BY 1;

-- ---
-- Table 'agencia'
-- 
-- ---

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE agencia';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
		
CREATE TABLE agencia (
  id NUMBER NOT NULL,
  nome VARCHAR2(100) NOT NULL,
  numero NUMBER(10) NOT NULL,
  id_banco NUMBER NOT NULL,
  PRIMARY KEY (id)
);

-- Generate ID using sequence and trigger
CREATE SEQUENCE agencia_seq START WITH 1 INCREMENT BY 1;


-- ---
-- Table 'tipo'
-- 
-- ---

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE tipo';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
		
CREATE TABLE tipo (
  id NUMBER NOT NULL,
  nome VARCHAR2(100) NOT NULL,
  PRIMARY KEY (id)
);

-- Generate ID using sequence and trigger
CREATE SEQUENCE tipo_seq START WITH 1 INCREMENT BY 1;


-- ---
-- Table 'movimentacao'
-- 
-- ---

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE movimentacao';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
		
CREATE TABLE movimentacao (
  id NUMBER NOT NULL,
  data DATE NOT NULL,
  valor NUMBER(10,2) NOT NULL,
  id_conta_origem NUMBER DEFAULT NULL NULL,
  id_conta_destino NUMBER DEFAULT NULL NULL,
  PRIMARY KEY (id)
);

-- Generate ID using sequence and trigger
CREATE SEQUENCE movimentacao_seq START WITH 1 INCREMENT BY 1;


-- ---
-- Table 'documento'
-- 
-- ---

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE documento';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
		
CREATE TABLE documento (
  id NUMBER NOT NULL,
  id_tipo_documento NUMBER NOT NULL,
  id_cliente NUMBER NOT NULL,
  valor VARCHAR2(100) NOT NULL,
  PRIMARY KEY (id)
);

-- Generate ID using sequence and trigger
CREATE SEQUENCE documento_seq START WITH 1 INCREMENT BY 1;


-- ---
-- Table 'endereco'
-- 
-- ---

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE endereco';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
		
CREATE TABLE endereco (
  id NUMBER NOT NULL,
  id_cidade NUMBER NOT NULL,
  nome VARCHAR2(100) NOT NULL,
  bairro VARCHAR2(100) DEFAULT NULL NULL,
  codigo_postal VARCHAR2(10) DEFAULT NULL NULL,
  numero NUMBER(10) NOT NULL,
  complemento VARCHAR2(10) DEFAULT NULL NULL,
  PRIMARY KEY (id)
);

-- Generate ID using sequence and trigger
CREATE SEQUENCE endereco_seq START WITH 1 INCREMENT BY 1;

-- ---
-- Table 'pessoa_endereco'
-- 
-- ---

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE pessoa_endereco';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
		
CREATE TABLE pessoa_endereco (
  id NUMBER NOT NULL,
  id_pessoa NUMBER NOT NULL,
  id_endereco NUMBER NOT NULL,
  PRIMARY KEY (id)
);

-- Generate ID using sequence and trigger
CREATE SEQUENCE pessoa_endereco_seq START WITH 1 INCREMENT BY 1;
-- ---
-- Table 'contato'
-- 
-- ---

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE contato';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
		
CREATE TABLE contato (
  id NUMBER NOT NULL,
  id_cliente NUMBER NOT NULL,
  valor VARCHAR2(100) NOT NULL,
  id_tipo_contato NUMBER DEFAULT NULL NULL,
  PRIMARY KEY (id)
);

-- Generate ID using sequence and trigger
CREATE SEQUENCE contato_seq START WITH 1 INCREMENT BY 1;


-- ---
-- Table 'tipo_contato'
-- 
-- ---

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE tipo_contato';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
		
CREATE TABLE tipo_contato (
  id NUMBER DEFAULT NULL NULL,
  nome VARCHAR2(100) NOT NULL,
  PRIMARY KEY (id)
);

-- Generate ID using sequence and trigger
CREATE SEQUENCE tipo_contato_seq START WITH 1 INCREMENT BY 1;


-- ---
-- Table 'tipo_documento'
-- 
-- ---

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE tipo_documento';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
		
CREATE TABLE tipo_documento (
  id NUMBER NOT NULL,
  nome VARCHAR2(50) NOT NULL,
  PRIMARY KEY (id)
);

-- Generate ID using sequence and trigger
CREATE SEQUENCE tipo_documento_seq START WITH 1 INCREMENT BY 1;


-- ---
-- Table 'cliente'
-- 
-- ---

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE cliente';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
		
CREATE TABLE cliente (
  id NUMBER NOT NULL,
  id_pessoa NUMBER NOT NULL,
  PRIMARY KEY (id)
);

-- Generate ID using sequence and trigger
CREATE SEQUENCE cliente_seq START WITH 1 INCREMENT BY 1;


-- ---
-- Table 'funcao'
-- 
-- ---

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE funcao';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
		
CREATE TABLE funcao (
  id NUMBER NOT NULL,
  nome VARCHAR2(100) NOT NULL,
  PRIMARY KEY (id)
);

-- Generate ID using sequence and trigger
CREATE SEQUENCE funcao_seq START WITH 1 INCREMENT BY 1;


-- ---
-- Table 'usuario'
-- 
-- ---

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE usuario';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
		
CREATE TABLE usuario (
  id NUMBER NOT NULL,
  id_pessoa NUMBER NOT NULL,
  id_banco NUMBER NOT NULL,
  id_funcao NUMBER NOT NULL,
  PRIMARY KEY (id)
);

-- Generate ID using sequence and trigger
CREATE SEQUENCE usuario_seq START WITH 1 INCREMENT BY 1;


-- ---
-- Table 'cliente_conta'
-- 
-- ---

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE cliente_conta';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
		
CREATE TABLE cliente_conta (
  id NUMBER NOT NULL,
  id_cliente NUMBER NOT NULL,
  id_conta NUMBER NOT NULL,
  PRIMARY KEY (id)
);

-- Generate ID using sequence and trigger
CREATE SEQUENCE cliente_conta_seq START WITH 1 INCREMENT BY 1;


-- ---
-- Table 'cliente_conta_emprestimo'
-- 
-- ---

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE cliente_conta_emprestimo';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
		
CREATE TABLE cliente_conta_emprestimo (
  id NUMBER NOT NULL,
  id_cliente_conta NUMBER NOT NULL,
  id_emprestimo NUMBER NOT NULL,
  id_situacao NUMBER NOT NULL,
  PRIMARY KEY (id)
);

-- Generate ID using sequence and trigger
CREATE SEQUENCE cliente_conta_emprestimo_seq START WITH 1 INCREMENT BY 1;

-- ---
-- Table 'emprestimo'
-- 
-- ---

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE emprestimo';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
		
CREATE TABLE emprestimo (
  id NUMBER NOT NULL,
  nome VARCHAR2(100) NOT NULL,
  valor NUMBER(10,0) DEFAULT 0 NOT NULL,
  PRIMARY KEY (id)
);

-- Generate ID using sequence and trigger
CREATE SEQUENCE emprestimo_seq START WITH 1 INCREMENT BY 1;


-- ---
-- Table 'usuario_aprovador'
-- 
-- ---

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE usuario_aprovador';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
		
CREATE TABLE usuario_aprovador (
  id NUMBER NOT NULL,
  id_usuario NUMBER NOT NULL,
  id_emprestimo NUMBER NOT NULL,
  PRIMARY KEY (id)
);

-- Generate ID using sequence and trigger
CREATE SEQUENCE usuario_aprovador_seq START WITH 1 INCREMENT BY 1;


-- ---
-- Table 'situacao'
-- 
-- ---

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE situacao';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
		
CREATE TABLE situacao (
  id NUMBER NOT NULL,
  valor VARCHAR2(50) NOT NULL,
  PRIMARY KEY (id)
);

-- Generate ID using sequence and trigger
CREATE SEQUENCE situacao_seq START WITH 1 INCREMENT BY 1;

-- ---
-- Foreign Keys 
-- ---

ALTER TABLE cidade ADD FOREIGN KEY (id_estado) REFERENCES estado (id);
ALTER TABLE estado ADD FOREIGN KEY (id_pais) REFERENCES pais (id);
ALTER TABLE conta ADD FOREIGN KEY (id_tipo) REFERENCES tipo (id);
ALTER TABLE conta ADD FOREIGN KEY (id_agencia) REFERENCES agencia (id);
ALTER TABLE agencia ADD FOREIGN KEY (id_banco) REFERENCES banco (id);
ALTER TABLE movimentacao ADD FOREIGN KEY (id_conta_origem) REFERENCES conta (id);
ALTER TABLE movimentacao ADD FOREIGN KEY (id_conta_destino) REFERENCES conta (id);
ALTER TABLE documento ADD FOREIGN KEY (id_tipo_documento) REFERENCES tipo_documento (id);
ALTER TABLE documento ADD FOREIGN KEY (id_cliente) REFERENCES pessoa (id);
ALTER TABLE endereco ADD FOREIGN KEY (id_cidade) REFERENCES cidade (id);
ALTER TABLE pessoa_endereco ADD FOREIGN KEY (id_) REFERENCES pessoa (id);
ALTER TABLE pessoa_endereco ADD FOREIGN KEY (id_endereco) REFERENCES endereco (id);
ALTER TABLE contato ADD FOREIGN KEY (id_cliente) REFERENCES pessoa (id);
ALTER TABLE contato ADD FOREIGN KEY (id_tipo_contato) REFERENCES tipo_contato (id);
ALTER TABLE cliente ADD FOREIGN KEY (id_pessoa) REFERENCES pessoa (id);
ALTER TABLE usuario ADD FOREIGN KEY (id_pessoa) REFERENCES pessoa (id);
ALTER TABLE usuario ADD FOREIGN KEY (id_banco) REFERENCES banco (id);
ALTER TABLE usuario ADD FOREIGN KEY (id_funcao) REFERENCES funcao (id);
ALTER TABLE cliente_conta ADD FOREIGN KEY (id_cliente) REFERENCES cliente (id);
ALTER TABLE cliente_conta ADD FOREIGN KEY (id_conta) REFERENCES conta (id);
ALTER TABLE cliente_conta_emprestimo ADD FOREIGN KEY (id_cliente_conta) REFERENCES cliente_conta (id);
ALTER TABLE cliente_conta_emprestimo ADD FOREIGN KEY (id_emprestimo) REFERENCES emprestimo (id);
ALTER TABLE cliente_conta_emprestimo ADD FOREIGN KEY (id_situacao) REFERENCES situacao (id);
ALTER TABLE usuario_aprovador ADD FOREIGN KEY (id_usuario) REFERENCES usuario (id);
ALTER TABLE usuario_aprovador ADD FOREIGN KEY (id_emprestimo) REFERENCES emprestimo (id);

-- ---
-- Table Properties
-- ---

-- ALTER TABLE `pais` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `cidade` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `estado` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `banco` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `pessoa` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `conta` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `agencia` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `tipo` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `movimentacao` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `documento` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `endereco` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `pessoa_endereco` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `contato` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `tipo_contato` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `tipo_documento` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `cliente` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `funcao` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `usuario` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `cliente_conta` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `cliente_conta_emprestimo` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `emprestimo` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `usuario_aprovador` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `situacao` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

