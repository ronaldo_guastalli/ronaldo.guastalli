--todos os clientes
select * from cliente;

--clientes seus enderecos, documento e contatos
select cliente.*, 
    rua.nome, 
    rua.numero, 
    rua.codigo_postal, 
    rua.complemento, 
    cidade.nome,
    estado.nome,
    estado.sigla,
    pais.nome,
    pais.sigla
from cliente
join endereco on endereco.id_cliente = cliente.id
join rua on rua.id = endereco.id_rua
join cidade on cidade.id = rua.id_cidade
join estado on estado.id = cidade.id_estado
join pais on pais.id = estado.id_pais;

--cliente conta
select cliente.*, conta.numero, tipo.nome, agencia.nome, agencia.numero, banco.nome, banco.codigo, conta.saldo
from cliente
join cliente_conta on cliente_conta.id_cliente = cliente.id
join conta on conta.id = cliente_conta.id_conta
join agencia on agencia.id = conta.id_agencia
join banco on banco.id = agencia.id_banco
join tipo on tipo.id = conta.id_tipo;

--cliente conta emprestimos
select cliente.*, 
    conta.numero as conta_numero, 
    tipo.nome as tipo, 
    agencia.nome as agencia_nome, 
    agencia.numero as agencia_numero, 
    banco.nome as banco_nome, 
    banco.codigo as banco_codigo, 
    conta.saldo as saldo,
    emprestimo.valor as emprestimo_aprovados,
    emprestimo.nome as nome_emprestimo,
    colaborador.nome,
    funcao.nome
from cliente
join cliente_conta on cliente_conta.id_cliente = cliente.id
join conta on conta.id = cliente_conta.id_conta
join agencia on agencia.id = conta.id_agencia
join banco on banco.id = agencia.id_banco
join tipo on tipo.id = conta.id_tipo
join conta_emprestimo on conta_emprestimo.id_conta = conta.id
join emprestimo on emprestimo.id = conta_emprestimo.id_emprestimo
join aprovador_emprestimo on aprovador_emprestimo.id_emprestimo = emprestimo.id
join colaborador on colaborador.id = aprovador_emprestimo.id_colaborador_aprovador
join funcao on funcao.id = colaborador.id_funcao;
