DROP TABLE cliente CASCADE CONSTRAINTS PURGE;
DROP SEQUENCE cliente_seq;
DROP TABLE colaborador CASCADE CONSTRAINTS PURGE;
DROP SEQUENCE colaborador_seq;
DROP TABLE parceiro CASCADE CONSTRAINTS PURGE;
DROP SEQUENCE parceiro_seq;
DROP TABLE rua CASCADE CONSTRAINTS PURGE;
DROP SEQUENCE rua_seq;
DROP TABLE cidade CASCADE CONSTRAINTS PURGE;
DROP SEQUENCE cidade_seq;
DROP TABLE estado CASCADE CONSTRAINTS PURGE;
DROP SEQUENCE estado_seq;
DROP TABLE pais CASCADE CONSTRAINTS PURGE;
DROP SEQUENCE pais_seq;
DROP TABLE endereco CASCADE CONSTRAINTS PURGE;
DROP SEQUENCE endereco_seq;
DROP TABLE conta CASCADE CONSTRAINTS PURGE;
DROP SEQUENCE conta_seq;
DROP TABLE agencia CASCADE CONSTRAINTS PURGE;
DROP SEQUENCE agencia_seq;
DROP TABLE banco CASCADE CONSTRAINTS PURGE;
DROP SEQUENCE banco_seq;
DROP TABLE movimentacao CASCADE CONSTRAINTS PURGE;
DROP SEQUENCE movimentacao_seq;
DROP TABLE tipo CASCADE CONSTRAINTS PURGE;
DROP SEQUENCE tipo_seq;
DROP TABLE cliente_conta CASCADE CONSTRAINTS PURGE;
DROP SEQUENCE cliente_conta_seq;
DROP TABLE emprestimo CASCADE CONSTRAINTS PURGE;
DROP SEQUENCE emprestimo_seq;
DROP TABLE aprovador_emprestimo CASCADE CONSTRAINTS PURGE;
DROP SEQUENCE aprovador_emprestimo_seq;
DROP TABLE conta_emprestimo CASCADE CONSTRAINTS PURGE;
DROP SEQUENCE conta_emprestimo_seq;
DROP TABLE contato CASCADE CONSTRAINTS PURGE;
DROP SEQUENCE contato_seq;
DROP TABLE contato_tipo CASCADE CONSTRAINTS PURGE;
DROP SEQUENCE contato_tipo_seq;
DROP TABLE documento CASCADE CONSTRAINTS PURGE;
DROP SEQUENCE documento_seq;
DROP TABLE documento_tipo CASCADE CONSTRAINTS PURGE;
DROP SEQUENCE documento_tipo_seq;
DROP TABLE funcao CASCADE CONSTRAINTS PURGE;
DROP SEQUENCE funcao_seq;


-------------------------------------------------------------------------------
--            cliente
-------------------------------------------------------------------------------
CREATE TABLE cliente (
  id NUMBER NOT NULL, 
  nome VARCHAR2(150) NOT NULL, 
  PRIMARY KEY (id)
);
-- Generete ID usando sequence
CREATE SEQUENCE cliente_seq START WITH 1 INCREMENT BY 1;
-------------------------------------------------------------------------------
--            colaborador
-------------------------------------------------------------------------------
CREATE TABLE colaborador (
  id NUMBER NOT NULL, 
  nome VARCHAR2(150) NOT NULL, 
  id_funcao NUMBER NOT NULL, 
  PRIMARY KEY (id)
);
-- Generete ID usando sequence
CREATE SEQUENCE colaborador_seq START WITH 1 INCREMENT BY 1;
-------------------------------------------------------------------------------
--            parceiro
-------------------------------------------------------------------------------
CREATE TABLE parceiro (
  id NUMBER NOT NULL, 
  nome VARCHAR2(150) NOT NULL, 
  PRIMARY KEY (id)
);
-- Generete ID usando sequence
CREATE SEQUENCE parceiro_seq START WITH 1 INCREMENT BY 1;
-------------------------------------------------------------------------------
--            rua
-------------------------------------------------------------------------------
CREATE TABLE rua (
  id NUMBER NOT NULL, 
  id_cidade NUMBER NOT NULL, 
  nome VARCHAR2(150) NOT NULL, 
  numero NUMBER(10) NOT NULL, 
  codigo_postal NUMBER NOT NULL, 
  complemento VARCHAR2(50), 
  PRIMARY KEY (id)
);
-- Generete ID usando sequence
CREATE SEQUENCE rua_seq START WITH 1 INCREMENT BY 1;
-------------------------------------------------------------------------------
--            cidade
-------------------------------------------------------------------------------
CREATE TABLE cidade (
  id NUMBER NOT NULL, 
  id_estado NUMBER NOT NULL, 
  nome VARCHAR2(150) NOT NULL, 
  PRIMARY KEY (id)
);
-- Generete ID usando sequence
CREATE SEQUENCE cidade_seq START WITH 1 INCREMENT BY 1;
-------------------------------------------------------------------------------
--            estado
-------------------------------------------------------------------------------
CREATE TABLE estado (
  id NUMBER NOT NULL, 
  id_pais NUMBER NOT NULL, 
  nome VARCHAR2(150) NOT NULL, 
  sigla VARCHAR2(10) NOT NULL, 
  PRIMARY KEY (id)
);
-- Generete ID usando sequence
CREATE SEQUENCE estado_seq START WITH 1 INCREMENT BY 1;
-------------------------------------------------------------------------------
--            pais
-------------------------------------------------------------------------------
CREATE TABLE pais (
  id NUMBER NOT NULL, 
  nome VARCHAR2(150) NOT NULL, 
  sigla VARCHAR2(10) NOT NULL, 
  PRIMARY KEY (id)
);
-- Generete ID usando sequence
CREATE SEQUENCE pais_seq START WITH 1 INCREMENT BY 1;
-------------------------------------------------------------------------------
--            endereco
-------------------------------------------------------------------------------
CREATE TABLE endereco (
  id NUMBER NOT NULL, 
  id_rua NUMBER NOT NULL, 
  id_parceiro NUMBER, 
  id_colaborador NUMBER, 
  id_cliente NUMBER, 
  PRIMARY KEY (id)
);
-- Generete ID usando sequence
CREATE SEQUENCE endereco_seq START WITH 1 INCREMENT BY 1;
-------------------------------------------------------------------------------
--            conta
-------------------------------------------------------------------------------
CREATE TABLE conta (
  id NUMBER NOT NULL, 
  id_tipo NUMBER NOT NULL, 
  id_agencia NUMBER NOT NULL, 
  numero NUMBER NOT NULL, 
  saldo NUMBER DEFAULT 0.0 NOT NULL, 
  PRIMARY KEY (id)
);
-- Generete ID usando sequence
CREATE SEQUENCE conta_seq START WITH 1 INCREMENT BY 1;
-------------------------------------------------------------------------------
--            agencia
-------------------------------------------------------------------------------
CREATE TABLE agencia (
  id NUMBER NOT NULL, 
  id_banco NUMBER NOT NULL, 
  nome VARCHAR2(150) NOT NULL, 
  numero VARCHAR2(50) NOT NULL, 
  PRIMARY KEY (id)
);
-- Generete ID usando sequence
CREATE SEQUENCE agencia_seq START WITH 1 INCREMENT BY 1;
-------------------------------------------------------------------------------
--            banco
-------------------------------------------------------------------------------
CREATE TABLE banco (
  id NUMBER NOT NULL, 
  nome VARCHAR2(150) NOT NULL, 
  codigo VARCHAR2(10) NOT NULL, 
  PRIMARY KEY (id)
);
-- Generete ID usando sequence
CREATE SEQUENCE banco_seq START WITH 1 INCREMENT BY 1;
-------------------------------------------------------------------------------
--            movimentacao
-------------------------------------------------------------------------------
CREATE TABLE movimentacao (
  id NUMBER NOT NULL, 
  id_conta_origem NUMBER, 
  id_conta_destino NUMBER, 
  data DATE NOT NULL, 
  valor NUMBER NOT NULL, 
  PRIMARY KEY (id)
);
-- Generete ID usando sequence
CREATE SEQUENCE movimentacao_seq START WITH 1 INCREMENT BY 1;
-------------------------------------------------------------------------------
--            tipo
-------------------------------------------------------------------------------
CREATE TABLE tipo (
  id NUMBER NOT NULL, 
  nome VARCHAR2(100) NOT NULL, 
  PRIMARY KEY (id)
);
-- Generete ID usando sequence
CREATE SEQUENCE tipo_seq START WITH 1 INCREMENT BY 1;
-------------------------------------------------------------------------------
--            cliente_conta
-------------------------------------------------------------------------------
CREATE TABLE cliente_conta (
  id NUMBER, 
  id_cliente NUMBER NOT NULL, 
  id_conta NUMBER NOT NULL, 
  PRIMARY KEY (id)
);
-- Generete ID usando sequence
CREATE SEQUENCE cliente_conta_seq START WITH 1 INCREMENT BY 1;
-------------------------------------------------------------------------------
--            emprestimo
-------------------------------------------------------------------------------
CREATE TABLE emprestimo (
  id NUMBER NOT NULL, 
  nome VARCHAR2(150) NOT NULL, 
  valor NUMBER DEFAULT 0 NOT NULL, 
  PRIMARY KEY (id)
);
-- Generete ID usando sequence
CREATE SEQUENCE emprestimo_seq START WITH 1 INCREMENT BY 1;
-------------------------------------------------------------------------------
--            aprovador_emprestimo
-------------------------------------------------------------------------------
CREATE TABLE aprovador_emprestimo (
  id NUMBER, 
  id_emprestimo NUMBER NOT NULL, 
  id_colaborador_aprovador NUMBER NOT NULL, 
  PRIMARY KEY (id)
);
-- Generete ID usando sequence
CREATE SEQUENCE aprovador_emprestimo_seq START WITH 1 INCREMENT BY 1;
-------------------------------------------------------------------------------
--            conta_emprestimo
-------------------------------------------------------------------------------
CREATE TABLE conta_emprestimo (
  id NUMBER, 
  id_emprestimo NUMBER NOT NULL, 
  id_conta NUMBER NOT NULL, 
  PRIMARY KEY (id)
);
-- Generete ID usando sequence
CREATE SEQUENCE conta_emprestimo_seq START WITH 1 INCREMENT BY 1;
-------------------------------------------------------------------------------
--            contato
-------------------------------------------------------------------------------
CREATE TABLE contato (
  id NUMBER NOT NULL, 
  id_contato_tipo NUMBER NOT NULL, 
  id_cliente NUMBER, 
  id_colaborador NUMBER, 
  id_parceiro NUMBER, 
  valor VARCHAR2(100) NOT NULL, 
  PRIMARY KEY (id)
);
-- Generete ID usando sequence
CREATE SEQUENCE contato_seq START WITH 1 INCREMENT BY 1;
-------------------------------------------------------------------------------
--            contato_tipo
-------------------------------------------------------------------------------
CREATE TABLE contato_tipo (
  id NUMBER NOT NULL, 
  nome VARCHAR2(100) NOT NULL, 
  PRIMARY KEY (id)
);

-- Generete ID usando sequence
CREATE SEQUENCE contato_tipo_seq START WITH 1 INCREMENT BY 1;

-------------------------------------------------------------------------------
--            documento
-------------------------------------------------------------------------------
CREATE TABLE documento (
  id NUMBER NOT NULL,
  id_documento_tipo NUMBER NOT NULL,
  id_cliente NUMBER, 
  id_parceiro NUMBER, 
  id_colaborador NUMBER, 
  valor VARCHAR2(100) NOT NULL, 
  PRIMARY KEY (id)
);
-- Generete ID usando sequence
CREATE SEQUENCE documento_seq START WITH 1 INCREMENT BY 1;
-------------------------------------------------------------------------------
--            documento_tipo
-------------------------------------------------------------------------------
CREATE TABLE documento_tipo (
  id NUMBER NOT NULL, 
  nome VARCHAR2(150) NOT NULL,
  PRIMARY KEY (id)
);
-- Generete ID usando sequence
CREATE SEQUENCE documento_tipo_seq START WITH 1 INCREMENT BY 1;
-------------------------------------------------------------------------------
--            funcao
-------------------------------------------------------------------------------
CREATE TABLE funcao (
  id NUMBER NOT NULL, 
  nome VARCHAR2(150) NOT NULL, 
  PRIMARY KEY (id)
);
-- Generete ID usando sequence
CREATE SEQUENCE funcao_seq START WITH 1 INCREMENT BY 1;
-------------------------------------------------------------------------------

-- ---
-- Foreign Keys 
-- ---
ALTER TABLE colaborador ADD FOREIGN KEY (id_funcao) REFERENCES funcao(id);
ALTER TABLE rua ADD FOREIGN KEY (id_cidade) REFERENCES cidade(id);
ALTER TABLE cidade ADD FOREIGN KEY (id_estado) REFERENCES estado(id);
ALTER TABLE estado ADD  FOREIGN KEY (id_pais) REFERENCES pais(id);
ALTER TABLE endereco ADD FOREIGN KEY (id_rua) REFERENCES rua(id);
ALTER TABLE endereco ADD FOREIGN KEY (id_parceiro) REFERENCES parceiro(id);
ALTER TABLE endereco ADD FOREIGN KEY (id_colaborador) REFERENCES colaborador(id);
ALTER TABLE endereco ADD FOREIGN KEY (id_cliente) REFERENCES cliente(id);
ALTER TABLE conta ADD FOREIGN KEY (id_tipo) REFERENCES tipo(id);
ALTER TABLE conta ADD FOREIGN KEY (id_agencia) REFERENCES agencia (id);
ALTER TABLE agencia ADD FOREIGN KEY (id_banco) REFERENCES banco (id);
ALTER TABLE movimentacao ADD FOREIGN KEY (id_conta_origem) REFERENCES conta(id);
ALTER TABLE movimentacao ADD FOREIGN KEY (id_conta_destino) REFERENCES conta(id);
ALTER TABLE cliente_conta ADD  FOREIGN KEY (id_cliente) REFERENCES cliente(id);
ALTER TABLE cliente_conta ADD  FOREIGN KEY (id_conta) REFERENCES conta(id);
ALTER TABLE aprovador_emprestimo ADD FOREIGN KEY (id_emprestimo) REFERENCES emprestimo(id);
ALTER TABLE aprovador_emprestimo ADD FOREIGN KEY (id_colaborador_aprovador) REFERENCES colaborador (id);
ALTER TABLE conta_emprestimo ADD  FOREIGN KEY (id_emprestimo) REFERENCES emprestimo(id);
ALTER TABLE conta_emprestimo ADD FOREIGN KEY (id_conta) REFERENCES conta(id);
ALTER TABLE contato ADD FOREIGN KEY (id_contato_tipo) REFERENCES contato_tipo(id);
ALTER TABLE contato ADD FOREIGN KEY (id_cliente) REFERENCES cliente(id);
ALTER TABLE contato ADD FOREIGN KEY (id_colaborador) REFERENCES colaborador(id);
ALTER TABLE contato ADD  FOREIGN KEY (id_parceiro) REFERENCES parceiro(id);
ALTER TABLE documento ADD FOREIGN KEY (id_documento_tipo) REFERENCES documento_tipo(id);
ALTER TABLE documento ADD FOREIGN KEY (id_cliente) REFERENCES cliente(id);
ALTER TABLE documento ADD  FOREIGN KEY (id_parceiro) REFERENCES parceiro(id);
ALTER TABLE documento ADD  FOREIGN KEY (id_colaborador) REFERENCES colaborador(id);