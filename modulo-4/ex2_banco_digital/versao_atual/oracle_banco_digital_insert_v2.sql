-- ---
-- Test Data
-- ---

/*
Pessoa 1: Paulo da Silva, Canoas, RS, Rua Pedro Zanetti, 405, ap234, 22222222-22, (51)111111111
gerente, banco do brasil, ag 234, Agencia Centro, 


*/

INSERT INTO pais (id,nome,sigla) VALUES
(pais_seq.nextval,'Brasil','BRL');

INSERT INTO estado (id,nome,id_pais,sigla) VALUES
(estado_seq.nextval,'Rio Grande do Sul',1,'RS');

INSERT INTO cidade (id,nome,id_estado) VALUES
(cidade_seq.nextval,'Canoas',1);
INSERT INTO cidade (id,nome,id_estado) VALUES
(cidade_seq.nextval,'Porto Alegre',1);

INSERT INTO rua (id, id_cidade, nome, numero, codigo_postal, complemento) VALUES
(rua_seq.nextval,1,'Dom Pedro II',123,87210123,'apt209B');
INSERT INTO rua (id, id_cidade, nome, numero, codigo_postal) VALUES
(rua_seq.nextval,2,'Zannetti',111,555555);

INSERT INTO cliente (id,nome) VALUES
(cliente_seq.nextval,'Paulo da Silva');
INSERT INTO cliente (id,nome) VALUES
(cliente_seq.nextval,'Maria Fernandes');

INSERT INTO endereco (id, id_rua, id_cliente) VALUES
(endereco_seq.nextval,1,1);
INSERT INTO endereco (id, id_rua, id_cliente) VALUES
(endereco_seq.nextval,2,2);

INSERT INTO banco (id,nome,codigo) VALUES
(banco_seq.nextval,'Banco do Brasil',1);

INSERT INTO tipo (id,nome) VALUES
(tipo_seq.nextval,'Conta Corrente');
INSERT INTO tipo (id,nome) VALUES
(tipo_seq.nextval,'Conta Salário');
INSERT INTO tipo (id,nome) VALUES
(tipo_seq.nextval,'Conta Conjunta');

INSERT INTO agencia (id,id_banco, nome,numero) VALUES
(agencia_seq.nextval,1,'Agencia Vila Madalena', '2222-2');

INSERT INTO conta (id,numero,id_tipo,id_agencia,saldo) VALUES
(conta_seq.nextval,12345,1,1,500.00);
INSERT INTO conta (id,numero,id_tipo,id_agencia,saldo) VALUES
(conta_seq.nextval,11111,2,1,200.00);

INSERT INTO movimentacao (id,data,valor,id_conta_origem,id_conta_destino) VALUES
(movimentacao_seq.nextval,TO_DATE('2019/02/03 21:02:44', 'yyyy/mm/dd hh24:mi:ss'),100.00,1,2);

INSERT INTO documento_tipo (id,nome) VALUES
(documento_tipo_seq.nextval,'RG');
INSERT INTO documento_tipo (id,nome) VALUES
(documento_tipo_seq.nextval,'CPF');
INSERT INTO documento_tipo (id,nome) VALUES
(documento_tipo_seq.nextval,'Registro Civil');
INSERT INTO documento_tipo (id,nome) VALUES
(documento_tipo_seq.nextval,'CNPJ');

INSERT INTO documento (id,id_documento_tipo,id_cliente,valor) VALUES
(documento_seq.nextval,1,1,'123459082');
INSERT INTO documento (id,id_documento_tipo,id_cliente,valor) VALUES
(documento_seq.nextval,2,1,'333333333');
INSERT INTO documento (id,id_documento_tipo,id_cliente,valor) VALUES
(documento_seq.nextval,1,2,'00000000');
INSERT INTO documento (id,id_documento_tipo,id_cliente,valor) VALUES
(documento_seq.nextval,2,2,'777777777');

INSERT INTO contato_tipo (id,nome) VALUES
(contato_tipo_seq.nextval,'Celular/Whatsapp');
INSERT INTO contato_tipo (id,nome) VALUES
(contato_tipo_seq.nextval,'E-mail pessoal');
INSERT INTO contato_tipo (id,nome) VALUES
(contato_tipo_seq.nextval,'E-mail profissional');

INSERT INTO contato (id,id_cliente,valor,id_contato_tipo) VALUES
(contato_seq.nextval,1,'51876564534',1);
INSERT INTO contato (id,id_cliente,valor,id_contato_tipo) VALUES
(contato_seq.nextval,2,'sq@gmail.com',2);


INSERT INTO funcao (id,nome) VALUES
(funcao_seq.nextval,'Gerente');
INSERT INTO funcao (id,nome) VALUES
(funcao_seq.nextval,'Gerente Geral');
INSERT INTO funcao (id,nome) VALUES
(funcao_seq.nextval,'Supervisor');

INSERT INTO colaborador (id,nome, id_funcao) VALUES
(colaborador_seq.nextval,'Marcio Feliciano',1);
INSERT INTO colaborador (id,nome, id_funcao) VALUES
(colaborador_seq.nextval,'Marta Feliciano',2);

INSERT INTO cliente_conta (id,id_cliente,id_conta) VALUES
(cliente_conta_seq.nextval,1,1);

INSERT INTO parceiro (id,nome) VALUES
(parceiro_seq.nextval,'XP Investimentos');
INSERT INTO documento (id, id_documento_tipo, id_parceiro, valor) VALUES
(documento_seq.nextval,4,1,'456784322567');

INSERT INTO emprestimo (id,nome,valor) VALUES
(emprestimo_seq.nextval,'Bom pagador', 10000.00);

INSERT INTO conta_emprestimo(id, id_emprestimo, id_conta) VALUES
(conta_emprestimo_seq.nextval, 1, 1);


INSERT INTO aprovador_emprestimo (id, id_emprestimo, id_colaborador_aprovador) VALUES
(aprovador_emprestimo_seq.nextval,1,1);

-- INSERT INTO situacao (id,valor) VALUES
-- (situacao_seq.nextval,'Aprovado');
-- INSERT INTO situacao (id,valor) VALUES
-- (situacao_seq.nextval,'Negado');
-- INSERT INTO situacao (id,valor) VALUES
-- (situacao_seq.nextval,'Aguardando assinaturas');