package br.com.dbc.contadigital.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class OperacaoBd {

    public static void insert(String sql) {
        Connection conn = Connector.connect();
        try {
            PreparedStatement pst = conn.prepareStatement(sql);
            //pst.setString(1, nome);
            //pst.setString(2, codigo);
            pst.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, "Erro no insert", ex);
        }
        System.out.println("Executado!");
    }

    //parametros = [nomeTabela: "banco", nome: "Banco do Brasil", codigo:"001"]
    /*public static void insert(String nomeTabela, HashMap<String, String> parametros) {;
        Connection conn = Connector.connect();
        String s = "";
        try {
            HashMap<String, String> valores = parametros;
            for (Map.Entry<String, String> valor : valores.entrySet()) {
                s = "INSERT INTO" + nomeTabela + "(id, " + valor.getKey() + ", " + ") VALUES("+nomeTabela+"_seq.nextval,"+valor.getValue()+")";
            }
            PreparedStatement pst = conn.prepareStatement(s);
            for (Map.Entry<String, String> valor : valores.entrySet()) {
                pst.setString(1, valor.getValue());
                pst.setString(1, codigo);
                pst.executeUpdate();
            }

        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, "Erro no insert", ex);
        }
        System.out.println("Executado!");
    }*/
}
