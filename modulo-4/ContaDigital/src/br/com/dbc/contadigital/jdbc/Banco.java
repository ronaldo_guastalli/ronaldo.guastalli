package br.com.dbc.contadigital.jdbc;

public class Banco {
    private String nome;
    private String codigo;
    
    public Banco(String nome, String codigo){
        this.nome = nome;
        this.codigo = codigo;
        
        String nomeDaTabela = "Banco";
        String primeiroParenteses = "id, nome, codigo";
        String depoisDoValue = "(banco_seq.nextval, " + nome + ", " + codigo + ")";
        String insertCommand = "insert into %s(%s) values (%s)";
        String sql = String.format(insertCommand, "banco", "id, nome, codigo", "banco_seq.nextval, 'Itau', '123'");
        OperacaoBd.insert(sql);     
        
    }
    
        //OperacaoBd.insertBanco("banco", nome, codigo);
        //OperacaoBd.insert(nomeDaTabela, primeiroParenteses, depoisDoValue);
    
}
