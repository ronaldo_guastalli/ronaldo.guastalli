
package br.com.dbc.contadigital.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
//import java.sql.DriverManager;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Connector {

    private static Connection conn;

    public static Connection connect() {
        try {
            if(conn != null && conn.isValid(10)){
                return conn;
            }
            Class.forName("oracle.jdbc.driver.OracleDriver");
            conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE",
                    "banco_digital",
                    "bancodigital");
        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
            Logger.getLogger(Connector.class.getName()).log(Level.SEVERE, "Erro de conexão!", ex);
        } finally {
            System.out.println("Terminou");
        }
        return conn;
    }
}
