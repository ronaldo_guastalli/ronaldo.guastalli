
package br.com.dbc.lotr.dao;

import br.com.dbc.lotr.entity.ElfoJoin;
import br.com.dbc.lotr.entity.HobbitJoin;
import br.com.dbc.lotr.entity.Usuario;
import br.com.dbc.lotr.dto.PersonagemDTO;


public class HobbitDAO extends AbstractDAO<HobbitJoin> {

    public HobbitJoin parseFrom(PersonagemDTO dto, Usuario usuario) {
        HobbitJoin hobbitEntity = null;
        if (dto.getId() != null) {
            hobbitEntity = buscar(dto.getId());
        }
        if (hobbitEntity == null) {
            hobbitEntity = new HobbitJoin();
        }
        hobbitEntity.setDanoHobbit(dto.getDanoElfo());
        hobbitEntity.setNome(dto.getNome());
        hobbitEntity.setRaca(dto.getRaca());
        hobbitEntity.setUsuario(usuario);
        return hobbitEntity;
    }

    @Override
    protected Class<HobbitJoin> getEntityClass() {
        return HobbitJoin.class;
    }
    

    
}
