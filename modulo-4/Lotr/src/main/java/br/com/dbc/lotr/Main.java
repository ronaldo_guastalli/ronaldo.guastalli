package br.com.dbc.lotr;

import br.com.dbc.lotr.entity.Contato;
import br.com.dbc.lotr.entity.ElfoJoin;
import br.com.dbc.lotr.entity.ElfoPerClass;
import br.com.dbc.lotr.entity.ElfoTabelao;
import br.com.dbc.lotr.entity.Endereco;
import br.com.dbc.lotr.entity.HobbitJoin;
import br.com.dbc.lotr.entity.HobbitPerClass;
import br.com.dbc.lotr.entity.HobbitTabelao;
import br.com.dbc.lotr.entity.RacaType;
import br.com.dbc.lotr.entity.TipoContato;
import br.com.dbc.lotr.entity.Usuario;
import br.com.dbc.lotr.dto.EnderecoDTO;
import br.com.dbc.lotr.dto.PersonagemDTO;
import br.com.dbc.lotr.dto.UsuarioPersonagemDTO;
import br.com.dbc.lotr.service.UsuarioService;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

public class Main {

    private static final Logger LOG = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {
        UsuarioService usuarioService = new UsuarioService();
        UsuarioPersonagemDTO dto = new UsuarioPersonagemDTO();
        dto.setApelidoUsuario("Gustavo");
        dto.setNomeUsuario("Gustavo");
        dto.setCpfUsuario(123l);
        dto.setSenhaUsuario("123");
        EnderecoDTO enderecoDTO = new EnderecoDTO();
        enderecoDTO.setLogradouro("Rua do Gustavo");
        enderecoDTO.setNumero(123);
        enderecoDTO.setBairro("Bairro do Gustava");
        enderecoDTO.setCidade("Cidade do Gustavo");
        enderecoDTO.setComplemento("CEP 123");
        dto.setEndereco(enderecoDTO);
        PersonagemDTO personagemDTO = new PersonagemDTO();
        personagemDTO.setRaca(RacaType.ELFO);
        personagemDTO.setNome("Elfo do Gustavo");
        personagemDTO.setDanoElfo(123d);
        dto.setPersonagem(personagemDTO);
        usuarioService.cadastrarUsuarioEPersonagem(dto);
        System.exit(0);
    }
    public static void oldMain(String[] args) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSession();
            transaction = session.beginTransaction();
            Usuario usuario = new Usuario();
            usuario.setNome("Priscila");
            usuario.setCpf(123l);
            usuario.setSenha("456");
            usuario.setApelido("pri");

            Endereco endereco = new Endereco();
            endereco.setLogradouro("Pedro Zanetti");
            endereco.setBairro("Igara");
            endereco.setCidade("Canoas");
            endereco.setNumero(200);
            endereco.setComplemento("ap34");

            Endereco endereco2 = new Endereco();
            endereco2.setLogradouro("Pedro Zanetti");
            endereco2.setBairro("Igara");
            endereco2.setCidade("Canoas");
            endereco2.setNumero(400);
            endereco2.setComplemento("ap34");

            TipoContato tipoContato = new TipoContato();
            tipoContato.setNome("celular");
            tipoContato.setQuantidade(10);

            Contato contato = new Contato();
            contato.setTipoContato(tipoContato);
            contato.setUsuario(usuario);
            contato.setValor("518888888888");

            usuario.pushContatos(contato);
            usuario.pushEnderecos(endereco, endereco2);

            tipoContato.setContato(contato);

            session.save(usuario);

            //PersonagemTabelao personagem = new PersonagemTabelao();
            ElfoTabelao elfoTabelao = new ElfoTabelao();
            elfoTabelao.setDanoElfo(100d);
            elfoTabelao.setNome("Frodo");
            //elfoTabelao.setRaca(RacaType.ELFO);
            session.save(elfoTabelao);

            HobbitTabelao hobbitTabelao = new HobbitTabelao();
            hobbitTabelao.setDanoHobbit(10d);
            hobbitTabelao.setNome("Frodo Bonceiro");
            //hobbitTabelao.setRaca(RacaType.ANAO);
            session.save(hobbitTabelao);

            ElfoPerClass elfoPerClass = new ElfoPerClass();
            elfoPerClass.setNome("Legolas Per Class");
            elfoPerClass.setDanoElfo(100d);
            session.save(elfoPerClass);

            HobbitPerClass hobbitPerClass = new HobbitPerClass();
            hobbitPerClass.setNome("Frod Per Class");
            hobbitPerClass.setDanoHobbit(100d);
            session.save(hobbitPerClass);

            ElfoJoin elfoJoin = new ElfoJoin();
            elfoJoin.setNome("Legolas Join");
            elfoJoin.setDanoElfo(100d);
            session.save(elfoJoin);

            HobbitJoin hobbitJoin = new HobbitJoin();
            hobbitJoin.setNome("Frod Join");
            hobbitJoin.setDanoHobbit(100d);
            session.save(hobbitJoin);

            Criteria criteria = session.createCriteria(Usuario.class);
            criteria.createAlias("enderecos", "endereco");
            //criteria.add(Restrictions.ilike("endereco.bairro", "%ga%" ));
            criteria.add(Restrictions
                    .and(
                            Restrictions.ilike("endereco.bairro", "%ga%"),
                            Restrictions.ilike("endereco.cidade", "%noa%")));
            List<Usuario> usuarios = criteria.list();
            usuarios.forEach(System.out::println);
            //usuarios.forEach((Usuario u)=>System.out.println(u));

            criteria = session.createCriteria(Usuario.class);
            criteria.createAlias("enderecos", "endereco");
            //criteria.add(Restrictions.ilike("endereco.bairro", "%ga%" ));

            criteria.add(Restrictions
                    .and(
                            Restrictions.ilike("endereco.bairro", "%ga%"),
                            Restrictions.ilike("endereco.cidade", "%noa%")));
            criteria.setProjection(Projections.count("id"));
            System.out.println(String
                    .format("Foram encontado(s) %s registro(s) "
                            + "com critérios especificados", criteria.uniqueResult()));

            usuarios = session.createQuery("select u from Usuario u "
                    + " join u.enderecos endereco "
                    + "where lower(endereco.cidade) like '%noas%' "
                    + " and lower(endereco.bairro) like '%iga%' ").list();
            usuarios.forEach(System.out::println);

            Criteria criteria3 = session.createCriteria(Usuario.class);
            criteria3.setProjection(Projections.rowCount());
            System.out.println(String.format("Foram encontrados %s registro(s) com rowCount.", criteria3.uniqueResult()));

            Long count = (Long) session
                    .createQuery("select count(distinct u.id) from Usuario u "
                            + " join u.enderecos endereco "
                            + " where lower(endereco.cidade) like '%noa%' "
                            + " and lower(endereco.bairro) like '%gar%' ").uniqueResult();
            System.out.println(String.format("Contamos com HQL %s valores", count));
            count = (Long) session
                    .createQuery("select count(endereco.id) from Usuario u "
                            + " join u.enderecos endereco "
                            + " where lower(endereco.cidade) like '%noa%' "
                            + " and lower(endereco.bairro) like '%gar%' ").uniqueResult();
            System.out.println(String.format("Contamos com HQL %s valores", count));
            Long sum = (Long) session
                    .createQuery("select sum(endereco.numero) from Usuario u "
                            + " join u.enderecos endereco "
                            + " where lower(endereco.cidade) like '%noa%' "
                            + " and lower(endereco.bairro) like '%gar%' ").uniqueResult();
            System.out.println(String.format("Somamos com HQL os numeros de dois endereço; %s (usando sum).", sum));
            
//            Criteria criteria4 = session.createCriteria(Usuario.class);
//                        criteria4.setProjection(Projections.sum("endereco.numero"));
//            System.out.println(String.format("Somamos com setProjection os numeros de dois endereço; %f (usando sum).", criteria4.uniqueResult()));

            transaction.commit();
            System.exit(0);
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
            System.exit(1);
        } finally {
            if (session != null) {
                session.close();
            }
        }

    }
}
