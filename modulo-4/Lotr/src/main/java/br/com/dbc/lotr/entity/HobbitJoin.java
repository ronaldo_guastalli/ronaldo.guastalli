
package br.com.dbc.lotr.entity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "HOBBIT_JOIN")
@PrimaryKeyJoinColumn(name = "id_personagem")
public class HobbitJoin extends PersonagemJoin{
    
    @Column(name = "DANO_HOBBIT")
    private Double danoHobbit;

    
    public Double getDanoHobbit() {
        return danoHobbit;
    }

    public void setDanoHobbit(Double danoHobbit) {
        this.danoHobbit = danoHobbit;
    }

    public HobbitJoin() {
        super.setRaca(RacaType.HOBBIT);
    }
    
    
    
    
}
