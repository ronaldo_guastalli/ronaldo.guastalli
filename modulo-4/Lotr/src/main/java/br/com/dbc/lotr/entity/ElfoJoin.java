
package br.com.dbc.lotr.entity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "ELFO_JOIN")
@PrimaryKeyJoinColumn(name = "id_personagem")
public class ElfoJoin extends PersonagemJoin{
    
    @Column(name = "DANO_ELFO")
    private Double danoElfo;

    public ElfoJoin() {
        super.setRaca(RacaType.ELFO);
    }   
    

    public Double getDanoElfo() {
        return danoElfo;
    }

    public void setDanoElfo(Double danoElfo) {
        this.danoElfo = danoElfo;
    }
    
    
}
