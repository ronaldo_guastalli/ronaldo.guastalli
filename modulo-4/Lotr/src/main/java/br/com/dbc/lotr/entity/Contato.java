package br.com.dbc.lotr.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "CONTATO")
@SequenceGenerator(allocationSize = 1, name="contato_seq", sequenceName = "contato_seq")
public class Contato {
   @Id
   @GeneratedValue(generator = "contato_seq", strategy = GenerationType.SEQUENCE)
   private Integer id;   
   
   @ManyToOne
   @JoinColumn(name = "id_usuario")
   private Usuario usuario;
   
   @OneToOne(cascade = CascadeType.ALL)
   @JoinColumn(name = "id_tipo_contato")
   private TipoContato tipoContato;
  
   private String valor;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public TipoContato getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(TipoContato tipoContato) {
        this.tipoContato = tipoContato;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

   
   
   

   
   

    
   
   
   
    
}
