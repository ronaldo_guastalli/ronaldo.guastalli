
package br.com.dbc.lotr.dto;


public class UsuarioPersonagemDTO {
    //segue contrato do front end
    private Integer idUsuario;
    private String nomeUsuario;
    private String apelidoUsuario;
    private String senhaUsuario;
    private Long cpfUsuario;   
    
    //Endereco
    private EnderecoDTO endereco;
    
    //Personagem
    private PersonagemDTO personagem;

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNomeUsuario() {
        return nomeUsuario;
    }

    public void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }

    public String getApelidoUsuario() {
        return apelidoUsuario;
    }

    public void setApelidoUsuario(String apelidoUsuario) {
        this.apelidoUsuario = apelidoUsuario;
    }

    public String getSenhaUsuario() {
        return senhaUsuario;
    }

    public void setSenhaUsuario(String senhaUsuario) {
        this.senhaUsuario = senhaUsuario;
    }

    public Long getCpfUsuario() {
        return cpfUsuario;
    }

    public void setCpfUsuario(Long cpfUsuario) {
        this.cpfUsuario = cpfUsuario;
    }

    public EnderecoDTO getEndereco() {
        return endereco;
    }

    public void setEndereco(EnderecoDTO endereco) {
        this.endereco = endereco;
    }

    public PersonagemDTO getPersonagem() {
        return personagem;
    }

    public void setPersonagem(PersonagemDTO personagem) {
        this.personagem = personagem;
    }
    
    
}
