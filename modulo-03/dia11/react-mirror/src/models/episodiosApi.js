import axios from 'axios'

const urlBase = 'http://localhost:9000/api'

// essas duas funções são privadas e servem apenas para reutilizar código, não precisam ser exportadas
const _get = url => new Promise( ( resolve, reject ) => axios.get( url ).then( response => resolve( response.data ) ) )
const _post = ( url, dados ) => new Promise( ( resolve, reject ) => axios.post( url, dados ).then( response => resolve( response.data ) ) )

export default class EpisodiosApi {
  
  buscar() {
    return _get( `${ urlBase }/episodios` )
  }

  buscarEpisodio( id ) {
    return _get( `${ urlBase }/episodios/${ id }` )
  }

  async buscarDetalhes( id ) {
    const response = await _get( `${ urlBase }/episodios/${ id }/detalhes` )
    // acessamos a posição 0 dentro de data pois o JSON-server retornar os detalhes como um array,
    return response[ 0 ]
  }

  registrarNota( parametros ) {
    return _post( `${ urlBase }/notas`, parametros )
  }

  async buscarNota( episodioId ) {
    const response = await _get( `${ urlBase }/notas?episodioId=${ episodioId }` )
    return response[ 0 ]
  }

  buscarNotas() {
    return _get( `${ urlBase }/notas` )
  }

  filtrarPorTermo( termo ) {
    return _get( `${ urlBase }/detalhes?q=${ termo }` )
  }
}
