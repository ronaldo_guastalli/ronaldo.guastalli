import React, { Component } from 'react'
import ListaEpisodiosUi from './shared/listaEpisodiosUi'
import EpisodiosApi from '../models/episodiosApi'
import ListaEpisodios from '../models/listaEpisodios'

export default class ListaAvaliacoes extends Component {
  
  constructor( props ) {
    super( props )
    this.episodiosApi = new EpisodiosApi()
    this.state = {
      avaliados: []
    }
  }

  componentDidMount() {
    let { listaEpisodios } = this.props.location.state

    if ( listaEpisodios.avaliados ) {
      this.setState( {
        avaliados: listaEpisodios.avaliados
      } )
      return
    }

    listaEpisodios = new ListaEpisodios()
    listaEpisodios.todos = this.props.location.state.listaEpisodios.todos
    this.episodiosApi.buscarNotas().then( notas => {
      listaEpisodios.atualizarNotas( notas )
      this.setState( {
        avaliados: listaEpisodios.avaliados
      } )
    } )
    
  }

  render() {
    // TODO: testar getDerivedStateFromProps para evitar utilizar props e state
    //const { listaEpisodios } = this.props.location.state
    const { avaliados } = this.state
    return <React.Fragment>
      <h1>Minhas avaliações</h1>
      <ListaEpisodiosUi listaEpisodios={ avaliados } />
    </React.Fragment>
  }

}
