// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date
let agora = new Date()
// let lista = new Array()
// let pessoa = new Object()
console.log( typeof agora )
console.log( agora.constructor === Date )
console.log( `desde 1970 se passaram: ${ agora.getTime() } millisecs` )
console.log( `dia da semana: ${ agora.getDay() }` )
console.log( `dia do mês: ${ agora.getDate() }` )
console.log( `mês??????? ${ agora.getMonth() }` )
console.log( `mês ${ agora.getMonth() + 1 }` )
const meses = [
  'janeiro', 'fevereiro', 'março', 'abril', 'maio', 'junho', 'julho', 'agosto',
  'setembro', 'outubro', 'novembro', 'dezembro'
]
console.log( `mês!!!! ${ meses[ agora.getMonth() ] }` )
console.log( `ano !!cuidado!! ${ agora.getYear() }`)
console.log( `ano :) ${ agora.getFullYear() }`)
console.log( `iso em utc: ${ agora.toISOString() }` )
