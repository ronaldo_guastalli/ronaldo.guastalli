console.log( moment().format('DD MM YYYY') )
console.log( moment().format('DD.MM.YYYY HH:mm:ss') )
console.log( moment().startOf('day').fromNow() )
console.log( moment().endOf('day').fromNow() )
console.log( moment().subtract(10, 'days').format('DD MM YYYY HH:mm:ss') )
console.log( moment().add(1000, 'days').format('DD MM YYYY HH:mm:ss') )
moment.locale() // consultar cultura configurada no moment
moment.locale('pt-br') // alterar cultura configurada no moment
console.log( moment().format( 'LT' ) )

var now = moment()
console.log( now.tz('America/New_York').format('LLL') )
console.log( now.tz('Africa/Nairobi').format('LLL') )
