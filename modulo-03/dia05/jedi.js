// var luke = {
//   nome: "Luke",
//   atacarComSabre: function() {
//     console.log( `${ this.nome } atacar com sabre!` )
//   }
// }

// var luke = new Jedi('Luke')
// luke.atacarComSabre()
// var maceWindu = new Jedi('Mace Windu')
// maceWindu.atacarComSabre()

// function Jedi(nome) {
//   this.nome = nome
// }

// Jedi.prototype.atacarComSabre = function() {
//   console.log( `${ this.nome } atacar com sabre!` )
// }

class Jedi {
  constructor(nome) {
    this.nome = nome
  }

  // problema de undefined, pois utiliza this da window
  atacarComSabre() {
    setTimeout( function() {
      console.log( `${ this.nome } atacar com sabre!` )
    }, 1000 )
  }

  atacarComBind() {
    setTimeout( function() {
      console.log( `${ this.nome } atacar com sabre!` )
    }.bind(this), 1000 )
  }

  atacarComSelf() {
    // var that = this
    // var thiz = this
    var self = this
    setTimeout( function() {
      console.log( `${ self.nome } atacar com sabre!` )
    }, 1000 )
  }

  atacarComArrowFunctions() {
    setTimeout( () => {
      console.log( `${ this.nome } atacar com sabre!` )
    }, 1000 )
  }

}

var luke = new Jedi('Luke')
luke.atacarComSabre()
luke.atacarComArrowFunctions()
var maceWindu = new Jedi('Mace Windu')
maceWindu.atacarComSabre()
maceWindu.atacarComArrowFunctions()

luke.idade = 23

// for (var campo in obj) {
  // var resultado = replacer(campo, obj[campo])
  // stringFinal += resultado
//}

var jsonSemIdade = JSON.stringify(luke, function( campo, valor ) {
  if ( campo === 'idade' ) {
    return undefined
  }
  return valor
} )

console.log(jsonSemIdade)

var objInterpretado = JSON.parse(jsonSemIdade, function( campo, valor ) {
  if ( campo === 'nome' ) {
    return valor.toUpperCase()
  }
  return valor
} )

console.log(objInterpretado)
