import React, { Component } from 'react';
import './App.css';
import ListaEpisodios from './models/listaEpisodios'

class App extends Component {

  constructor( props ) {
    super( props )
    this.listaEpisodios = new ListaEpisodios()
    // definindo estado inicial de um componente
    this.state = {
      episodio: this.listaEpisodios.episodioAleatorio,
    }
  }

  sortear() {
    const episodio = this.listaEpisodios.episodioAleatorio
    this.setState( {
      episodio,
    } )
  }

  marcarComoAssistido() {
    const { episodio } = this.state
    this.listaEpisodios.marcarComoAssistido( episodio )
    this.setState( {
      episodio,
    } )
  }

  registrarNota( evt ) {
    const { episodio } = this.state
    episodio.avaliar( evt.target.value )
    this.setState( {
      episodio
    } )
  }

  geraCampoDeNota() {
    // https://reactjs.org/docs/conditional-rendering.html
    return (
      <div>
        {
          this.state.episodio.assistido && (
            <div>
              <span>Qual sua nota para este episódio?</span>
              <input type="number" placeholder="1 a 5" onBlur={ this.registrarNota.bind( this ) }></input>
            </div>
          )
        }
      </div>
    )
  }

  render() {
    const { episodio } = this.state

    return (
      <div className="App">
        <header className="App-header">
          <h2>{ episodio.nome }</h2>
          <img src={ episodio.thumbUrl } alt={ episodio.nome }></img>
          <span>Já assisti? { episodio.assistido ? 'sim' : 'não' }, { episodio.qtdVezesAssistido } ve(zes)</span>
          <span>{ episodio.duracaoEmMin }</span>
          <span>{ episodio.temporadaEpisodio }</span>
          <span>{ episodio.nota || 'sem nota' }</span>
          { this.geraCampoDeNota() }
          <div className="botoes">
            <button className="btn verde" onClick={ this.sortear.bind( this ) }>Próximo</button>
            <button className="btn azul" onClick={ this.marcarComoAssistido.bind( this ) }>Já assisti</button>
          </div>
        </header>
      </div>
    );
  }
}

export default App;
