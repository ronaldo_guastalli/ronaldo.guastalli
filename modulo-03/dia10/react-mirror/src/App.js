import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom'
import './App.css';

import PaginaInicial from './components/paginaInicial';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Router>
          <React.Fragment>
            <section className="screen">
              <Route path="/" exact component={ PaginaInicial }/>
              <Route path="/avaliacoes" component={ ListaAvaliacoes }/>
            </section>
          </React.Fragment>
        </Router>
      </div>
    );
  }
}

export default App;

const ListaAvaliacoes = () => <h2>Lista de avaliações</h2>
