import React, { Component } from 'react';
import './App.css';
import ListaEpisodios from './models/listaEpisodios'
import EpisodioUi from './components/episodioUi'
import MensagemFlash from './components/shared/mensagemFlash'

class App extends Component {

  constructor( props ) {
    super( props )
    this.listaEpisodios = new ListaEpisodios()
    // definindo estado inicial de um componente
    this.state = {
      episodio: this.listaEpisodios.episodioAleatorio,
      deveExibirMensagem: false
    }
  }

  sortear = () => {
    const episodio = this.listaEpisodios.episodioAleatorio
    this.setState( {
      episodio,
    } )
  }

  marcarComoAssistido = () => {
    const { episodio } = this.state
    this.listaEpisodios.marcarComoAssistido( episodio )
    this.setState( {
      episodio,
    } )
  }

  registrarNota = evt => {
    const { episodio } = this.state
    episodio.avaliar( evt.target.value )
    this.setState( {
      episodio, deveExibirMensagem: true
    } )
    setTimeout( () => {
      this.setState( {
        deveExibirMensagem: false
      } )
    }, 5 * 1000 )
  }

  geraCampoDeNota = () => {
    // https://reactjs.org/docs/conditional-rendering.html
    return (
      <div>
        {
          this.state.episodio.assistido && (
            <div>
              <span>Qual sua nota para este episódio?</span>
              <input type="number" placeholder="1 a 5" onBlur={ this.registrarNota.bind( this ) }></input>
            </div>
          )
        }
      </div>
    )
  }

  atualizarMensagem = devoExibir => {
    this.setState( {
      deveExibirMensagem: devoExibir
    } )
  }

  render() {
    const { episodio, deveExibirMensagem } = this.state

    return (
      <div className="App">
        <MensagemFlash 
          atualizarMensagem={ this.atualizarMensagem }
          deveExibirMensagem={ deveExibirMensagem } mensagem="Registramos sua nota!" />
        <header className="App-header">
          <EpisodioUi episodio={ episodio } />
          {/* <EpisodioUi episodio={ this.listaEpisodios.todos[10] } />
          <EpisodioUi episodio={ this.listaEpisodios.todos[11] } /> */}
          { this.geraCampoDeNota() }
          <div className="botoes">
            <button className="btn verde" onClick={ this.sortear }>Próximo</button>
            <button className="btn azul" onClick={ this.marcarComoAssistido.bind( this ) }>Já assisti</button>
          </div>
        </header>
      </div>
    );
  }
}

export default App;
