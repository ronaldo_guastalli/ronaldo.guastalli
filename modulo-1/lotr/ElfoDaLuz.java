import java.util.*;
public class ElfoDaLuz extends Elfo
{
    private final Item espada;
    private final ArrayList<String> DESCRICOES_VALIDAS = new ArrayList<>(
        Arrays.asList(
          "Galvorn"
        )    
    );

    public ElfoDaLuz(String nome){
        super(nome);
        espada = new Item(1, "Galvorn");
        this.inventario.adicionar(espada);
        this.inventario.adicionarMap("espada", espada);
        
    }
    
    protected double calcularDano() {
        return this.qtdDano = (this.getAtaque() % 2 == 0) ? new Double(-10.0) : 21.0;
    }

    public void lutar(){
        this.ataque();
        //this.vida();
    }
    
    public void perderItem(Item item){
        if(!this.DESCRICOES_VALIDAS.contains(item.getDescricao())){
            super.inventario.remover(item);
        }   
    }

    
    
}
