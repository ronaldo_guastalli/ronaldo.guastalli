import java.util.*;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PaginadorInventarioTest
{
    @Test
    public void pularLimitarComInventarioVazio() {
        Inventario inventario = new Inventario(0);
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        paginador.pular(0);
        ArrayList<Item> primeiraPagina = paginador.limitar(1);
        assertTrue(primeiraPagina.isEmpty());
    }
    
    @Test
    public void pularLimitarComApenasUmItem() {
        Inventario inventario = new Inventario(0);
        Item espada = new Item(1, "Espada");
        inventario.adicionar(espada);
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        paginador.pular(0);
        ArrayList<Item> primeiraPagina = paginador.limitar(1);
        assertEquals(espada, primeiraPagina.get(0));
        assertEquals(1, primeiraPagina.size());
    }
    
    @Test
    public void pularLimitarDentroDosLimites() {
        Inventario inventario = new Inventario(0);
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(2, "Escudo");
        Item lanca = new Item(10, "Lança");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(lanca);
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        paginador.pular(0);
        ArrayList<Item> primeiraPagina = paginador.limitar(2);
        paginador.pular(2);
        ArrayList<Item> segundaPagina = paginador.limitar(1);
        assertEquals(espada, primeiraPagina.get(0));
        assertEquals(escudo, primeiraPagina.get(1));
        assertEquals(2, primeiraPagina.size());
        assertEquals(lanca, segundaPagina.get(0));
        assertEquals(1, segundaPagina.size());
    }
    
    @Test
    public void pularLimitarForaDosLimites() {
        Inventario inventario = new Inventario(0);
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(2, "Escudo");
        Item lanca = new Item(10, "Lança");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(lanca);
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        paginador.pular(0);
        ArrayList<Item> primeiraPagina = paginador.limitar(2);
        paginador.pular(2);
        ArrayList<Item> segundaPagina = paginador.limitar(10000);
        assertEquals(espada, primeiraPagina.get(0));
        assertEquals(escudo, primeiraPagina.get(1));
        assertEquals(2, primeiraPagina.size());
        assertEquals(lanca, segundaPagina.get(0));
        assertEquals(1, segundaPagina.size());
    }
    
    @Test
    public void pularForaDosLimites() {
        Inventario inventario = new Inventario(0);
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(2, "Escudo");
        Item lanca = new Item(10, "Lança");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(lanca);
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        paginador.pular(-2);
        ArrayList<Item> primeiraPagina = paginador.limitar(2);
        boolean taPreenchido = !primeiraPagina.isEmpty();
        assertTrue(taPreenchido);
    }
    
    @Test
    public void inventario(){
        Inventario inventario = new Inventario(6);
        Item adaga = new Item(1, "Adaga");
        Item escudo = new Item(9, "Escudo");
        Item bracelete = new Item(18, "Bracelete");
        Item bota = new Item(2, "Bota");  
        Item espada = new Item(8, "Espada"); 
        Item machado = new Item(4, "Machado");
        inventario.adicionar(adaga);
        inventario.adicionar(escudo);
        inventario.adicionar(bracelete);
        inventario.adicionar(bota);
        inventario.adicionar(espada); 
        inventario.adicionar(machado);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
    }
    
    @Test
    public void pularLimitarComInventarioVazios(){
        Inventario inventario = new Inventario(0);
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        paginador.pular(0);
        ArrayList<Item> primeiraPagina = paginador.limitar(1);
  	assertTrue(primeiraPagina.isEmpty());
    }
    
    @Test
    public void paginadorLimitarMaiorQueQuantidadeItens(){
        Inventario inventario = new Inventario(6);
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(9, "Escudo de metal");
        Item pocao = new Item(18, "Poção de HP");
        Item bracelete = new Item(2, "Bracelete"); 
        Item adaga = new Item(1, "Adaga");
        Item bota = new Item(2, "Bota");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(pocao);
        inventario.adicionar(bracelete);
        inventario.adicionar(adaga);
        inventario.adicionar(bota);
        PaginadorInventario paginador = new PaginadorInventario(inventario);
	paginador.pular(5);
	ArrayList<Item> pagina = paginador.limitar(4);
	assertEquals(bota, pagina.get(0));
    }
    
    @Test
    public void paninador6Itens(){
        Inventario inventario = new Inventario(6);
        Item espada = new Item(1, "Espada");//
        Item escudo = new Item(9, "Escudo de metal");//
        Item pocao = new Item(18, "Poção de HP");
        Item bracelete = new Item(2, "Bracelete"); 
        Item adaga = new Item(1, "Adaga");
        Item bota = new Item(2, "Bota");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(pocao);
        inventario.adicionar(bracelete);
        inventario.adicionar(adaga);
        inventario.adicionar(bota);
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        paginador.pular(2);
        ArrayList<Item> pagina = paginador.limitar(9);
	assertEquals(pocao, pagina.get(0));
	assertEquals(bracelete, pagina.get(1));
	assertEquals(adaga, pagina.get(2));
	assertEquals(bota, pagina.get(3));
    }
}
