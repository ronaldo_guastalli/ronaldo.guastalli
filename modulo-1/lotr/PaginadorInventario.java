import java.util.*;
public class PaginadorInventario
{
    private Inventario inventario;
    private int marcadorInicial;

    public PaginadorInventario(Inventario inventario){
        this.inventario = inventario;
    }
    
    public Inventario getInventario(){
        return this.inventario;
    }
    
    public void pular(int pular){
        this.marcadorInicial = pular > 0 ? pular : 0;
    }
    
    public ArrayList<Item> limitar(int limitar){
        ArrayList<Item> subConjunto = new ArrayList<>();
        int indiceInicial = this.marcadorInicial;
        int indiceFinal = this.marcadorInicial + limitar;
        int indiceMenor = this.inventario.getItens().size();
        for(int i = indiceInicial; i < indiceFinal && i < indiceMenor; i++){
            subConjunto.add(this.inventario.obter(i)); 
        }
        return subConjunto;
    }
    
    
    
}
