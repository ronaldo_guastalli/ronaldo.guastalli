import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
public class DwarfTest
{
    //private static final double DELTA = 1e-15;
    
    @Test
    public void nasceCom110Vidas(){
        Dwarf umDwarf = new Dwarf("Gimli");        
        assertEquals(110.0, umDwarf.getQtdVida(), Constantes.DELTA);
    }
    
    @Test
    public void dwarfPerde10DeVida(){
        // Arrange
        Dwarf dwarf = new Dwarf("Gimli");
        // Act
        dwarf.sofrerDano();
        // Assert
        assertEquals(100.0, dwarf.getQtdVida(),  Constantes.DELTA);
        
    }
    
    @Test
    public void dwarfPerde10DeVidaDuasVezes(){
        // Arrange
        Dwarf dwarf = new Dwarf("Gimli");
        // Act
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        // Assert
        assertEquals(90.0, dwarf.getQtdVida(),  Constantes.DELTA);
        
    }
    
    @Test
    public void dwarfPerde10DeVidaDozeVezes(){
        // Arrange
        Dwarf dwarf = new Dwarf("Gimli");
        // Act
        int i = 0;
        while(i < 13){
            dwarf.sofrerDano();
            i++;
        }
        // Assert
        assertEquals(0.0, dwarf.getQtdVida(),  Constantes.DELTA);
        
    }
    
    @Test
    public void trocarStatusDwarfParaMorto(){
        Dwarf dwarf = new Dwarf("Gimli");
        int i = 0;
        while(i < 13){
            dwarf.sofrerDano();
            i++;
        }
        assertEquals(0.0, dwarf.getQtdVida(),  Constantes.DELTA);
        assertEquals(Status.MORTO,dwarf.getStatus());
    }
    
     @Test
    public void trocarStatusDwarfParaSofreuDano(){
        Dwarf dwarf = new Dwarf("Gimli");
        int i = 1;
        while(i < 5){
            dwarf.sofrerDano();
            i++;
        }
        assertEquals(70.0, dwarf.getQtdVida(),  Constantes.DELTA);
        assertEquals(Status.SOFREU_DANO,dwarf.getStatus());
    }
    
    @Test
    public void dwarfNasceComStatusRecemCriado(){
        Dwarf dwarf = new Dwarf("Gimli");
        assertEquals(Status.RECEM_CRIADO,dwarf.getStatus());
    }
    
    @Test
    public void dwarfUsaEscudo(){
        Dwarf dwarf = new Dwarf("Gimli");
        dwarf.equiparEscudo();
        dwarf.sofrerDano();
        assertEquals(105.0, dwarf.getQtdVida(),  Constantes.DELTA);
    }
    
    @Test
    public void dwarfNasceEscudo(){
        Dwarf dwarf = new Dwarf("Gimli");
        assertEquals(1, dwarf.getEscudo().getQuantidade());
        assertEquals("Escudo", dwarf.getEscudo().getDescricao());
    }
    
    @Test
    public void equalsEmItens(){
        Dwarf umDwarf = new Dwarf("Gimli"); //estância do Dwarf contem escudo = new Item(1, "Escudo");
        Item escudo = new Item(1, "Escudo"); //nova estância
        assertEquals(escudo, umDwarf.getEscudo());
    }
    
}
