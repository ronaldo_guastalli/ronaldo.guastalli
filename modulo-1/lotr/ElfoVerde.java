import java.util.*;
public class ElfoVerde extends Elfo
{
    private final ArrayList<String> DESCRICOES_VALIDAS = new ArrayList<>(
        Arrays.asList(
          "Espada de aço valiriano",
          "Arco de Vidro",
          "Flecha de Vidro"
        )    
    );
    
    public ElfoVerde(String nome){
        super(nome);
        this.qtdExperienciaPorAtaque = 2;
    }
    
    private boolean podeAlterarItem(Item item){
        boolean descricaoValida = true
            ? 
            this.DESCRICOES_VALIDAS.contains(item.getDescricao())
            : 
            false;
        return descricaoValida;
    }

    public void perderItem(Item item){
        if(podeAlterarItem(item)){
            super.inventario.remover(item);
        }   
    }

    public void ganharItem(Item item){
        if(podeAlterarItem(item)){
            super.inventario.adicionar(item);
        }
    }

}
