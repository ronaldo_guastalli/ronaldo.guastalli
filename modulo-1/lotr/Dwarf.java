public class Dwarf extends Personagem{
    
    private int indiceEscudo;
    private boolean equipado;
    
    public Dwarf(String nome){
        super(nome);
        qtdVida = 110.0;
        this.qtdDano = 10.0;
        status = Status.RECEM_CRIADO;
        inventario = new Inventario(1);
        inventario.adicionar(new Item(1, "Escudo"));
        indiceEscudo = 0;
        equipado = false;
    }
       
    public Item getEscudo(){
        return this.inventario.obter(indiceEscudo);
    }   
    
    private boolean podeSofrerDano() {
        return this.qtdVida > 0;
    } 
    
    protected double calcularDano() {
        return this.equipado ? 5.0 : this.qtdDano;
    }
    
    public void equiparEscudo(){
        this.equipado = true;
    }
    
    public  String imprimirResumo(){
        return "Dwarf";
    }
}
