

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoTest
{
    private static final double DELTA = 1e-15;
    //cenario de teste
    @Test
    public void atirarFlechaDevePerderFlechaAumentarExperiencia(){
        // Arrange
        Elfo umElfo = new Elfo("Legolas");
        Dwarf umDwarf = new Dwarf("Gimli");
        // Act
        umElfo.atirarFlecha(umDwarf);
        // Assert
        assertEquals(1, umElfo.getExperiencia());
        //Lei de Demeter 
        //assertEquals(41, umElfo.getQtdFlecha().getQuantidade());
        assertEquals(1, umElfo.getQtdFlecha());
    }
    
    @Test
    public void atirarFlecha3VezesDevePerderFlechaAumentarExperiencia(){
        // Arrange
        Elfo umElfo = new Elfo("Legolas");
        Dwarf umDwarf = new Dwarf("Gimli");
        // Act
        umElfo.atirarFlecha(umDwarf);
        umElfo.atirarFlecha(umDwarf);
        umElfo.atirarFlecha(umDwarf);
        // Assert
        assertEquals(2, umElfo.getExperiencia());
        //Lei de Demeter 
        //assertEquals(41, umElfo.getQtdFlecha().getQuantidade());
        assertEquals(0, umElfo.getQtdFlecha());
    }
    
    @Test
    public void elfosNascemCom2Flechas(){
        Elfo umElfo = new Elfo("Legolas");
        assertEquals(2, umElfo.getQtdFlecha());        
    }
    
    @Test
    public void atirarFlechaEmDwarfTiraVida(){        
        // Arrange
        Elfo umElfo = new Elfo("Legolas");
        Dwarf dwarf = new Dwarf("Gimli");
        // Act
        umElfo.atirarFlecha(dwarf);
        // Assert
        assertEquals(100.0, dwarf.getQtdVida(), 1e-15);
        assertEquals(1, umElfo.getQtdFlecha());
        assertEquals(1, umElfo.getExperiencia());
    }
    
    @Test
    public void elfoNasceComStatusRecemCriado(){
        Elfo elfo = new Elfo("Dain Yesxalim");
        assertEquals(Status.RECEM_CRIADO, elfo.getStatus());
    }
    
    @Test
    public void nasceCom100Vidas(){
        Elfo elfo = new Elfo("Legolas");        
        assertEquals(100.0, elfo.getQtdVida(), DELTA);
    }
    
    @Test
    public void contaInstanciasDeElfo(){
        Contador contaElfo = new Contador();
        Elfo umElfo = new Elfo("Legolas");
        Dwarf dwarf = new Dwarf("Gimli");
        umElfo.atirarFlecha(dwarf);
        assertEquals(1, Contador.getConta());
    }
}
