
public class DadoD6
{
    public static int numeroSorteado;
    
    public DadoD6(){
        numeroSorteado = 0;
    }
    
    public static int sortear(){
        numeroSorteado = 0;
        while(!(numeroSorteado >= 1 && numeroSorteado <= 6)){
        numeroSorteado = (int)(10 * Math.random());
        }
        return numeroSorteado;
    }
}
