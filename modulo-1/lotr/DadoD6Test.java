
import java.util.*;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DadoD6Test
{
  @Test
  public void oitentaNumero(){
      int i = 0;
      ArrayList<Integer> num = new ArrayList<>();
      Integer valor = 0;
      while(i < 100){
          valor = DadoD6.sortear();
          num.add(valor);          
          assertTrue(valor >= 1); 
          assertTrue(valor <= 6);
          i++;
      }

  }
}
