import java.util.*;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ExercitoTest
{
    @Test
    public void criaExercitoCom1Elfos(){
        Elfo elfo = new Elfo("Legolas");
        ElfoVerde elfoVerde = new ElfoVerde("Legolas");
        Exercito exercito = new Exercito();
        exercito.alistar(elfo);
        exercito.alistar(elfoVerde);
        assertEquals(elfoVerde, exercito.getElfo(0));
    }
    
    @Test
    public void criaExercitoCom2Elfos(){
        Elfo elfoNoturno = new ElfoNoturno("Legolas");
        Elfo elfoVerde = new ElfoVerde("Legolas");
        Exercito exercito = new Exercito();
        exercito.alistar(elfoNoturno);
        exercito.alistar(elfoVerde);
        assertEquals(elfoVerde, exercito.getElfo(0));
        assertEquals(elfoNoturno, exercito.getElfo(1));
    }
    
    @Test
    public void buscarElfoStatusRecemCriado(){
        Elfo elfoNoturno = new ElfoNoturno("Legolas"); //0
        Elfo elfoVerde = new ElfoVerde("Legolas");//1
        elfoNoturno.sofrerDano();//0
        Exercito exercito = new Exercito();
        exercito.alistar(elfoNoturno);
        exercito.alistar(elfoVerde);
        assertEquals(Status.SOFREU_DANO, exercito.getElfo(0).getStatus());
        assertEquals(Status.RECEM_CRIADO, exercito.getElfo(1).getStatus());
        assertEquals(elfoNoturno, 
            exercito.buscarElfo(Status.SOFREU_DANO).get(0));
        assertEquals(elfoVerde,
            exercito.buscarElfo(Status.RECEM_CRIADO).get(0));
    }
    
    @Test
    public void buscarElfoStatusListaVazia(){
        Exercito exercito = new Exercito();
        assertNull(exercito.buscarElfo(Status.SOFREU_DANO));

    }
}
