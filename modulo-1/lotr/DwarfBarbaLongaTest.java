

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DwarfBarbaLongaTest
{
    @Test
    public void caindoNaProbabilidade(){
        DwarfBarbaLonga dwarf = new DwarfBarbaLonga("Gimli");
        DadoD6 d = new DadoD6();
        int i = 1;
        dwarf.sofrerDano();
        double vidas = dwarf.getQtdVida();
        double dano = dwarf.qtdDano;
        while(vidas < 110.0){
            dwarf = new DwarfBarbaLonga("Gimli");
            dwarf.sofrerDano();
            //System.out.println(i++);
            if(DwarfBarbaLonga.valor1 == DwarfBarbaLonga.valor2)
                assertEquals(110.0, dwarf.getQtdVida(),  Constantes.DELTA);
            vidas = dwarf.getQtdVida();
        }     
        
    }
    
    @Test
    public void caindoForaProbabilidade(){
        DwarfBarbaLonga dwarf = new DwarfBarbaLonga("Gimli");
        DadoD6 d = new DadoD6();
        int i = 1;
        dwarf.calcularDano();
        //act
        double vidas = dwarf.getQtdVida();
        double dano = dwarf.qtdDano;        
        while(vidas > 0.0){
            dwarf.calcularDano();
            dwarf.sofrerDano();
            vidas = dwarf.getQtdVida();
        }     
        assertEquals(0.0, dwarf.getQtdVida(),  Constantes.DELTA);
        assertEquals(10.0, dwarf.qtdDano,  Constantes.DELTA);
    }
    
}
