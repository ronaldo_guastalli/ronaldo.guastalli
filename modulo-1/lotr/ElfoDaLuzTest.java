

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoDaLuzTest
{
    @Test
    public void inventarioDoElfoDaLuz(){
        ElfoDaLuz elfoDaLuz = new ElfoDaLuz("Legolas");
        elfoDaLuz.atirarFlecha(new Dwarf("Gimli"));
        Item flecha = new Item(34, "Flecha");
        elfoDaLuz.inventario.adicionarMap("flecha", flecha);
        assertEquals(1, elfoDaLuz.getInventario().obterMap("arco").getQuantidade());
        assertEquals(36, elfoDaLuz.getInventario().obterMap("flecha").getQuantidade());
        assertEquals(1, elfoDaLuz.getInventario().obterMap("espada").getQuantidade());
        
    }
    
    @Test
    public void atiraUmaFlecha(){
        ElfoDaLuz elfoDaLuz = new ElfoDaLuz("Legolas");
        elfoDaLuz.atirarFlechaMap(new Dwarf("Gimli"));
        assertEquals(1, elfoDaLuz.getInventario().obterMap("flecha").getQuantidade()); 
    }
    
    @Test
    public void atiraTodasAsFlecha(){
        ElfoDaLuz elfoDaLuz = new ElfoDaLuz("Legolas");
        elfoDaLuz.atirarFlechaMap(new Dwarf("Gimli"));
        elfoDaLuz.atirarFlechaMap(new Dwarf("Gimli"));
        assertEquals(0, elfoDaLuz.getInventario().obterMap("flecha").getQuantidade()); 
    }
    
    @Test
    public void adicionarMais6Flechas(){
        ElfoDaLuz elfoDaLuz = new ElfoDaLuz("Legolas");
        Item flecha = new Item(6, "Flechas");
        elfoDaLuz.inventario.adicionarMap("flecha", flecha);
        assertEquals(8, elfoDaLuz.getInventario().obterMap("flecha").getQuantidade()); 
    }
    
    @Test
    public void ataqueImpar(){
        ElfoDaLuz elfoDaLuz = new ElfoDaLuz("Legolas");
        Item flecha = new Item(4, "Flecha");
        elfoDaLuz.inventario.adicionarMap("flecha", flecha);
        Dwarf dwarf = new Dwarf("Gimli");        
        elfoDaLuz.atirarFlechaMap(dwarf);
        assertEquals(79.0, elfoDaLuz.getQtdVida(), Constantes.DELTA);
        assertEquals(5, elfoDaLuz.getInventario().obterMap("flecha").getQuantidade());
        
    }
    
    @Test
    public void ataquePar(){
        ElfoDaLuz elfoDaLuz = new ElfoDaLuz("Legolas");
        Item flecha = new Item(4, "Flecha");
        elfoDaLuz.inventario.adicionarMap("flecha", flecha);
        Dwarf dwarf = new Dwarf("Gimli");        
        elfoDaLuz.atirarFlechaMap(dwarf);
        elfoDaLuz.atirarFlechaMap(dwarf);
        assertEquals(89.0, elfoDaLuz.getQtdVida(), Constantes.DELTA);
        assertEquals(4, elfoDaLuz.getInventario().obterMap("flecha").getQuantidade());
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
}
