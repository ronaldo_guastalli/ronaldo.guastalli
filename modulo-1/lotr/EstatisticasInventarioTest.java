import java.util.*;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
public class EstatisticasInventarioTest
{
    private static final double DELTA = 1e-15;
    @Test
    public void calculoMedia(){
        Inventario inventario = new Inventario(5);
        Item adaga = new Item(46, "Adaga");
        Item escudo = new Item(4, "Escudo");
        Item bracelete = new Item(3, "Bracelete");
        Item bota = new Item(78, "Bota");  
        Item espada = new Item(14, "Espada"); 
        inventario.adicionar(adaga);
        inventario.adicionar(escudo);
        inventario.adicionar(bracelete);
        inventario.adicionar(bota);
        inventario.adicionar(espada);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double expecteds = (46+4+3+78+14)/5;
        double actuals = estatisticas.calcularMedia();
        assertEquals(expecteds, actuals, DELTA);
    }
    
    @Test
    public void calculoMediaZeroItens(){
        Inventario inventario = new Inventario(5);
        Item adaga = new Item(46, "Adaga");
        Item escudo = new Item(4, "Escudo");
        Item bracelete = new Item(3, "Bracelete");
        Item bota = new Item(78, "Bota");  
        Item espada = new Item(14, "Espada"); 
        inventario.adicionar(adaga);
        inventario.adicionar(escudo);
        inventario.adicionar(bracelete);
        inventario.adicionar(bota);
        inventario.adicionar(espada);
        while(inventario.getItens().size() > 0){
        inventario.remover(0);
        }        
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double expecteds = Double.NaN;
        double actuals = estatisticas.calcularMedia();
        assertEquals(expecteds, actuals, DELTA);
    }
    
    @Test
    public void calculoMediaItensZeroQtd(){
        Inventario inventario = new Inventario(5);
        Item adaga = new Item(0, "Adaga");
        Item escudo = new Item(0, "Escudo");
        Item bracelete = new Item(0, "Bracelete");
        Item bota = new Item(0, "Bota");  
        Item espada = new Item(0, "Espada"); 
        inventario.adicionar(adaga);
        inventario.adicionar(escudo);
        inventario.adicionar(bracelete);
        inventario.adicionar(bota);
        inventario.adicionar(espada);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double expecteds = (0)/5;
        double actuals = estatisticas.calcularMedia();
        assertEquals(expecteds, actuals, DELTA);
    }
    
    @Test
    public void calculoMediaDepoisRemocaoItem(){
        Inventario inventario = new Inventario(5);
        Item adaga = new Item(46, "Adaga");
        Item escudo = new Item(4, "Escudo");
        Item bracelete = new Item(3, "Bracelete");
        Item bota = new Item(78, "Bota");  
        Item espada = new Item(14, "Espada"); 
        inventario.adicionar(adaga);
        inventario.adicionar(escudo);
        inventario.adicionar(bracelete);
        inventario.adicionar(bota);
        inventario.adicionar(espada);
        inventario.remover(0);
        inventario.remover(0);
        inventario.remover(0);        
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double expecteds = (78+14)/2;
        double actuals = estatisticas.calcularMedia();
        assertEquals(expecteds, actuals, DELTA);
    }
    
    @Test
    public void medianaElementoImpares(){
        Inventario inventario = new Inventario(5);
        Item adaga = new Item(1, "Adaga");
        Item escudo = new Item(9, "Escudo");
        Item bracelete = new Item(7, "Bracelete");
        Item bota = new Item(5, "Bota");  
        Item espada = new Item(3, "Espada"); 
        inventario.adicionar(adaga);
        inventario.adicionar(escudo);
        inventario.adicionar(bracelete);
        inventario.adicionar(bota);
        inventario.adicionar(espada);        
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double expecteds = 5; //1, 3, 5, 7, 9
        double actuals = estatisticas.calcularMediana();
        assertEquals(expecteds, actuals, DELTA);
    }
    
    @Test
    public void medianaComApenasUmItem(){
        Inventario inventario = new Inventario(1);
        Item adaga = new Item(5, "Adaga"); 
        inventario.adicionar(adaga);        
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double expecteds = 5;
        double actuals = estatisticas.calcularMediana();
        assertEquals(expecteds, actuals, DELTA);
    }
    
    @Test
    public void medianaElementoPares(){
        Inventario inventario = new Inventario(6);
        Item adaga = new Item(1, "Adaga");
        Item escudo = new Item(9, "Escudo");
        Item bracelete = new Item(18, "Bracelete");
        Item bota = new Item(2, "Bota");  
        Item espada = new Item(8, "Espada"); 
        Item machado = new Item(4, "Machado");
        inventario.adicionar(adaga);
        inventario.adicionar(escudo);
        inventario.adicionar(bracelete);
        inventario.adicionar(bota);
        inventario.adicionar(espada); 
        inventario.adicionar(machado);
        inventario.ordenarItens(TipoOrdenacao.ASC);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double expecteds = (4+8)/2.0; //mediana
        double expecteds2 = (1+9+18+2+8+4)/6.0; //media
        double actuals = estatisticas.calcularMediana();
        double actuals2 = estatisticas.calcularMedia();
        assertEquals(expecteds, actuals, DELTA);
        assertEquals(expecteds2, actuals2, DELTA);
    }
    
    @Test
    public void quanrtidadesItensAcimaDaMedia(){
        Inventario inventario = new Inventario(6);
        Item adaga = new Item(1, "Adaga");
        Item escudo = new Item(9, "Escudo");
        Item bracelete = new Item(18, "Bracelete");
        Item bota = new Item(2, "Bota");  
        Item espada = new Item(8, "Espada"); 
        Item machado = new Item(4, "Machado");
        inventario.adicionar(adaga);
        inventario.adicionar(escudo);
        inventario.adicionar(bracelete);
        inventario.adicionar(bota);
        inventario.adicionar(espada); 
        inventario.adicionar(machado);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double expecteds1 = (1+9+18+2+8+4)/6; //media = 7
        double expecteds2 = 3; //18, 8, 9,         
        double actuals1 = estatisticas.calcularMedia();
        int actuals2 = estatisticas.qtdItensAcimaDaMedia();
        assertEquals(expecteds1, actuals1, DELTA);
        assertEquals(expecteds2, actuals2, DELTA);
    }
    
    
    @Test
    public void inventarioConstrutor(){
        Inventario inventario = new Inventario(3);
        Item adaga = new Item(1, "Adaga");
        Item escudo = new Item(9, "Escudo");
        Item bracelete = new Item(18, "Bracelete");
        inventario.adicionar(adaga);
        inventario.adicionar(escudo);
        inventario.adicionar(bracelete);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        ArrayList<Item> actual = inventario.getItens();        
        assertEquals(adaga, actual.get(0));
        assertEquals(escudo, actual.get(1));
        assertEquals(bracelete, actual.get(2));

    }

}
