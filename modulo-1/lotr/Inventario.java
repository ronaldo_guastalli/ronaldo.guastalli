import java.util.*;
public class Inventario
{
    private ArrayList<Item> itens;
    int remocaoItens = 0;
    boolean parametroAsc = true;
    protected HashMap<String, Item> itensMap;

    public Inventario(int quatidadeItens){ 
        itens = new ArrayList<>(quatidadeItens);
        itensMap = new HashMap<>();
    }
   

    public void adicionar(Item item){
        this.itens.add(item);
    }
    
    public void adicionarMap(String key, Item item){
        if (itensMap.containsKey(key)){
            int provisorio = this.itensMap.get(key).getQuantidade();
            int soma = provisorio + item.getQuantidade();
            item.setQuantidade(soma);
            Item novo = item;
            this.itensMap.replace(key,novo);
        }else{
            this.itensMap.put(key, item);
        }
        
    }
   
    public Item obter(int posicao){
        if (posicao >= this.itens.size())
            return null;
        else
            return this.itens.get(posicao);
    }
    
    public Item obter(String descricao){
        Item itemComDescricao = null;
        if(descricao == null){
            return itemComDescricao;
        }else{
            for(Item item : itens){
                if(item.getDescricao().equals(descricao))
                    itemComDescricao = item;
                    break;
            }
            return itemComDescricao;
        }
    }
    
    public Item obterMap(String key){
        return this.itensMap.get(key);
    }

    public void remover(int posicao){
        this.itens.remove(posicao);
    }
    
    public void remover(Item item){
        this.getItens().remove(item);
    }
    
    public void removerMap(String key){
        this.itensMap.get(key);
    }

    public ArrayList<Item> getItens(){
        return this.itens;
    }

    public String getDescricoesItens(){
        StringBuilder descricoes = new StringBuilder();
        for(int i = 0; i < this.itens.size(); i++){
            if (this.itens.get(i) != null){
                String descricao = this.itens.get(i).getDescricao();
                descricoes.append(descricao);
                boolean deveColocarVirgula = i < (this.itens.size()-1);
                if (deveColocarVirgula) {
                    descricoes.append(",");
                }
            }
        }
        return descricoes.toString();
    }

    public Item getItemMaiorQuantidade(){
        int maiorQuantidade = 0; 
        int indiceItemMaiorQuantidade = 0;
        for(int i = 0; i < itens.size(); i++){
            if(itens.get(i) != null){
                if(itens.get(i).getQuantidade() >  maiorQuantidade){
                    maiorQuantidade = itens.get(i).getQuantidade();
                    indiceItemMaiorQuantidade = i;
                }
            }
        }
        return this.itens.size() > 0 ? this.itens.get(indiceItemMaiorQuantidade) : null;
    }

    public Item buscar(String descricaoItem){
        Item itemEncontrado = null;
        for(Item item : itens){
            if(item.getDescricao().equalsIgnoreCase(descricaoItem)){
                itemEncontrado = item;
                return itemEncontrado; //retorna o primeiro encontrado;
            }
        }
        return itemEncontrado;
    }

    public ArrayList<Item> inverter(){
        ArrayList<Item> itensInvertidos = new ArrayList<>(this.itens.size());
        for(int i = (this.itens.size()-1) ; i >= 0 ; i--){          
            itensInvertidos.add(this.itens.get(i));
        }
        return itensInvertidos;
    }

    public void ordenarItens(){
        int size = this.itens.size();
        for(int i = 1; i < size ; i++){
            Item provisorio = this.itens.get(i); //objeto
            int j = i -1;
            if(parametroAsc){
                while(j >= 0 && this.itens.get(j).getQuantidade() > provisorio.getQuantidade()){
                    this.itens.set(j + 1, this.itens.get(j));
                    j--;
                }
            }else{
                while(j >= 0 && this.itens.get(j).getQuantidade() < provisorio.getQuantidade()){
                    this.itens.set(j + 1, this.itens.get(j));
                    j--;
                }
            }
            this.itens.set(j + 1, provisorio);
        }
    }

    public void ordenarItens(TipoOrdenacao tipoOrdenacao){
        if(tipoOrdenacao == TipoOrdenacao.ASC){
            this.parametroAsc = true;
            ordenarItens();
        }else if(tipoOrdenacao == TipoOrdenacao.DESC){
            this.parametroAsc = false;
            ordenarItens();
        }
        // if(tipoOrdenacao == TipoOrdenacao.DESC){        
            // for(int i = 1; i < this.itens.size(); i++){
                // Item provisorio = this.itens.get(i);
                // int j = i-1;
                // while(j >= 0 && this.itens.get(j).getQuantidade() < provisorio.getQuantidade()){
                    // this.itens.set(j + 1, this.itens.get(j));
                    // j--;
                // }
                // this.itens.set(j + 1, provisorio);
            // }
        // } 
    }

    public int quantidadeTotalDeItens(){
        int somaItens = 0;
        for(Item i : this.getItens()){
            somaItens += i.getQuantidade();
        }
        return somaItens;
    }

}
