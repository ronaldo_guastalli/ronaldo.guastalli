public class EstatisticasInventario
{
    private Inventario inventario;

    public EstatisticasInventario(Inventario inventario){
        this.inventario = inventario;
    }

    public double calcularMedia(){
        if (this.inventario.getItens().size() == 0){
            return Double.NaN;
        }else{
            double somaQuantidades = this.inventario.quantidadeTotalDeItens();
            int qtdItens = this.inventario.getItens().size(); 
            return somaQuantidades / qtdItens;
        }
    }

    private boolean getPar(){
        boolean par = false;
        boolean impar = false;
        if(this.inventario.getItens().size() % 2 == 0){
            par = true;
            return par;
        }else{
            impar = false;
            return impar;
        }
    }

    public double calcularMediana(){
        if (this.inventario.getItens().isEmpty()) {
            return Double.NaN;
        }
        this.inventario.ordenarItens(TipoOrdenacao.ASC);
        int qtdItens = this.inventario.getItens().size();
        int meio = qtdItens / 2;
        int qtdMeio = this.inventario.obter(meio).getQuantidade();
        double mediana = 0.0;
        if(getPar()){
            int qtdMeioMenosUm = this.inventario.obter(meio - 1).getQuantidade();
            mediana = (qtdMeio + qtdMeioMenosUm) / 2.0;
        }else{
            mediana = qtdMeio;
        }
        return mediana;
    }

    public int qtdItensAcimaDaMedia(){
        double mediaQuantidades = calcularMedia();
        int cont = 0;
        for(Item i : this.inventario.getItens()){
            int itemQuantidade = i.getQuantidade();
            if(itemQuantidade > mediaQuantidades){
                cont++;
            }
        }
        return cont;
    }
}
