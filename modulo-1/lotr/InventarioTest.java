import java.util.*;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
public class InventarioTest
{
    @Test
    public void criarInventarioVazioInformandoQuantidadeInicialDeItens() {
        Inventario inventario = new Inventario(42);
        assertEquals(0, inventario.getItens().size());
    }
    
    @Test
    public void adicionarUmItem(){
        Inventario inventario = new Inventario(1);
        Item espada = new Item(1,"Espada");
        inventario.adicionar(espada);
        assertEquals(espada, inventario.getItens().get(0));
    }
    
    @Test
    public void adicionarDoisItem(){
        Inventario inventario = new Inventario(2);
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(3, "Escudo de aço");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        assertEquals(espada, inventario.getItens().get(0));
        assertEquals(escudo, inventario.getItens().get(1));
    }
    
    @Test
    public void obterItemNaPrimeiraPosicao() {
        Inventario inventario = new Inventario(1);
        Item espada = new Item(1, "Espada");
        inventario.adicionar(espada);
        assertEquals(espada, inventario.obter(0));
    }
    
    @Test
    public void obterItemNaoAdicionado() {
        Inventario inventario = new Inventario(1);
        Item espada = new Item(1, "Espada");
        assertNull(inventario.obter(0));
        assertEquals(0, inventario.getItens().size());
        assertArrayEquals(new Item[]{} ,inventario.getItens().toArray());
    }
    
    @Test
    public void removerItem() {
        // Inventario inventario = new Inventario(1);
        Inventario inventario = new Inventario(1);
        Item espada = new Item(1, "Espada");
        inventario.adicionar(espada);
        inventario.remover(0);
        assertNull(inventario.obter(0));
        assertEquals(0, inventario.getItens().size());
        assertArrayEquals(new Item[]{} ,inventario.getItens().toArray());
    }
    
    @Test
    public void removerItemAntesDeAdicionarProximo() {
         Inventario inventario = new Inventario(1);
        Item espada = new Item(1, "Espada");
        Item armadura = new Item(1, "Armadura");
        inventario.adicionar(espada);
        inventario.remover(0);
        inventario.adicionar(armadura);
        assertEquals(armadura, inventario.obter(0));
        // assertEquals(1, inventario.getItens().length);
        assertEquals(1, inventario.getItens().size());
    }
    
    @Test
    public void retornaONomeDos3ItensSeparadoPorVirgula(){
        Inventario inventario = new Inventario(3);
        Item adaga = new Item(1, "Adaga");
        Item escudo = new Item(1, "Escudo");
        Item bracelete = new Item(1, "Bracelete");
        inventario.adicionar(adaga);
        inventario.adicionar(escudo);
        inventario.adicionar(bracelete);
        assertEquals("Adaga,Escudo,Bracelete", inventario.getDescricoesItens());
    }
    
    @Test
    public void getDescricoesItensRemovendoItemNoMeio() {
        Inventario inventario = new Inventario(4);
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(2, "Escudo");
        Item flechas = new Item(3, "Flechas");
        Item botas = new Item(1, "Botas de ferro");
        inventario.adicionar(espada); //0
        inventario.adicionar(escudo); //1
        inventario.adicionar(flechas); //2
        inventario.adicionar(botas); //3
        inventario.adicionar(espada); //4
        inventario.adicionar(escudo); //5
        inventario.adicionar(flechas); //6
        inventario.adicionar(botas); //7
        inventario.remover(1);
        inventario.remover(2);
        inventario.remover(3);
        inventario.remover(4);
        String resultado = inventario.getDescricoesItens();
        assertEquals("Espada,Flechas,Espada,Flechas", resultado);
    }
    
    @Test
    public void retornaONomeDos3ItensSeparadoPorVirgulaPrimeiroNull(){
        Inventario inventario = new Inventario(3);
        Item adaga = new Item(1, "Adaga");
        Item escudo = new Item(1, "Escudo");
        Item bracelete = new Item(1, "Bracelete");
        inventario.adicionar(adaga);
        inventario.adicionar(escudo);
        inventario.adicionar(bracelete);
        inventario.remover(0);
        assertEquals("Escudo,Bracelete", inventario.getDescricoesItens());
    }
    
    @Test
    public void retornaNome2ItensDepoisDeRemover1Item(){
        Inventario inventario = new Inventario(3);
        Item adaga = new Item(1, "Adaga");
        Item escudo = new Item(1, "Escudo");
        Item bracelete = new Item(1, "Bracelete");
        inventario.adicionar(adaga);
        inventario.adicionar(escudo);
        inventario.adicionar(bracelete);
        inventario.remover(1);
        assertEquals("Adaga,Bracelete", inventario.getDescricoesItens());
    }
    
    @Test
    public void retornaItensDepoisDeRemover3Item(){
        Inventario inventario = new Inventario(3);
        Item adaga = new Item(1, "Adaga");
        Item escudo = new Item(1, "Escudo");
        Item bracelete = new Item(1, "Bracelete");
        inventario.adicionar(adaga);
        inventario.adicionar(escudo);
        inventario.adicionar(bracelete);
        int i =0;
        while(inventario.getItens().size()>0){
            inventario.remover(i);
        }
        assertEquals("", inventario.getDescricoesItens());
    }
    
    @Test
    public void adicionarAposRemover() {
        Inventario inventario = new Inventario(3);
        Item espada = new Item(1, "Espada");
        Item bracelete = new Item(1, "Bracelete de prata");
        Item armadura = new Item(1, "Armadura");
        inventario.adicionar(espada);
        inventario.adicionar(bracelete);
        inventario.remover(0);
        inventario.adicionar(armadura);
        assertEquals(bracelete, inventario.obter(0));
        assertEquals(armadura, inventario.obter(1));
    }
    
    @Test
    public void retornaItemComMaiorQuantidade3ItensInformados(){
        Inventario inventario = new Inventario(3);
        Item adaga = new Item(1, "Adaga");
        Item escudo = new Item(7, "Escudo");
        Item bracelete = new Item(3, "Bracelete");
        inventario.adicionar(adaga);
        inventario.adicionar(escudo);
        inventario.adicionar(bracelete);
        assertEquals(escudo, inventario.getItemMaiorQuantidade());
    }
    
    @Test
    public void getItemMaiorQuantidadeInventarioVazio() {
         Inventario inventario = new Inventario(0);
        Item resultado = inventario.getItemMaiorQuantidade();
        assertNull(resultado);
    }
    
    @Test
    public void retornaItemComMaiorQuantidadeNaoInformeQtdItens(){
        Inventario inventario = new Inventario(3);
        Item adaga = new Item(1, "Adaga");
        Item escudo = new Item(7, "Escudo");
        Item bracelete = new Item(13, "Bracelete");
        inventario.adicionar(adaga);
        inventario.adicionar(escudo);
        inventario.adicionar(bracelete);
        assertEquals(bracelete, inventario.getItemMaiorQuantidade());
    }
    
    @Test
    public void buscaItemPorStringDescricao(){
        Inventario inventario = new Inventario(3);
        Item adaga = new Item(1, "Adaga");
        Item escudo = new Item(7, "Escudo");
        Item bracelete = new Item(13, "Bracelete");
        inventario.adicionar(adaga);
        inventario.adicionar(escudo);
        inventario.adicionar(bracelete);
        Item resultado1 = inventario.buscar("Escudo");
        Item resultado2 = inventario.buscar("adaga");
        assertEquals(escudo, resultado1); 
        assertEquals(adaga, resultado2);
    }
    
    @Test
    public void buscarItemComInventarioVazio(){
        Inventario inventario = new Inventario(0);
        assertNull(inventario.buscar(new String("Bracelete")));
    }
    
    // e se o objeto que possui o parâmetro for repitido?
    @Test
    public void buscaItemPorStringDescricaoComDescricaoRepetidas(){
        Inventario inventario = new Inventario(3);
        Item adaga = new Item(1, "Adaga");
        Item escudo = new Item(7, "Escudo");
        Item adaga2 = new Item(13, "Adaga");
        inventario.adicionar(adaga);
        inventario.adicionar(escudo);
        inventario.adicionar(adaga2);
        Item resultado1 = inventario.buscar("Adaga");
        Item resultado2 = inventario.buscar("adaga");
        assertEquals(adaga, resultado1); 
        assertEquals(adaga, resultado2);
    }
    
    @Test
    public void buscaItemPorStringDescricaoNaoEncontra(){
        Inventario inventario = new Inventario(2);
        Item adaga = new Item(1, "Adaga");
        Item escudo = new Item(7, "Escudo");
        inventario.adicionar(adaga);
        inventario.adicionar(escudo);
        Item resultado1 = inventario.buscar("Adaga");
        Item resultado2 = inventario.buscar("bota");
        assertEquals(adaga, resultado1); 
        assertEquals(null, resultado2);
    }
    
    @Test
    public void itensInvertidos(){
        Inventario inventario = new Inventario(3);
        Item adaga = new Item(1, "Adaga");
        Item escudo = new Item(7, "Escudo");
        Item bracelete =new Item(3, "Bracelete");
        Item bota = new Item(8, "Bota");
        inventario.adicionar(adaga);
        inventario.adicionar(escudo);
        inventario.adicionar(bracelete);
        inventario.adicionar(bota);
        ArrayList<Item> itensInvertidos = inventario.inverter();
        ArrayList<Item> resultado = new ArrayList<>();
        resultado.add(bota);
        resultado.add(bracelete);
        resultado.add(escudo);
        resultado.add(adaga);
        assertEquals(resultado.get(0), itensInvertidos.get(0));
        assertEquals(resultado.get(1), itensInvertidos.get(1));
        assertEquals(resultado.get(2), itensInvertidos.get(2));
        assertEquals(resultado.get(3), itensInvertidos.get(3));
    }
    
    @Test
    public void inverterInventarioVazio(){
        Inventario inventario = new Inventario(0);
        assertTrue(inventario.inverter().isEmpty());
    }
    
    @Test
    public void inverterInventarioComUmItem(){
        Inventario inventario = new Inventario(1);
        Item espada = new Item(1, "Espada de aço valeriano");
        inventario.adicionar(espada);
        ArrayList<Item> resultado = inventario.inverter();
        assertEquals(espada, resultado.get(0));
        assertEquals(1, resultado.size());
    }
    
    @Test
    public void ordenarItensAsc(){
        Inventario inventario = new Inventario(4);
        Item adaga = new Item(46, "Adaga");
        Item escudo = new Item(4, "Escudo");
        Item bracelete = new Item(3, "Bracelete");
        Item bota = new Item(78, "Bota");        
        inventario.adicionar(adaga);
        inventario.adicionar(escudo);
        inventario.adicionar(bracelete);
        inventario.adicionar(bota);        
        inventario.ordenarItens();
        assertTrue(inventario.getItens().get(0).getQuantidade() == 3);
        assertTrue(inventario.getItens().get(1).getQuantidade() == 4);
        assertTrue(inventario.getItens().get(2).getQuantidade() == 46);
        assertTrue(inventario.getItens().get(3).getQuantidade() == 78);
    }
    
    @Test
    public void ordenarItensDesc(){
        Inventario inventario = new Inventario(4);
        Item adaga = new Item(46, "Adaga");
        Item escudo = new Item(4, "Escudo");
        Item bracelete = new Item(3, "Bracelete");
        Item bota = new Item(78, "Bota");        
        inventario.adicionar(adaga);
        inventario.adicionar(escudo);
        inventario.adicionar(bracelete);
        inventario.adicionar(bota);        
        inventario.ordenarItens(TipoOrdenacao.DESC);
        assertTrue(inventario.getItens().get(0).getQuantidade() == 78);
        assertTrue(inventario.getItens().get(1).getQuantidade() == 46);
        assertTrue(inventario.getItens().get(2).getQuantidade() == 4);
        assertTrue(inventario.getItens().get(3).getQuantidade() == 3);
    }
    
    @Test
    public void ordenarItensDescItensImparesDepoisOrdenarAsc(){
        Inventario inventario = new Inventario(5);
        Item adaga = new Item(46, "Adaga");
        Item escudo = new Item(4, "Escudo");
        Item bracelete = new Item(3, "Bracelete");
        Item bota = new Item(78, "Bota");  
        Item espada = new Item(14, "Espada"); 
        inventario.adicionar(adaga);
        inventario.adicionar(escudo);
        inventario.adicionar(bracelete);
        inventario.adicionar(bota);
        inventario.adicionar(espada);
        inventario.ordenarItens(TipoOrdenacao.DESC);
        assertTrue(inventario.getItens().get(0).getQuantidade() == 78);
        assertTrue(inventario.getItens().get(1).getQuantidade() == 46);
        assertTrue(inventario.getItens().get(2).getQuantidade() == 14);
        assertTrue(inventario.getItens().get(3).getQuantidade() == 4);
        assertTrue(inventario.getItens().get(4).getQuantidade() == 3);
        inventario.ordenarItens(TipoOrdenacao.ASC);
        assertTrue(inventario.getItens().get(0).getQuantidade() == 3);
        assertTrue(inventario.getItens().get(1).getQuantidade() == 4);
        assertTrue(inventario.getItens().get(2).getQuantidade() == 14);
        assertTrue(inventario.getItens().get(3).getQuantidade() == 46);
        assertTrue(inventario.getItens().get(4).getQuantidade() == 78);
    }
   
    @Test
    public void ordenarInventarioVazio(){
        Inventario inventario = new Inventario(0);
        inventario.ordenarItens();
        assertTrue(inventario.getItens().isEmpty());
    }
    
    @Test
    public void ordenarTotalmenteDesordenado(){
        Inventario inventario = new Inventario(1);
        Item espada = new Item(14, "Espada");
        Item adaga = new Item(46, "Adaga");
        Item escudo = new Item(4, "Escudo");
        Item bracelete = new Item(3, "Bracelete");
        Item bota = new Item(78, "Bota");  
        inventario.adicionar(espada);
        inventario.adicionar(adaga);
        inventario.adicionar(escudo);
        inventario.adicionar(bracelete);
        inventario.adicionar(bota);
        inventario.ordenarItens();
        ArrayList<Item> esperado = new ArrayList<>(
            Arrays.asList(espada, adaga, escudo, bracelete, bota));
    }
    
    @Test
    public void ordenarTotalmenteOrdenado(){
        Inventario inventario = new Inventario(1);
        Item espada = new Item(1, "Espada");
        Item adaga = new Item(4, "Adaga");
        Item escudo = new Item(7, "Escudo");
        Item bracelete = new Item(13, "Bracelete");
        Item bota = new Item(18, "Bota");  
        inventario.adicionar(espada);
        inventario.adicionar(adaga);
        inventario.adicionar(escudo);
        inventario.adicionar(bracelete);
        inventario.adicionar(bota);
        inventario.ordenarItens();
        ArrayList<Item> esperado = new ArrayList<>(
            Arrays.asList(espada, adaga, escudo, bracelete, bota));
    }
    
    @Test
    public void ordenarTotalmenteIguais(){
        Inventario inventario = new Inventario(1);
        Item espada = new Item(4, "Espada");
        Item adaga = new Item(4, "Adaga");
        Item escudo = new Item(4, "Escudo");
        Item bracelete = new Item(4, "Bracelete");
        Item bota = new Item(4, "Bota");  
        inventario.adicionar(espada);
        inventario.adicionar(adaga);
        inventario.adicionar(escudo);
        inventario.adicionar(bracelete);
        inventario.adicionar(bota);
        inventario.ordenarItens();
        ArrayList<Item> esperado = new ArrayList<>(
            Arrays.asList(espada, adaga, escudo, bracelete, bota));
    }
    
    @Test
    public void ordenarDescComInventarioVazio(){
        Inventario inventario = new Inventario(0);
        inventario.ordenarItens(TipoOrdenacao.DESC);
        assertTrue(inventario.getItens().isEmpty());
    }
    
    @Test
    public void ordenarDescTotalmenteDesordenado(){
        Inventario inventario = new Inventario(1);
        Item espada = new Item(14, "Espada");
        Item adaga = new Item(46, "Adaga");
        Item escudo = new Item(4, "Escudo");
        Item bracelete = new Item(3, "Bracelete");
        Item bota = new Item(78, "Bota");  
        inventario.adicionar(espada);
        inventario.adicionar(adaga);
        inventario.adicionar(escudo);
        inventario.adicionar(bracelete);
        inventario.adicionar(bota);
        inventario.ordenarItens(TipoOrdenacao.DESC);
        ArrayList<Item> esperado = new ArrayList<>(
            Arrays.asList(bota, bracelete, escudo, adaga, espada));
    }
    
    @Test
    public void ordenarDescTotalmenteOrdenado(){
        Inventario inventario = new Inventario(1);
        Item espada = new Item(1, "Espada");
        Item adaga = new Item(4, "Adaga");
        Item escudo = new Item(7, "Escudo");
        Item bracelete = new Item(13, "Bracelete");
        Item bota = new Item(18, "Bota");  
        inventario.adicionar(espada);
        inventario.adicionar(adaga);
        inventario.adicionar(escudo);
        inventario.adicionar(bracelete);
        inventario.adicionar(bota);
        inventario.ordenarItens(TipoOrdenacao.DESC);
        ArrayList<Item> esperado = new ArrayList<>(
            Arrays.asList(bota, bracelete, escudo, adaga, espada));
    }
    
    @Test
    public void ordenarAscTotalmenteOrdenado(){
        Inventario inventario = new Inventario(1);
        Item espada = new Item(1, "Espada");
        Item adaga = new Item(4, "Adaga");
        Item escudo = new Item(7, "Escudo");
        Item bracelete = new Item(13, "Bracelete");
        Item bota = new Item(18, "Bota");  
        inventario.adicionar(espada);
        inventario.adicionar(adaga);
        inventario.adicionar(escudo);
        inventario.adicionar(bracelete);
        inventario.adicionar(bota);
        inventario.ordenarItens(TipoOrdenacao.ASC);
        ArrayList<Item> esperado = new ArrayList<>(
            Arrays.asList(bota, bracelete, escudo, adaga, espada));
    }
    
    @Test
    public void ordenarTotaDesclmenteIguais(){
        Inventario inventario = new Inventario(1);
        Item espada = new Item(4, "Espada");
        Item adaga = new Item(4, "Adaga");
        Item escudo = new Item(4, "Escudo");
        Item bracelete = new Item(4, "Bracelete");
        Item bota = new Item(4, "Bota");  
        inventario.adicionar(espada);
        inventario.adicionar(adaga);
        inventario.adicionar(escudo);
        inventario.adicionar(bracelete);
        inventario.adicionar(bota);
        inventario.ordenarItens(TipoOrdenacao.DESC);
        ArrayList<Item> esperado = new ArrayList<>(
            Arrays.asList(bota, bracelete, escudo, adaga, espada));
    }
    
    @Test
    public void ordenarTotaAsclmenteIguais(){
        Inventario inventario = new Inventario(1);
        Item espada = new Item(4, "Espada");
        Item adaga = new Item(4, "Adaga");
        Item escudo = new Item(4, "Escudo");
        Item bracelete = new Item(4, "Bracelete");
        Item bota = new Item(4, "Bota");  
        inventario.adicionar(espada);
        inventario.adicionar(adaga);
        inventario.adicionar(escudo);
        inventario.adicionar(bracelete);
        inventario.adicionar(bota);
        inventario.ordenarItens(TipoOrdenacao.ASC);
        ArrayList<Item> esperado = new ArrayList<>(
            Arrays.asList(espada, adaga, escudo, bracelete, bota));
    }
    
    @Test
    public void ordenarAscComInventarioVazio(){
        Inventario inventario = new Inventario(0);
        inventario.ordenarItens(TipoOrdenacao.ASC);
        assertTrue(inventario.getItens().isEmpty());
    }
    
    @Test
    public void removerItemRecebidoComoParametro(){
        Inventario inventario = new Inventario(1);
        Item espada = new Item(2, "Espada");
        Item adaga = new Item(4, "Adaga");
        inventario.adicionar(espada);
        inventario.adicionar(adaga);
        inventario.remover(espada);
        assertEquals(adaga, inventario.getItens().get(0));
    }
    
    
    
    
}
