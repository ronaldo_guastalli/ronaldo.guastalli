
public abstract class Personagem
{
    protected String nome;
    protected Status status;
    protected Inventario inventario;
    protected double qtdVida, qtdDano;
    protected boolean perdeItem;
    private int ataque;
    
    
    
    protected Personagem(String nome){
        this.nome = nome;
        inventario = new Inventario(1);
        status = Status.RECEM_CRIADO;
        this.qtdDano = 0.0;
        this.perdeItem = false;
        this.ataque = 0;
    }
    
    protected String getNome(){
        return this.nome;
    }
    
    protected void setNome(String nome){
        this.nome = nome;
    }
    
    protected Status getStatus(){
        return this.status;
    }
    
    protected Inventario getInventario(){
        return this.inventario;
    }
    
    protected double getQtdVida(){
        return this.qtdVida;
    }
    
    protected int getAtaque(){
       return this.ataque;
    }
    
    protected void setAtaque(int numeroAtaque){
        this.ataque = numeroAtaque;
    }
    
    protected void ganharItem(Item item){
        this.inventario.adicionar(item);
    }
    
    protected void perderItem(Item item){
        this.inventario.remover(item);
    }
    
    protected boolean podeTirarVida(){
        return this.qtdVida > 0;
    }
    
    protected double calcularDano() {
        return this.qtdDano;
    }
    
    protected void sofrerDano(){
        this.status = Status.SOFREU_DANO;
        this.qtdVida -= this.qtdVida >= this.qtdDano ? calcularDano() : this.qtdVida;
        if(this.qtdVida == 0){
            this.status = Status.MORTO;
        };
    }
    
    public boolean equals(Object obj){
       Personagem outroPersonagem = (Personagem)obj;
       return this.nome.equals(outroPersonagem.getNome())
        && this.inventario.quantidadeTotalDeItens() == outroPersonagem.getInventario().quantidadeTotalDeItens()
        && this.qtdVida == outroPersonagem.getQtdVida()
        && this.inventario.getDescricoesItens().equals(outroPersonagem.getInventario().getDescricoesItens())
        && this.status.equals(outroPersonagem.getStatus());
    }
    
    protected void ataque(){
       this.ataque++;
    }   
    
    public abstract String imprimirResumo();
}


