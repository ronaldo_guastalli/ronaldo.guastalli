import java.util.*;
public class Exercito
{
    private final static ArrayList<Class> TIPOS_PRIMITIVOS =
           new ArrayList<>(
            Arrays.asList(ElfoVerde.class,
            ElfoNoturno.class)
           );
    private ArrayList<Elfo> exercitoElfos;
    public Exercito(){
        exercitoElfos = new ArrayList<>(1);
    }
    
    
    public void alistar(Elfo elfo){
        if((elfo instanceof ElfoVerde)
        ||
        (elfo instanceof ElfoNoturno)){
            this.exercitoElfos.add(elfo);
        }
    }
    
    public ArrayList<Elfo> buscarElfo(Status status){
        ArrayList<Elfo> elfosSelecionados = new ArrayList<>();
        if(!exercitoElfos.isEmpty()){
            for(Elfo elfo : exercitoElfos){
                if(elfo.getStatus().equals(status)){
                    elfosSelecionados.add(elfo);
                }
            }
        }else{
            elfosSelecionados = null;
        }
        return elfosSelecionados;
    }
    
    public Elfo getElfo(int indice){
        return exercitoElfos.get(indice);
    }
}
