

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoNoturnoTest
{
   @Test
   public void alterarExperienciaAtirar2Flecha(){
       ElfoNoturno elfoNoturno = new ElfoNoturno("Legolas");
       Dwarf dwarf = new Dwarf("Gimli");
       elfoNoturno.atirarFlecha(dwarf);
       elfoNoturno.atirarFlecha(dwarf);
       assertEquals(0, elfoNoturno.getQtdFlecha());
       assertEquals(6, elfoNoturno.getExperiencia());
    }
    
    @Test
    public void sofreDanoDe15UnidadesDeVidaAtirar1Flecha(){
        ElfoNoturno elfoNoturno = new ElfoNoturno("Legolas");
        Dwarf dwarf = new Dwarf("Gimli");
        elfoNoturno.atirarFlecha(dwarf);
        assertEquals(1, elfoNoturno.getQtdFlecha());
        assertEquals(85.0, elfoNoturno.getQtdVida(), 1e-8);
    }
    @Test
    public void criarUmElfoNoturno(){
        ElfoNoturno elfoNoturno = new ElfoNoturno("Legolas");
        Dwarf dwarf = new Dwarf("Gimli");
        assertEquals(2, elfoNoturno.getQtdFlecha());
        assertEquals(100.0, elfoNoturno.getQtdVida(), 1e-8);
    }
    
    // @Test
    // public void sofrerDanoMaiorQueQuatidadeDeVida(){
       // ElfoNoturno elfoNoturno = new ElfoNoturno("Legolas");
       // Dwarf dwarf = new Dwarf("Gimli");
       // Item flecha = new Item(5, "Flecha");
       // elfoNoturno.ganharItem(flecha);
       // elfoNoturno.atirarFlecha(dwarf);//15
       // elfoNoturno.atirarFlecha(dwarf);//30
       // elfoNoturno.atirarFlecha(dwarf);//45
       // elfoNoturno.atirarFlecha(dwarf);//60
       // elfoNoturno.atirarFlecha(dwarf);//75
       // elfoNoturno.atirarFlecha(dwarf);//90
       // elfoNoturno.atirarFlecha(dwarf);//105
       // assertEquals(7, (elfoNoturno.getInventario().buscar("Flecha").getQuantidade() +
        // elfoNoturno.getInventario().obter(2).getQuantidade()));
       // assertEquals(21, elfoNoturno.getExperiencia());
       // assertEquals(0.0, elfoNoturno.getQtdVida(), 1e-8);
    
    // }
}
