import java.util.*;
public class Elfo extends Personagem {
    private int indiceFlecha;
    protected int experiencia, qtdExperienciaPorAtaque;
    
    {
        this.inventario = new Inventario(2);
        this.indiceFlecha = 1;
        this.qtdExperienciaPorAtaque = 1;
        this.experiencia = 0;
        this.qtdVida = 100.0;
    }
    
    public Elfo(String nome){
        super(nome);
        inventario.adicionar(new Item(1, "Arco"));
        inventario.adicionar(new Item(2, "Flecha"));
        
        this.inventario.adicionarMap("arco", new Item(1, "Arco"));
        this.inventario.adicionarMap("flecha", new Item(2, "Flecha"));
        
        //Contador contaElfo = new Contador();
        Contador.conta++;
    }
       
    public int getExperiencia(){
        return this.experiencia;
    }
    
    public Item getFlecha(){
        return this.inventario.obter(this.indiceFlecha);
    }
    
    public int getQtdFlecha(){
        return this.getFlecha().getQuantidade();
    }
    
    private boolean podeAtirarFlecha(){
        return this.getFlecha().getQuantidade() > 0;        
    }
    
    //trocar para personagem passando podeAtirar(key)
    private boolean podeAtirarFlechaMap(){
        return this.inventario.obterMap("flecha").getQuantidade() > 0;        
    }
    
    public void atirarFlecha(Dwarf dwarf){
        this.ataque();
        int qtdAtual = this.getFlecha().getQuantidade();
        if (podeAtirarFlecha()){
            this.getFlecha().setQuantidade(qtdAtual - 1);
            this.alterarExperiencia();
            this.sofrerDano();
            dwarf.sofrerDano();           
        }        
    }
    
    public void atirarFlechaMap(Dwarf dwarf){
        this.ataque();
        int qtdAtual = this.inventario.obterMap("flecha").getQuantidade();
        if (podeAtirarFlechaMap()){
            this.inventario.obterMap("flecha").setQuantidade(qtdAtual - 1);
            //this.getFlecha().setQuantidade(qtdAtual - 1);
            this.alterarExperiencia();
            this.sofrerDano();
            dwarf.sofrerDano();
        }        
    }
    
    protected void alterarExperiencia(){
        experiencia = experiencia + this.qtdExperienciaPorAtaque;
    }
    
    public String imprimirResumo(){
        return "elfo";
    }
    
    
}