
import java.util.*;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoVerdeTest
{
    @Test
    public void atiraUmaFlechaGanha2Experiencias(){
        ElfoVerde elfosVerdes = new ElfoVerde("Legolas");
        Dwarf dwarf = new Dwarf("Gimli");
        elfosVerdes.atirarFlecha(dwarf);
        assertEquals(2, elfosVerdes.getExperiencia());
        assertEquals(100, dwarf.getQtdVida(), Constantes.DELTA);
    }
    
    @Test
    public void ganharItemComDescricaoEspecifica(){
        ElfoVerde elfosVerdes = new ElfoVerde("Legolas");
        Item arco = new Item(1, "Arco");//nasce
        Item flecha = new Item(2, "Flecha");//nasce
        Item espada = new Item(2, "Espada de aço valiriano"); //ok
        Item flecha2 = new Item(2, "Flecha de ouro");
        Item flecha3 = new Item(2, "Flecha de Vidro"); //ok
        elfosVerdes.ganharItem(espada);
        elfosVerdes.ganharItem(flecha2);
        elfosVerdes.ganharItem(flecha3);
        assertEquals(4, elfosVerdes.getInventario().getItens().size());
        ArrayList<Item> esperado = new ArrayList<>(
                                       Arrays.asList(arco, flecha, espada, flecha3));
        assertEquals(esperado, elfosVerdes.getInventario().getItens());
                                                 
    }
    
    @Test
    public void perderItemComDescricaoEspecifica(){
        ElfoVerde elfosVerdes = new ElfoVerde("Legolas");
        Item arco = new Item(1, "Arco");//nasce
        Item flecha = new Item(2, "Flecha");//nasce
        Item espada = new Item(2, "Espada de aço valiriano"); //ok
        Item flecha2 = new Item(2, "Flecha de ouro");
        Item flecha3 = new Item(2, "Flecha de Vidro"); //ok
        elfosVerdes.ganharItem(espada);
        elfosVerdes.ganharItem(flecha2);
        elfosVerdes.ganharItem(flecha3);
        elfosVerdes.perderItem(espada);
        elfosVerdes.perderItem(flecha2);
        elfosVerdes.perderItem(flecha3);
        assertEquals(2, elfosVerdes.getInventario().getItens().size());
        ArrayList<Item> esperado = new ArrayList<>(
                                       Arrays.asList(arco, flecha));
        assertEquals(esperado, elfosVerdes.getInventario().getItens());                                      
    }
}
