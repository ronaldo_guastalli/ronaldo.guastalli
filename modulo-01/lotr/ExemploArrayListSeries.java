import java.util.*;

public class ExemploArrayListSeries {
    public void indicar() {
        ArrayList<String> lista = new ArrayList<>();
        lista.add("Stranger Things");
        lista.add("True detective");
        lista.add("Mr. Robot");
        lista.add("Punisher");
        lista.add("Dark");
        lista.add("Dexter");
        lista.add("Breaking Bad");
        lista.add("Game of Thrones");
        lista.add("How I met your mother");
        lista.add("Westworld");
        lista.add("A casa de papel");
        lista.add("Black Mirror");
        lista.add("Veep");
        lista.add("Lucifer");
        lista.add("The Last Kingdom");
        lista.add("Two and a half men");
        lista.add("24 horas");

        for (int i = 0; i < lista.size(); i++) {
            System.out.println(lista.get(i));
        }

        boolean taVazia = lista.isEmpty(); // lista.size() == 0;
        System.out.println(taVazia);

        lista.remove(8);

        for (String serie : lista) {
            System.out.println(serie);
        }
    }
}





