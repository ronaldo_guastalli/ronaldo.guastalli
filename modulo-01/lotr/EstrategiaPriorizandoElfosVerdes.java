import java.util.*;

public class EstrategiaPriorizandoElfosVerdes implements EstrategiaDeAtaque {

    private ArrayList<Elfo> bubble(ArrayList<Elfo> elfos) {
        boolean houverTroca = true;
        while (houverTroca) {
            houverTroca = false;
            for (int i = 0; i < elfos.size() - 1; i++) {

                Elfo elfoAtual = elfos.get(i);
                Elfo elfoProximo = elfos.get(i + 1);

                // só precisa trocar se um elfo noturno vier antes de um elfo verde
                boolean precisaTrocar = elfoAtual instanceof ElfoNoturno && elfoProximo instanceof ElfoVerde;

                if (precisaTrocar) {
                    elfos.set(i, elfoProximo);
                    elfos.set(i + 1, elfoAtual);
                    houverTroca = true;
                }

            }

        }
        return elfos;
    }
    
    // Dwarf dwarf = new Dwarf("");
    // elfo.atirarFlecha(dwarf);
    // elfo.atirarFlecha(new Dwarf(""));
    
    // https://docs.oracle.com/javase/8/docs/api/java/util/Comparator.html#compare-T-T-
    private ArrayList<Elfo> collections(ArrayList<Elfo> elfos) {
        // Collections.sort(elfos, new ComparadorDeElfos());
        Collections.sort(elfos, new Comparator<Elfo>() {
            public int compare(Elfo elfoAtual, Elfo proximoElfo) {
                boolean mesmoTipo = elfoAtual.getClass() == proximoElfo.getClass();
                
                if (mesmoTipo) {
                    return 0;
                }
                
                return elfoAtual instanceof ElfoVerde && proximoElfo instanceof ElfoNoturno ? -1 : 1;
            }
        });
        return elfos;
    }

    public ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> elfos) {
        return collections(elfos);
    }
}