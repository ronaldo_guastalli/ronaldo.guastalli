package br.com.dbccompany.integracao;



import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.dbccompany.entity.Cidade;
import br.com.dbccompany.entity.Estado;
import br.com.dbccompany.repository.EstadoRepository;





@RunWith(SpringRunner.class)
@DataJpaTest
public class EstadoIntegrationTests {
	
	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private EstadoRepository estadoRepository;
	
	@Test
	public void achaEtadoPorNome() {
		Estado estado = new Estado();
		estado.setNome("Rio de Janeiro");
		entityManager.persist(estado);
		entityManager.flush();		
		Estado found = estadoRepository.findByNome(estado.getNome());		
		assertThat(found.getNome()).isEqualTo(estado.getNome());
	}
	
	@Test
	public void listaCidadesdoEstado() {
		Estado estado = new Estado();
		estado.setNome("Rio de Janeiro");			
		
		List<Cidade> cidadeList = new ArrayList<>();
		Cidade cidade = new Cidade();
		cidade.setNome("Porto Alegre");
		cidade.setEstado(estado);
		cidadeList.add(cidade);
		
		estado.setCidade(cidadeList);
		entityManager.persist(estado);
		entityManager.persist(cidade);
		entityManager.flush();		
		Estado foundEstado = estadoRepository.findByCidade(cidadeList);		
		assertThat(foundEstado.getCidade().get(0).getNome()).isEqualTo(cidade.getNome());
		assertEquals(1, foundEstado.getCidade().size());
	}
	
//	@Test
//	public void lista3CidadesdoNoEstado() {
//		Estado estado = new Estado();
//		estado.setNome("Rio Grande do Sul");			
//		
//		List<Cidade> cidadeList = new ArrayList<>();
//		Cidade cidade = new Cidade();
//		cidade.setNome("Canoas");
//		cidade.setEstado(estado);
//		cidadeList.add(cidade);
//		
//		Cidade cidade1 = new Cidade();
//		cidade1.setNome("Porto Alegre");
//		cidade1.setEstado(estado);
//		cidadeList.add(cidade1);
//		
//		Cidade cidade2 = new Cidade();
//		cidade2.setNome("Esteio");
//		cidade2.setEstado(estado);
//		cidadeList.add(cidade2);
//		
//		estado.setCidade(cidadeList);
//		entityManager.persist(estado);
//		entityManager.persist(cidade);
//		entityManager.persist(cidade1);
//		entityManager.persist(cidade2);
//		entityManager.flush();		
//		Estado foundEstado = estadoRepository.findByCidade(cidadeList);		
//		assertThat(foundEstado.getCidade().get(0).getNome()).isEqualTo(cidade.getNome());
//		assertEquals("Canoas", cidade.getNome());
//		assertEquals(2, foundEstado.getCidade().size());
//	}
}
