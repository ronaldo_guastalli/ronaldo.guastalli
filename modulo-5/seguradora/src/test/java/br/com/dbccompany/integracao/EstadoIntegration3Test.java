package br.com.dbccompany.integracao;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import br.com.dbccompany.SeguradoraApplicationTests;
import br.com.dbccompany.controller.EstadoController;

public class EstadoIntegration3Test extends SeguradoraApplicationTests {

	private MockMvc mvc;
	
	@Autowired
	private EstadoController controller;
	
	@Before
	public void setUp() {
		this.mvc = MockMvcBuilders.standaloneSetup(controller).build();
	}
	
	@Test
	public void statusOk() throws Exception{
		this.mvc.perform(MockMvcRequestBuilders.get("/api/estado/")).andExpect(MockMvcResultMatchers.status().isOk());
	}
}
