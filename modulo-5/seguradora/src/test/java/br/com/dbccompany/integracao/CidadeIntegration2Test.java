package br.com.dbccompany.integracao;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertArrayEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.dbccompany.entity.Bairro;
import br.com.dbccompany.entity.Cidade;
import br.com.dbccompany.entity.Estado;
import br.com.dbccompany.repository.CidadeRepository;

@RunWith(SpringRunner.class)
public class CidadeIntegration2Test {

	@MockBean
	private CidadeRepository cidadeRepository;
	
	@Autowired
	ApplicationContext context;
	
	@Before
	public void setUp() {
		Cidade cidade = new Cidade();
		Cidade cidade2 = new Cidade();
		Estado estado = new Estado();
		Bairro bairro = new Bairro();
		Bairro bairro2 = new Bairro();
		List<Bairro> bairros = new ArrayList<Bairro>();
		List<Cidade> cidades = new ArrayList<Cidade>();	
		
		estado.setNome("Rio grande do Sul");
		
		cidade.setNome("Tupã");		
		cidade.setEstado(estado);
		cidades.add(cidade);//
		
		cidade2.setNome("Marilia");
		cidade2.setEstado(estado);
		cidades.add(cidade2);//
		
		bairro.setCidade(cidade);
		bairro.setNome("Igara");
		bairros.add(bairro);
		
		bairro2.setCidade(cidade);
		bairro2.setNome("Mathias");
		bairros.add(bairro);
		
		
		Mockito.when(cidadeRepository.findByNome(cidade.getNome())).thenReturn(cidade);	
		Mockito.when(cidadeRepository.findAllByEstado(estado)).thenReturn(cidades);

	}
	
	@Test
	public void acharCidadePorNome() {
		String nome = "Tupã";
		Cidade found = cidadeRepository.findByNome(nome);
		assertThat(found.getNome()).isEqualTo(nome);
	}
	
	@Test
	public void listaDeBairro() {		
		Estado estado = new Estado();
		estado.setNome("Rio grande do Sul");
		
		List<Cidade> cidades = new ArrayList<>();
		Cidade cidadeTest = new Cidade();
		cidadeTest.setNome("Marilia");
		cidadeTest.setEstado(estado);
		cidades.add(cidadeTest);//
		
		List<Cidade> found = cidadeRepository.findAllByEstado(estado);
		
		assertThat(cidades.get(0).getNome()).isEqualTo(found.get(1).getNome());
	}
}
