package br.com.dbccompany.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.entity.Endereco;
import br.com.dbccompany.entity.EnderecoPessoas;
import br.com.dbccompany.entity.Pessoas;


public interface EnderecoPessoasRepository extends CrudRepository<EnderecoPessoas, Integer> {

	List<EnderecoPessoas> findAllByEndereco(Endereco endereco);
	List<EnderecoPessoas> findAllByPessoa(Pessoas pessoa);
	
}
