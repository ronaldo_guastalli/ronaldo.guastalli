package br.com.dbccompany.service.contratos;

import java.util.List;

public interface ServicePadrao<T> {
	
	List<T> findAll();
	T save(T t);
	T findById(Integer id);
	T update(Integer id, T t);
	void deleteById(Integer id);
}
