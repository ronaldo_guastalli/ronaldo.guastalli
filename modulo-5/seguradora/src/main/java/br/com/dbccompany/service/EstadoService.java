package br.com.dbccompany.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.entity.Estado;
import br.com.dbccompany.repository.EstadoRepository;
import br.com.dbccompany.service.contratos.EstadoServiceContrato;

@Service
public class EstadoService implements EstadoServiceContrato{

	@Autowired
	private EstadoRepository estadoRepository;

	@Override
	public List<Estado> findAll() {		
		return (List<Estado>) estadoRepository.findAll();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Estado save(Estado t) {
		return estadoRepository.save(t);
	}

	@Override
	public Estado findById(Integer id) {
		return estadoRepository
				.findById(id)
				.orElse(null);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Estado update(Integer id, Estado t) {
		t.setId(id);
		return estadoRepository.save(t);
	}

	@Override
	public void deleteById(Integer id) {
		estadoRepository.deleteById(id);

	}
	
	

}
