package br.com.dbccompany.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.entity.Seguradora;
import br.com.dbccompany.entity.ServicoContratado;
import br.com.dbccompany.entity.Servicos;
import br.com.dbccompany.service.SeguradoraService;
import br.com.dbccompany.service.ServicosService;

@Controller
@RequestMapping("/api/servicos")
public class ServicosController{
	
	@Autowired
	ServicosService servicosService;
	
	@Autowired
	SeguradoraService seguradoraService;

	@GetMapping(value="/")
	@ResponseBody
	public List<Servicos> findAll(){
		return servicosService.findAll();
	}
	
	@PostMapping(path = "/novo")
	@ResponseBody
	public Servicos save(@RequestBody Servicos servicos) {
		
		return servicosService.save(servicos);
	}	
	
	@PutMapping(value="/editar/{id}")
	@ResponseBody
	public Servicos update(@PathVariable Integer id, @RequestBody Servicos servicos) {
		servicos.setId(id);
		return servicosService.save(servicos);
	}
	
	@DeleteMapping(value="/deletar/{id}")
	public void deleteById(@PathVariable Integer id) {
		servicosService.deleteById(id);

	}
	
	@GetMapping(value="/servicoscontratados")
	public List<ServicoContratado> relatorioServicosContratados(@RequestBody Seguradora seguradora){
		return servicosService.relatorioServicosContratados(seguradora);
	}


}
