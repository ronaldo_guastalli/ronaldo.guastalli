package br.com.dbccompany.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.entity.Seguradora;
import br.com.dbccompany.entity.ServicoContratado;
import br.com.dbccompany.entity.Servicos;
import br.com.dbccompany.repository.ServicosRepository;
import br.com.dbccompany.service.contratos.ServicePadrao;

@Service
public class ServicosService implements ServicePadrao<Servicos> {
	
	@Autowired
	private ServicosRepository servicosRepository;

	@Override
	public List<Servicos> findAll() {		
		return (List<Servicos>) servicosRepository.findAll();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Servicos save(Servicos t) {
		return servicosRepository.save(t);
	}

	@Override
	public Servicos findById(Integer id) {
		return servicosRepository
				.findById(id)
				.orElse(null);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Servicos update(Integer id, Servicos t) {
		t.setId(id);
		return servicosRepository.save(t);
	}

	@Override
	public void deleteById(Integer id) {
		servicosRepository.deleteById(id);

	}
	
	public List<ServicoContratado> relatorioServicosContratados(Seguradora seguradora){
		return servicosRepository.findAllBySeguradora(seguradora);
	}


}
