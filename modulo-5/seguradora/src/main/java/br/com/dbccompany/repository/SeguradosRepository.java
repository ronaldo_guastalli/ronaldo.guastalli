package br.com.dbccompany.repository;

import java.util.List;

import br.com.dbccompany.entity.EnderecoPessoas;
import br.com.dbccompany.entity.Pessoas;
import br.com.dbccompany.entity.Segurados;


public interface SeguradosRepository extends PessoasRepository<Segurados> {

	Segurados findByNome(String nome);
	Segurados findByCpf(Integer cpf);
	Segurados findByPai(String pai);
	Segurados findByMae(String mae);
	Segurados findByTelefone(Integer telefone);
	Segurados findByEmail(String email);
	List<EnderecoPessoas> findAllByCpf(Integer cpf);
	List<EnderecoPessoas> findAllByTelefone(Integer telefone);
	List<EnderecoPessoas> findAllByEmail(String email);


}
