package br.com.dbccompany.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="pessoas")
@Inheritance(strategy=InheritanceType.JOINED)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Pessoas {

	@Id
	@SequenceGenerator(allocationSize = 1, name = "pessoas_seq", sequenceName = "pessoas_seq")
	@GeneratedValue(generator="pessoas_seq", strategy = GenerationType.SEQUENCE)
	private Integer id;

	@Column(name="nome", nullable=false, length=150)
	private String nome;

	@Column(name="cpf", nullable=false, unique=true)
	private Integer cpf;

	@Column(name="pai", nullable=false, length=150)
	private String pai;

	@Column(name="mae", nullable=false, length=150)
	private String mae;

	@Column(name="telefone", nullable=false, unique=true)
	private Integer telefone;

	@Column(name="email", nullable=false, length=150, unique=true)
	private String email;
	
	@OneToMany(mappedBy="pessoa")
	private List<EnderecoPessoas> enderecoPessoas = new ArrayList<>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getCpf() {
		return cpf;
	}

	public void setCpf(Integer cpf) {
		this.cpf = cpf;
	}

	public String getPai() {
		return pai;
	}

	public void setPai(String pai) {
		this.pai = pai;
	}

	public String getMae() {
		return mae;
	}

	public void setMae(String mae) {
		this.mae = mae;
	}

	public Integer getTelefone() {
		return telefone;
	}

	public void setTelefone(Integer telefone) {
		this.telefone = telefone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<EnderecoPessoas> getEnderecoPessoas() {
		return enderecoPessoas;
	}

	public void setEnderecoPessoas(List<EnderecoPessoas> enderecoPessoas) {
		this.enderecoPessoas = enderecoPessoas;
	}

//	@OneToMany(mappedBy="pessoa")
//	private List<Endereco> enderecoPessoas = new ArrayList<>();



}
