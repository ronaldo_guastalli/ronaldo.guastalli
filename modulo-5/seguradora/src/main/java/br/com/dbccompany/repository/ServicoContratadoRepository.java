package br.com.dbccompany.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.entity.Corretor;
import br.com.dbccompany.entity.Seguradora;
import br.com.dbccompany.entity.Segurados;
import br.com.dbccompany.entity.ServicoContratado;
import br.com.dbccompany.entity.Servicos;


public interface ServicoContratadoRepository extends CrudRepository<ServicoContratado, Integer> {

	ServicoContratado findByDescricao(String descricao);
	ServicoContratado findByValor(Double valor);
	ServicoContratado findBySeguradora(Seguradora seguradora);
	ServicoContratado findByServico(Servicos servico);
	ServicoContratado findByPessoasSegurada(Segurados pessoasSegurada);
	ServicoContratado findByPessoasCorretor(Corretor pessoasCorretor);
	

}
