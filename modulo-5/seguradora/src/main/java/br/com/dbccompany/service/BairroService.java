package br.com.dbccompany.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.entity.Bairro;
import br.com.dbccompany.repository.BairroRepository;
import br.com.dbccompany.service.contratos.ServicePadrao;

@Service
public class BairroService implements  ServicePadrao<Bairro>{

	@Autowired
	private BairroRepository bairroRepository;

	@Override
	public List<Bairro> findAll() {		
		return (List<Bairro>) bairroRepository.findAll();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Bairro save(Bairro t) {
		return bairroRepository.save(t);
	}

	@Override
	public Bairro findById(Integer id) {
		return bairroRepository
				.findById(id)
				.orElse(null);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Bairro update(Integer id, Bairro t) {
		t.setId(id);
		return bairroRepository.save(t);
	}

	@Override
	public void deleteById(Integer id) {
		bairroRepository.deleteById(id);

	}


}
