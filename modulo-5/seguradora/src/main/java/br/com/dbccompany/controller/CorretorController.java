package br.com.dbccompany.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.entity.Corretor;
import br.com.dbccompany.entity.Pessoas;
import br.com.dbccompany.service.CorretorService;

@Controller
@RequestMapping("/api/corretor")
public class CorretorController{
	
	@Autowired
	CorretorService corretorService;

	@GetMapping(value="/")
	@ResponseBody
	public List<Corretor> findAll(){
		return corretorService.findAll();
	}
	
	@PostMapping(path = "/novo")
	@ResponseBody
	public Corretor save(@RequestBody Corretor corretor) {
		return corretorService.save(corretor);
	}	
	
	@PutMapping(value="/editar/{id}")
	@ResponseBody
	public Corretor update(@PathVariable Integer id, @RequestBody Corretor corretor) {
		corretor.setId(id);
		return corretorService.save(corretor);
	}
	
	@DeleteMapping(value="/deletar/{id}")
	public void deleteById(@PathVariable Integer id) {
		corretorService.deleteById(id);
	}
	
	@GetMapping(value="/{telefone}")
	public Pessoas findByTelefone(@PathVariable Integer telefone) {
		return corretorService.findByTelefone(telefone);
	}
	
	@GetMapping(value="/{cpf}")
	public Pessoas findByCpf(@PathVariable Integer cpf) {
		return corretorService.findByCpf(cpf);
	}
	
	@GetMapping(value="/{email}")
	public Pessoas findByCpf(@PathVariable String email) {
		return corretorService.findByEmail(email);
	}


}
