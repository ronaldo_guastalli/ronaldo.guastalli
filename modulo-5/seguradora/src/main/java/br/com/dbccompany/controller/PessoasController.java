//package br.com.dbccompany.controller;
//
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.DeleteMapping;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.PutMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//import br.com.dbccompany.entity.Endereco;
//import br.com.dbccompany.entity.Pessoas;
//import br.com.dbccompany.service.EnderecoService;
//import br.com.dbccompany.service.PessoasService;
//
//@Controller
//@RequestMapping("/api/pessoas")
//public class PessoasController{
//	
//	@Autowired
//	PessoasService pessoasService;
//	
//	@Autowired
//	EnderecoService enderecoService;
//
//	@GetMapping(value="/")
//	@ResponseBody
//	public List<Pessoas> findAll(){
//		return pessoasService.findAll();
//	}
//	
//	@PostMapping(path = "/novo")
//	@ResponseBody
//	public Pessoas save(@RequestBody Pessoas pessoas, @RequestBody Endereco endereco) {
//		enderecoService.save(endereco);
//		return pessoasService.save(pessoas);
//	}	
//	
//	@PutMapping(value="/editar/{id}")
//	@ResponseBody
//	public Pessoas update(@PathVariable Integer id, @RequestBody Pessoas pessoas) {
//		pessoas.setId(id);
//		return pessoasService.save(pessoas);
//	}
//	
//	@DeleteMapping(value="/deletar/{id}")
//	public void deleteById(@PathVariable Integer id) {
//		pessoasService.deleteById(id);
//
//	}
//	
//	@GetMapping(value="/{telefone}")
//	public Pessoas findByTelefone(@PathVariable Integer telefone) {
//		return pessoasService.findByTelefone(telefone);
//	}
//	
//	@GetMapping(value="/{cpf}")
//	public Pessoas findByCpf(@PathVariable Integer cpf) {
//		return pessoasService.findByCpf(cpf);
//	}
//	
//	@GetMapping(value="/{email}")
//	public Pessoas findByCpf(@PathVariable String email) {
//		return pessoasService.findByEmail(email);
//	}
//
//}
