package br.com.dbccompany.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.entity.Seguradora;
import br.com.dbccompany.entity.ServicoContratado;
import br.com.dbccompany.entity.Servicos;


public interface ServicosRepository extends CrudRepository<Servicos, Integer>{

	Servicos findByNome(String nome);	
	Servicos findByDescricao(String descricao);
	Servicos findByValorPadrao(Double valorPadrao);
	List<ServicoContratado> findAllBySeguradora(Seguradora seguradora);

	

	

	

}
