package br.com.dbccompany.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.entity.Pessoas;
import br.com.dbccompany.entity.Segurados;
import br.com.dbccompany.repository.SeguradosRepository;
import br.com.dbccompany.service.contratos.SeguradosServiceContrato;

@Service
public class SeguradosService implements SeguradosServiceContrato {
	
	@Autowired
	private SeguradosRepository seguradosRepository;

	@Override
	public List<Segurados> findAll() {		
		return (List<Segurados>) seguradosRepository.findAll();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Segurados save(Segurados t) {
		return seguradosRepository.save(t);
	}

	@Override
	public Segurados findById(Integer id) {
		return seguradosRepository
				.findById(id)
				.orElse(null);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Segurados update(Integer id, Segurados t) {
		t.setId(id);
		return seguradosRepository.save(t);
	}

	@Override
	public void deleteById(Integer id) {
		seguradosRepository.deleteById(id);

	}
	
	public Pessoas findByCpf(Integer cpf) {
		return seguradosRepository.findByCpf(cpf);
	}
	
	public Pessoas findByEmail(String email) {
		return seguradosRepository.findByEmail(email);
	}
	
	public Pessoas findByTelefone(Integer telefone) {
		return seguradosRepository.findByTelefone(telefone);
	}

}
