package br.com.dbccompany.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@PrimaryKeyJoinColumn(name="id_pessoas")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Segurados extends Pessoas {

	@Column(name="qtd_servicos", nullable=false)
	private Integer qtdServicos;

	@OneToMany(mappedBy="pessoasSegurada")
	private List<ServicoContratado> servicoContratado = new ArrayList<>();

	public Integer getQtdServicos() {
		return qtdServicos;
	}

	public void setQtdServicos(Integer qtdServicos) {
		this.qtdServicos = qtdServicos;
	}

	public List<ServicoContratado> getServicoContratado() {
		return servicoContratado;
	}

	public void setServicoContratado(List<ServicoContratado> servicoContratado) {
		this.servicoContratado = servicoContratado;
	}

}
