package br.com.dbccompany.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="seguradora")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Seguradora {

	@Id
	@SequenceGenerator(allocationSize = 1, name = "seguradora_seq", sequenceName = "seguradora_seq")
	@GeneratedValue(generator="seguradora_seq", strategy = GenerationType.SEQUENCE)
	private Integer id;

	@Column(name="nome", nullable=false, length=150)
	private String nome;

	@Column(name="cnpj", nullable=false, unique=true)
	private Integer cnpj;

	@ManyToMany(mappedBy="seguradora")
	private List<Servicos> servicos = new ArrayList<>();

	@OneToMany(mappedBy="servico")
	private List<ServicoContratado> servicoContratado = new ArrayList<>();
	
	//gerar a FK
	@OneToOne(mappedBy="seguradora")
	private EnderecoSeguradoras enderecoSeguradoras;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getCnpj() {
		return cnpj;
	}

	public void setCnpj(Integer cnpj) {
		this.cnpj = cnpj;
	}

	

	public List<Servicos> getServicos() {
		return servicos;
	}

	public void setServicos(List<Servicos> servicos) {
		this.servicos = servicos;
	}

	public List<ServicoContratado> getServicoContratado() {
		return servicoContratado;
	}

	public void setServicoContratado(List<ServicoContratado> servicoContratado) {
		this.servicoContratado = servicoContratado;
	}

	public EnderecoSeguradoras getEnderecoSeguradoras() {
		return enderecoSeguradoras;
	}

	public void setEnderecoSeguradoras(EnderecoSeguradoras enderecoSeguradoras) {
		this.enderecoSeguradoras = enderecoSeguradoras;
	}

	
	

}
