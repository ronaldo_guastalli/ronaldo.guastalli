package br.com.dbccompany.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.entity.Corretor;
import br.com.dbccompany.entity.Segurados;
import br.com.dbccompany.entity.ServicoContratado;
import br.com.dbccompany.repository.CorretorRepository;
import br.com.dbccompany.repository.SeguradosRepository;
import br.com.dbccompany.repository.ServicoContratadoRepository;
import br.com.dbccompany.service.contratos.ServicoContratadoServiceContrato;

@Service
public class ServicoContratadoService implements ServicoContratadoServiceContrato {
	
	@Autowired
	private ServicoContratadoRepository servicoContratadoRepository;
	
	@Autowired
	private CorretorRepository corretorRepository;
	
	@Autowired
	private SeguradosRepository seguradosRepository;

	@Override
	public List<ServicoContratado> findAll() {		
		return (List<ServicoContratado>) servicoContratadoRepository.findAll();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ServicoContratado save(ServicoContratado t) {
		Corretor corretor = corretorRepository.findById(t.getPessoasCorretor().getId()).get();
		Segurados segurado = seguradosRepository.findById(t.getPessoasSegurada().getId()).get();
		segurado.setQtdServicos(segurado.getQtdServicos() + 1);
		corretor.setComissao(corretor.getComissao() + (t.getValor() * 0.1));
		t.setPessoasCorretor(corretor);
		t.setPessoasSegurada(segurado);
		return servicoContratadoRepository.save(t);
	}

	@Override
	public ServicoContratado findById(Integer id) {
		return servicoContratadoRepository
				.findById(id)
				.orElse(null);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ServicoContratado update(Integer id, ServicoContratado t) {
		t.setId(id);
		return servicoContratadoRepository.save(t);
	}

	@Override
	public void deleteById(Integer id) {
		servicoContratadoRepository.deleteById(id);

	}

}
