package br.com.dbccompany.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.entity.Endereco;
import br.com.dbccompany.service.EnderecoService;

@Controller
@RequestMapping("/api/endereco")
public class EnderecoController{
	
	@Autowired
	EnderecoService enderecoService;

	@GetMapping(value="/")
	@ResponseBody
	public List<Endereco> findAll(){
		return enderecoService.findAll();
	}
	
	@PostMapping(path = "/novo")
	@ResponseBody
	public Endereco save(@RequestBody Endereco endereco) {
		return enderecoService.save(endereco);
	}	
	
	@PutMapping(value="/editar/{id}")
	@ResponseBody
	public Endereco update(@PathVariable Integer id, @RequestBody Endereco endereco) {
		endereco.setId(id);
		return enderecoService.save(endereco);
	}
	
	@DeleteMapping(value="/deletar/{id}")
	public void deleteById(@PathVariable Integer id) {
		enderecoService.deleteById(id);

	}


}
