package br.com.dbccompany.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.entity.Endereco;
import br.com.dbccompany.entity.EnderecoSeguradoras;
import br.com.dbccompany.entity.Seguradora;
import br.com.dbccompany.entity.Servicos;
import br.com.dbccompany.service.EnderecoSeguradorasService;
import br.com.dbccompany.service.EnderecoService;
import br.com.dbccompany.service.SeguradoraService;
import br.com.dbccompany.service.ServicosService;

@Controller
@RequestMapping("/api/seguradora")
public class SeguradoraController{
	
	@Autowired
	SeguradoraService seguradoraService;
	
	@Autowired
	EnderecoSeguradorasService enderecoSeguradoraService;
	
	@Autowired
	EnderecoService enderecoService;
	
	@Autowired
	ServicosService servicosService;

	@GetMapping(value="/")
	@ResponseBody
	public List<Seguradora> findAll(){
		return seguradoraService.findAll();
	}
	
	@PostMapping(path = "/novo")
	@ResponseBody
	public Seguradora save(@RequestBody Seguradora seguradora) {
//		EnderecoSeguradoras t = seguradora.getEnderecoSeguradoras();
//		List<Servicos> servicos = seguradora.getServicos();
//		enderecoSeguradoraService.save(t);
//		enderecoService.save(t);
//		servicosService.save(servicos.get(0));
		return seguradoraService.save(seguradora);
	}
	
	@PostMapping(path = "/novo/endereco")
	@ResponseBody
	public Seguradora save(@RequestBody Endereco testeEndereco) {
		return seguradoraService.enderecoSeguradora(testeEndereco);
	}

	
	@PutMapping(value="/editar/{id}")
	@ResponseBody
	public Seguradora update(@PathVariable Integer id, @RequestBody Seguradora seguradora) {
		seguradora.setId(id);
		return seguradoraService.save(seguradora);
	}
	
	@DeleteMapping(value="/deletar/{id}")
	public void deleteById(@PathVariable Integer id) {
		seguradoraService.deleteById(id);

	}
	



}
