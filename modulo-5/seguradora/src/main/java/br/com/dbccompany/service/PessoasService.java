//package br.com.dbccompany.service;
//
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import br.com.dbccompany.entity.Pessoas;
//import br.com.dbccompany.repository.PessoasRepository;
//import br.com.dbccompany.service.contratos.PessoasServiceContrato;
//
//@Service
//public class PessoasService implements PessoasServiceContrato {
//	
//	@Autowired
//	private PessoasRepository pessoasRepository;
//
//	@Override
//	public List<Pessoas> findAll() {		
//		return (List<Pessoas>) pessoasRepository.findAll();
//	}
//
//	@Override
//	@Transactional(rollbackFor = Exception.class)
//	public Pessoas save(Pessoas t) {
//		return pessoasRepository.save(t);
//	}
//
//	@Override
//	public Pessoas findById(Integer id) {
//		return pessoasRepository
//				.findById(id)
//				.orElse(null);
//	}
//
//	@Override
//	@Transactional(rollbackFor = Exception.class)
//	public Pessoas update(Integer id, Pessoas t) {
//		t.setId(id);
//		return pessoasRepository.save(t);
//	}
//
//	@Override
//	public void deleteById(Integer id) {
//		pessoasRepository.deleteById(id);
//
//	}
//	
//	public Pessoas findByCpf(Integer cpf) {
//		return pessoasRepository.findByCpf(cpf);
//	}
//	
//	public Pessoas findByEmail(String email) {
//		return pessoasRepository.findByEmail(email);
//	}
//	
//	public Pessoas findByTelefone(Integer telefone) {
//		return pessoasRepository.findByTelefone(telefone);
//	}
//
//}
