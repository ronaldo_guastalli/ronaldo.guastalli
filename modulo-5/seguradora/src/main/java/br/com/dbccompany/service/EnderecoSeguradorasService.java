package br.com.dbccompany.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.entity.EnderecoSeguradoras;
import br.com.dbccompany.repository.EnderecoSeguradorasRepository;
import br.com.dbccompany.service.contratos.EnderecoSeguradoraServiceContrato;

@Service
public class EnderecoSeguradorasService implements EnderecoSeguradoraServiceContrato {
	
	@Autowired
	private EnderecoSeguradorasRepository enderecoSeguradorasRepository;

	@Override
	public List<EnderecoSeguradoras> findAll() {		
		return (List<EnderecoSeguradoras>) enderecoSeguradorasRepository.findAll();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public EnderecoSeguradoras save(EnderecoSeguradoras t) {
		return enderecoSeguradorasRepository.save(t);
	}

	@Override
	public EnderecoSeguradoras findById(Integer id) {
		return enderecoSeguradorasRepository
				.findById(id)
				.orElse(null);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public EnderecoSeguradoras update(Integer id, EnderecoSeguradoras t) {
		t.setId(id);
		return enderecoSeguradorasRepository.save(t);
	}

	@Override
	public void deleteById(Integer id) {
		enderecoSeguradorasRepository.deleteById(id);

	}

}
