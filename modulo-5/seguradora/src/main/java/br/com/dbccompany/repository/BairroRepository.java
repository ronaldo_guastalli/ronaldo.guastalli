package br.com.dbccompany.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.entity.Bairro;
import br.com.dbccompany.entity.Cidade;

public interface BairroRepository extends CrudRepository<Bairro, Integer> {

	Bairro findByNome(String nome);
	List<Bairro> findAllByCidade(Cidade cidade);

}
