//package br.com.dbccompany.service;
//
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import br.com.dbccompany.entity.SeguradorasServicos;
//import br.com.dbccompany.repository.SeguradorasServicosRepository;
//import br.com.dbccompany.service.contratos.ServicePadrao;
//
//@Service
//public class SeguradorasServicosService implements ServicePadrao<SeguradorasServicos> {
//	
//	@Autowired
//	private SeguradorasServicosRepository seguradorasServicosRepository;
//
//	@Override
//	public List<SeguradorasServicos> findAll() {		
//		return (List<SeguradorasServicos>) seguradorasServicosRepository.findAll();
//	}
//
//	@Override
//	@Transactional(rollbackFor = Exception.class)
//	public SeguradorasServicos save(SeguradorasServicos t) {
//		return seguradorasServicosRepository.save(t);
//	}
//
//	@Override
//	public SeguradorasServicos findById(Integer id) {
//		return seguradorasServicosRepository
//				.findById(id)
//				.orElse(null);
//	}
//
//	@Override
//	@Transactional(rollbackFor = Exception.class)
//	public SeguradorasServicos update(Integer id, SeguradorasServicos t) {
//		t.setId(id);
//		return seguradorasServicosRepository.save(t);
//	}
//
//	@Override
//	public void deleteById(Integer id) {
//		seguradorasServicosRepository.deleteById(id);
//
//	}
//	
//
//
//}
