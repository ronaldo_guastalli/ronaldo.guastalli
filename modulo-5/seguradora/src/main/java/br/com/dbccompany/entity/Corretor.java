package br.com.dbccompany.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@PrimaryKeyJoinColumn(name="id_pessoas")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Corretor extends Pessoas {

	@Column(name="cargo", nullable = false, length=150)
	private String cargo;

	@Column(name="camissao", nullable=false, precision=10, scale=2)
	private Double comissao;

	@OneToMany(mappedBy="pessoasCorretor")
	private List<ServicoContratado> servicoContratado = new ArrayList<>();

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public Double getComissao() {
		return comissao;
	}

	public void setComissao(Double comissao) {
		this.comissao = comissao;
	}

	public List<ServicoContratado> getServicoContratado() {
		return servicoContratado;
	}

	public void setServicoContratado(List<ServicoContratado> servicoContratado) {
		this.servicoContratado = servicoContratado;
	}

}
