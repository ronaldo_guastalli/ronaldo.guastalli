package br.com.dbccompany.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.entity.Estado;
import br.com.dbccompany.entity.Estado;
import br.com.dbccompany.service.EnderecoService;
import br.com.dbccompany.service.EstadoService;

@Controller
@RequestMapping("/api/estado")
public class EstadoController{
	
	@Autowired
	EstadoService estadoService;

	@GetMapping(value="/")
	@ResponseBody
	public List<Estado> findAll(){
		return estadoService.findAll();
	}
	
	@PostMapping(path = "/novo")
	@ResponseBody
	public Estado save(@RequestBody Estado estado) {
		return estadoService.save(estado);
	}	
	
	@PutMapping(value="/editar/{id}")
	@ResponseBody
	public Estado update(@PathVariable Integer id, @RequestBody Estado estado) {
		estado.setId(id);
		return estadoService.save(estado);
	}
	
	@DeleteMapping(value="/deletar/{id}")
	public void deleteById(@PathVariable Integer id) {
		estadoService.deleteById(id);

	}
	

}
