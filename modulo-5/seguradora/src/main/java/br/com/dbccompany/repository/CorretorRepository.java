package br.com.dbccompany.repository;

import java.util.List;

import br.com.dbccompany.entity.Corretor;
import br.com.dbccompany.entity.ServicoContratado;

public interface CorretorRepository extends PessoasRepository<Corretor> {

	Corretor findByCpf(Integer cpf);
	Corretor findByNome(String nome);
	Corretor findByEmail(String email);
	Corretor findByTelefone(Integer telefone);
	
	Corretor findByCargo(String cargo);
	Corretor findByComissao(Double comissao);
	List<ServicoContratado> findAllByServicoContratado(Corretor corretor);

}
