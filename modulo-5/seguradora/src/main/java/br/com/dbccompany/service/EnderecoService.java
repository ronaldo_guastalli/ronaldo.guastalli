package br.com.dbccompany.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.entity.Endereco;
import br.com.dbccompany.repository.EnderecoRepository;
import br.com.dbccompany.service.contratos.EnderecoServiceContrato;

@Service
public class EnderecoService implements EnderecoServiceContrato {
	
	@Autowired
	private EnderecoRepository<Endereco> enderecoRepository;

	@Override
	public List<Endereco> findAll() {		
		return (List<Endereco>) enderecoRepository.findAll();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Endereco save(Endereco t) {
		return enderecoRepository.save(t);
	}

	@Override
	public Endereco findById(Integer id) {
		return enderecoRepository
				.findById(id)
				.orElse(null);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Endereco update(Integer id, Endereco t) {
		t.setId(id);
		return enderecoRepository.save(t);
	}

	@Override
	public void deleteById(Integer id) {
		enderecoRepository.deleteById(id);

	}

}
