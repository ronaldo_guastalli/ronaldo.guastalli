package br.com.dbccompany.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="servicos")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Servicos {

	@Id
	@SequenceGenerator(allocationSize = 1, name = "servicos_seq", sequenceName = "servicos_seq")
	@GeneratedValue(generator="servicos_seq", strategy = GenerationType.SEQUENCE)
	private Integer id;

	@Column(name="nome", nullable=false, length=150)
	private String nome;

	@Column(name="descricao", nullable=false, length=250)
	private String descricao;

	@Column(name="valor_padrao", nullable=false, precision=10, scale=2)
	private Double valorPadrao;
	
	@ManyToMany
	@JoinTable(name = "seguradora_servicos",
    joinColumns = {
        @JoinColumn(name = "id_seguradoras")},
    inverseJoinColumns = {
        @JoinColumn(name = "id_servicos")})
	private List<Seguradora> seguradora = new ArrayList<>();	

	@OneToMany
	private List<ServicoContratado> servicoContratado = new ArrayList<>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Double getValorPadrao() {
		return valorPadrao;
	}

	public void setValorPadrao(Double valorPadrao) {
		this.valorPadrao = valorPadrao;
	}

	

	public List<Seguradora> getSeguradora() {
		return seguradora;
	}

	public void setSeguradora(List<Seguradora> seguradora) {
		this.seguradora = seguradora;
	}

	public List<ServicoContratado> getServicoContratado() {
		return servicoContratado;
	}

	public void setServicoContratado(List<ServicoContratado> servicoContratado) {
		this.servicoContratado = servicoContratado;
	}

	

	

}
