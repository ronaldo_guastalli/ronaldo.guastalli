package br.com.dbccompany.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.entity.Bairro;
import br.com.dbccompany.entity.Bairro;
import br.com.dbccompany.service.BairroService;
import br.com.dbccompany.service.CidadeService;

@Controller
@RequestMapping("/api/bairro")
public class BairroController{
	
	@Autowired
	BairroService bairroService;

	@GetMapping(value="/")
	@ResponseBody
	public List<Bairro> findAll(){
		return bairroService.findAll();
	}
	
	@PostMapping(path = "/novo")
	@ResponseBody
	public Bairro save(@RequestBody Bairro bairro) {
		return bairroService.save(bairro);
	}	
	
	@PutMapping(value="/editar/{id}")
	@ResponseBody
	public Bairro update(@PathVariable Integer id, @RequestBody Bairro bairro) {
		bairro.setId(id);
		return bairroService.save(bairro);
	}
	
	@DeleteMapping(value="/deletar/{id}")
	public void deleteById(@PathVariable Integer id) {
		bairroService.deleteById(id);

	}

}
