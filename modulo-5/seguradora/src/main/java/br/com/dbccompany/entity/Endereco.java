package br.com.dbccompany.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="endereco")
@Inheritance(strategy=InheritanceType.JOINED)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Endereco {

	@Id
	@SequenceGenerator(allocationSize = 1, name = "endereco_seq", sequenceName = "endereco_seq")
	@GeneratedValue(generator="endereco_seq", strategy = GenerationType.SEQUENCE)
	private Integer id;

	@Column(name="logradouro", nullable=false, length=150)
	private String logradouro;

	@Column(name="numero", nullable=false)
	private Integer numero;

	@Column(name="complemento", nullable=true, length=150)
	private String complemento;

	@ManyToOne
	@JoinColumn(name="id_bairro", nullable=false)
	private Bairro bairro;

	@OneToMany(mappedBy="endereco")	
	private List<EnderecoPessoas> enderecoPessoas = new ArrayList<>();
	//private List<Pessoas> enderecoPessoas = new ArrayList<>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public Bairro getBairro() {
		return bairro;
	}

	public void setBairro(Bairro bairro) {
		this.bairro = bairro;
	}

	public List<EnderecoPessoas> getEnderecoPessoas() {
		return enderecoPessoas;
	}

	public void setEnderecoPessoas(List<EnderecoPessoas> enderecoPessoas) {
		this.enderecoPessoas = enderecoPessoas;
	}

	


}
