package br.com.dbccompany.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.entity.Cidade;
import br.com.dbccompany.entity.Cidade;
import br.com.dbccompany.service.CidadeService;
import br.com.dbccompany.service.EstadoService;

@Controller
@RequestMapping("/api/cidade")
public class CidadeController{
	
	@Autowired
	CidadeService cidadeService;

	@GetMapping(value="/")
	@ResponseBody
	public List<Cidade> findAll(){
		return cidadeService.findAll();
	}
	
	@PostMapping(path = "/novo")
	@ResponseBody
	public Cidade save(@RequestBody Cidade cidade) {
		return cidadeService.save(cidade);
	}	
	
	@PutMapping(value="/editar/{id}")
	@ResponseBody
	public Cidade update(@PathVariable Integer id, @RequestBody Cidade cidade) {
		cidade.setId(id);
		return cidadeService.save(cidade);
	}
	
	@DeleteMapping(value="/deletar/{id}")
	public void deleteById(@PathVariable Integer id) {
		cidadeService.deleteById(id);

	}

}
