package br.com.dbccompany.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.entity.Bairro;
import br.com.dbccompany.entity.Endereco;


public interface EnderecoRepository<E extends Endereco> extends CrudRepository<E, Integer> {	

	List<Endereco> findAllByLogradouro(String logradouro);
	List<Endereco> findAllByNumero(Integer numero);
	List<Endereco> findAllByComplemento(String complemento);
	List<Endereco> findAllByBairro(Bairro bairro);

}

//public interface EnderecoRepository extends CrudRepository<Endereco, Integer> {	
//
//	List<Endereco> findAllByLogradouro(String logradouro);
//	List<Endereco> findAllByNumero(Integer numero);
//	List<Endereco> findAllByComplemento(String complemento);
//	List<Endereco> findAllByBairro(Bairro bairro);
//
//}
