package br.com.dbccompany.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.entity.EnderecoSeguradoras;
import br.com.dbccompany.service.EnderecoSeguradorasService;

@Controller
@RequestMapping("/api/endereco/seguradora")
public class EnderecoSeguradorasController{
	
	@Autowired
	EnderecoSeguradorasService enderecoSeguradorasService;

	@GetMapping(value="/")
	@ResponseBody
	public List<EnderecoSeguradoras> findAll(){
		return enderecoSeguradorasService.findAll();
	}
	
	@PostMapping(path = "/novo")
	@ResponseBody
	public EnderecoSeguradoras save(@RequestBody EnderecoSeguradoras enderecoSeguradoras) {
		return enderecoSeguradorasService.save(enderecoSeguradoras);
	}	
	
	@PutMapping(value="/editar/{id}")
	@ResponseBody
	public EnderecoSeguradoras update(@PathVariable Integer id, @RequestBody EnderecoSeguradoras enderecoSeguradoras) {
		enderecoSeguradoras.setId(id);
		return enderecoSeguradorasService.save(enderecoSeguradoras);
	}
	
	@DeleteMapping(value="/deletar/{id}")
	public void deleteById(@PathVariable Integer id) {
		enderecoSeguradorasService.deleteById(id);

	}


}
