//package br.com.dbccompany.controller;
//
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.DeleteMapping;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.PutMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//import br.com.dbccompany.entity.SeguradorasServicos;
//import br.com.dbccompany.service.EnderecoSeguradorasService;
//import br.com.dbccompany.service.EnderecoService;
//import br.com.dbccompany.service.SeguradorasServicosService;
//
//@Controller
//@RequestMapping("/api/seguradoras/servicos")
//public class SeguradorasServicosController{
//	
//	@Autowired
//	SeguradorasServicosService seguradorasServicosService;
//
//	@GetMapping(value="/")
//	@ResponseBody
//	public List<SeguradorasServicos> findAll(){
//		return seguradorasServicosService.findAll();
//	}
//	
//	@PostMapping(path = "/novo")
//	@ResponseBody
//	public SeguradorasServicos save(@RequestBody SeguradorasServicos seguradorasServicos) {
//		return seguradorasServicosService.save(seguradorasServicos);
//	}
//
//	
//	@PutMapping(value="/editar/{id}")
//	@ResponseBody
//	public SeguradorasServicos update(@PathVariable Integer id, @RequestBody SeguradorasServicos seguradorasServicos) {
//		seguradorasServicos.setId(id);
//		return seguradorasServicosService.save(seguradorasServicos);
//	}
//	
//	@DeleteMapping(value="/deletar/{id}")
//	public void deleteById(@PathVariable Integer id) {
//		seguradorasServicosService.deleteById(id);
//
//	}
//
//
//}
