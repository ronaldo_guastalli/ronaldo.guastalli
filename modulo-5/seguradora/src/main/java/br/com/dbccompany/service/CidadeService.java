package br.com.dbccompany.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.entity.Cidade;
import br.com.dbccompany.repository.CidadeRepository;
import br.com.dbccompany.service.contratos.CidadeServiceContrato;

@Service
public class CidadeService implements CidadeServiceContrato{
	
	@Autowired
	private CidadeRepository cidadeRepository;

	@Override
	public List<Cidade> findAll() {		
		return (List<Cidade>) cidadeRepository.findAll();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Cidade save(Cidade t) {
		return cidadeRepository.save(t);
	}

	@Override
	public Cidade findById(Integer id) {
		return cidadeRepository
				.findById(id)
				.orElse(null);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Cidade update(Integer id, Cidade t) {
		t.setId(id);
		return cidadeRepository.save(t);
	}

	@Override
	public void deleteById(Integer id) {
		cidadeRepository.deleteById(id);

	}

}
