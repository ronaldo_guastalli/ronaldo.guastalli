package br.com.dbccompany.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.entity.Endereco;
import br.com.dbccompany.entity.Seguradora;
import br.com.dbccompany.repository.SeguradoraRepository;
import br.com.dbccompany.service.contratos.SeguradoraServiceContrato;

@Service
public class SeguradoraService implements SeguradoraServiceContrato {
	
	@Autowired
	private SeguradoraRepository seguradoraRepository;

	@Override
	public List<Seguradora> findAll() {		
		return (List<Seguradora>) seguradoraRepository.findAll();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Seguradora save(Seguradora t) {
		return seguradoraRepository.save(t);
	}

	@Override
	public Seguradora findById(Integer id) {
		return seguradoraRepository
				.findById(id)
				.orElse(null);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Seguradora update(Integer id, Seguradora t) {
		t.setId(id);
		return seguradoraRepository.save(t);
	}

	@Override
	public void deleteById(Integer id) {
		seguradoraRepository.deleteById(id);

	}
	
	public Seguradora enderecoSeguradora(Endereco endereco) {
		return seguradoraRepository.findByEnderecoSeguradoras(endereco);
	}
	


}
