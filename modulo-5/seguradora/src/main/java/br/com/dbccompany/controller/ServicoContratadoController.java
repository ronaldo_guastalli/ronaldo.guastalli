package br.com.dbccompany.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.entity.Corretor;
import br.com.dbccompany.entity.Segurados;
import br.com.dbccompany.entity.ServicoContratado;
import br.com.dbccompany.service.CorretorService;
import br.com.dbccompany.service.ServicoContratadoService;

@Controller
@RequestMapping("/api/servicoContratado")
public class ServicoContratadoController{
	
	@Autowired
	ServicoContratadoService servicoContratadoService;
	
	@Autowired
	CorretorService corretorService;

	@GetMapping(value="/")
	@ResponseBody
	public List<ServicoContratado> findAll(){
		return servicoContratadoService.findAll();
	}
	
	@PostMapping(path = "/novo")
	@ResponseBody
	public ServicoContratado save(@RequestBody ServicoContratado servicoContratado){		
		return servicoContratadoService.save(servicoContratado);
	}	
	
	@PutMapping(value="/editar/{id}")
	@ResponseBody
	public ServicoContratado update(@PathVariable Integer id, @RequestBody ServicoContratado servicoContratado) {
		servicoContratado.setId(id);
		return servicoContratadoService.save(servicoContratado);
	}
	
	@DeleteMapping(value="/deletar/{id}")
	public void deleteById(@PathVariable Integer id) {
		servicoContratadoService.deleteById(id);

	}


}
