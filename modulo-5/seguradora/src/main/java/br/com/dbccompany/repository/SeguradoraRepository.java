package br.com.dbccompany.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.entity.Endereco;
import br.com.dbccompany.entity.Seguradora;
import br.com.dbccompany.entity.Servicos;


public interface SeguradoraRepository extends CrudRepository<Seguradora, Integer> {


	Seguradora findByNome(String nome);
	Seguradora findByCnpj(Integer cnpj);
	Seguradora findByEnderecoSeguradoras(Endereco endereco);
	
}
