package br.com.dbccompany.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.entity.Cidade;
import br.com.dbccompany.entity.Estado;


public interface EstadoRepository extends CrudRepository<Estado, Integer> {

	Estado findByNome(String nome);
	Estado findByCidade(List<Cidade> cidade);	

}
