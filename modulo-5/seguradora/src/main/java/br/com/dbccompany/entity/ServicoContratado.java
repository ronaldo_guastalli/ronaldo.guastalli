package br.com.dbccompany.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="servico_contratado")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class ServicoContratado {

	@Id
	@SequenceGenerator(allocationSize = 1, name = "servico_contratado_seq", sequenceName = "servico_contratado_seq")
	@GeneratedValue(generator="servico_contratado_seq", strategy = GenerationType.SEQUENCE)
	private Integer id;
	
	@Column(name="descricao", nullable = false, length=200)
	private String descricao;

	@Column(name="valor", nullable=false, precision=10, scale=2)
	private Double valor;

	@ManyToOne
	@JoinColumn(name="id_seguradora", nullable=false)
	private Seguradora seguradora;

	@ManyToOne
	@JoinColumn(name="id_servicos", nullable=false)
	private Servicos servico;

	@ManyToOne
	@JoinColumn(name="id_pessoas_segurados", nullable=false)
	private Segurados pessoasSegurada;

	@ManyToOne
	@JoinColumn(name="id_pessoas_corretor", nullable=false)
	private Corretor pessoasCorretor;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public Seguradora getSeguradora() {
		return seguradora;
	}

	public void setSeguradora(Seguradora seguradora) {
		this.seguradora = seguradora;
	}

	public Servicos getServico() {
		return servico;
	}

	public void setServico(Servicos servico) {
		this.servico = servico;
	}

	public Segurados getPessoasSegurada() {
		return pessoasSegurada;
	}

	public void setPessoasSegurada(Segurados pessoasSegurada) {
		this.pessoasSegurada = pessoasSegurada;
	}

	public Corretor getPessoasCorretor() {
		return pessoasCorretor;
	}

	public void setPessoasCorretor(Corretor pessoasCorretor) {
		this.pessoasCorretor = pessoasCorretor;
	}

	

}
