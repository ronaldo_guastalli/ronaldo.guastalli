package br.com.dbccompany.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.entity.EnderecoPessoas;
import br.com.dbccompany.entity.Pessoas;


public interface PessoasRepository<E extends Pessoas> extends CrudRepository<E, Integer> {

	Pessoas findByNome(String nome);
	Pessoas findByCpf(Integer cpf);
	Pessoas findByPai(String pai);
	Pessoas findByMae(String mae);
	Pessoas findByTelefone(Integer telefone);
	Pessoas findByEmail(String email);
	List<EnderecoPessoas> findAllByCpf(Integer cpf);
	List<EnderecoPessoas> findAllByTelefone(Integer telefone);
	List<EnderecoPessoas> findAllByEmail(String email);

	
}
