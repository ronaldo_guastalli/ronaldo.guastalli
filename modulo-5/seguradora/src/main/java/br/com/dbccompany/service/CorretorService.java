package br.com.dbccompany.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.entity.Corretor;
import br.com.dbccompany.entity.Pessoas;
import br.com.dbccompany.repository.CorretorRepository;
import br.com.dbccompany.service.contratos.CorretorServiceContrato;

@Service
public class CorretorService implements CorretorServiceContrato {
	
	@Autowired
	private CorretorRepository corretorRepository;

	@Override
	public List<Corretor> findAll() {		
		return (List<Corretor>) corretorRepository.findAll();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Corretor save(Corretor t) {
		return corretorRepository.save(t);
	}

	@Override
	public Corretor findById(Integer id) {
		return corretorRepository
				.findById(id)
				.orElse(null);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Corretor update(Integer id, Corretor t) {
		t.setId(id);
		return corretorRepository.save(t);
	}

	@Override
	public void deleteById(Integer id) {
		corretorRepository.deleteById(id);

	}
	
	public Pessoas findByCpf(Integer cpf) {
		return corretorRepository.findByCpf(cpf);
	}
	
	public Pessoas findByEmail(String email) {
		return corretorRepository.findByEmail(email);
	}
	
	public Pessoas findByTelefone(Integer telefone) {
		return corretorRepository.findByTelefone(telefone);
	}

}
