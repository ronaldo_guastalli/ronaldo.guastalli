package br.com.dbccompany.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.entity.Cidade;
import br.com.dbccompany.entity.Estado;


public interface CidadeRepository extends CrudRepository<Cidade, Integer> {

	Cidade findByNome(String nome);
	List<Cidade> findAllByEstado(Estado estado);	

}
