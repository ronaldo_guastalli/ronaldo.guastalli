package br.com.dbccompany.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="bairro")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Bairro.class)
public class Bairro {

	@Id
	@SequenceGenerator(allocationSize = 1, name = "bairro_seq", sequenceName = "bairro_seq")
	@GeneratedValue(generator="bairro_seq", strategy = GenerationType.SEQUENCE)
	private Integer id;

	@Column(name="nome", nullable=false, length=150)
	private String nome;

	@ManyToOne 
	@JoinColumn(name="id_cidade", nullable=false)
	private Cidade cidade;

	@OneToMany(mappedBy="bairro")
	private List<Endereco> endereco = new ArrayList<>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public List<Endereco> getEndereco() {
		return endereco;
	}

	public void setEndereco(List<Endereco> endereco) {
		this.endereco = endereco;
	}

}
