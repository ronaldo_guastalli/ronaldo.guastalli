package br.com.dbccompany.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.entity.Pessoas;
import br.com.dbccompany.entity.Segurados;
import br.com.dbccompany.service.SeguradosService;

@Controller
@RequestMapping("/api/segurados")
public class SeguradosController{
	
	@Autowired
	SeguradosService seguradosService;

	@GetMapping(value="/")
	@ResponseBody
	public List<Segurados> findAll(){
		return seguradosService.findAll();
	}
	
	@PostMapping(path = "/novo")
	@ResponseBody
	public Segurados save(@RequestBody Segurados segurados) {
		return seguradosService.save(segurados);
	}	
	
	@PutMapping(value="/editar/{id}")
	@ResponseBody
	public Segurados update(@PathVariable Integer id, @RequestBody Segurados segurados) {
		segurados.setId(id);
		return seguradosService.save(segurados);
	}
	
	@DeleteMapping(value="/deletar/{id}")
	public void deleteById(@PathVariable Integer id) {
		seguradosService.deleteById(id);

	}
	
	@GetMapping(value="/{telefone}")
	public Pessoas findByTelefone(@PathVariable Integer telefone) {
		return seguradosService.findByTelefone(telefone);
	}
	
	@GetMapping(value="/{cpf}")
	public Pessoas findByCpf(@PathVariable Integer cpf) {
		return seguradosService.findByCpf(cpf);
	}
	
	@GetMapping(value="/{email}")
	public Pessoas findByCpf(@PathVariable String email) {
		return seguradosService.findByEmail(email);
	}


}
