//package br.com.dbccompany.entity;
//
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.JoinColumn;
//import javax.persistence.ManyToOne;
//import javax.persistence.SequenceGenerator;
//import javax.persistence.Table;
//
//import com.fasterxml.jackson.annotation.JsonIdentityInfo;
//import com.fasterxml.jackson.annotation.ObjectIdGenerators;
//
//@Entity
//@Table(name="seguradoras_servicos")
//@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
//public class SeguradorasServicos {
//
//	@Id
//	@SequenceGenerator(allocationSize = 1, name = "seguradoras_servicos_seq", sequenceName = "seguradoras_servicos_seq")
//	@GeneratedValue(generator="seguradoras_servicos_seq", strategy = GenerationType.SEQUENCE)
//	private Integer id;
//
//	@ManyToOne
//    @JoinColumn(name = "id_seguradora", nullable=false)
//	private Seguradora seguradora;
//
//	@ManyToOne
//    @JoinColumn(name = "id_servicos", nullable=false)
//	private Servicos servicos;
//
//	public Integer getId() {
//		return id;
//	}
//
//	public void setId(Integer id) {
//		this.id = id;
//	}
//
//	public Seguradora getSeguradora() {
//		return seguradora;
//	}
//
//	public void setSeguradora(Seguradora seguradora) {
//		this.seguradora = seguradora;
//	}
//
//	public Servicos getServicos() {
//		return servicos;
//	}
//
//	public void setServicos(Servicos servicos) {
//		this.servicos = servicos;
//	}
//
//}
