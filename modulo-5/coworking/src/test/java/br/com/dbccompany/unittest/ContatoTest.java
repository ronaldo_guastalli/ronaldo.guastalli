package br.com.dbccompany.unittest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.sql.Date;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.dbccompany.entity.Cliente;
import br.com.dbccompany.entity.Contato;
import br.com.dbccompany.entity.TipoContato;
import br.com.dbccompany.repository.ContatoRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ContatoTest{

	// usandoh2

	@Autowired
	private TestEntityManager entityManager;
	@Autowired
	private ContatoRepository contatoRepository;
	
	
	

	@Test
	public void acharContatoPorValor() {
		TipoContato tipoContato = new TipoContato();
		tipoContato.setNome("email");
		
		Cliente cliente = new Cliente();		
		cliente.setCpf("1212");
		cliente.setDataNascimento(LocalDateTime.of(2000, 05, 14, 2, 40));
		cliente.setNome("Eita Projeto");	
		
		Contato contato = new Contato();
		contato.setValor("1234");	
		contato.setTipoContato(tipoContato);
		contato.setCliente(cliente);
		
		List<Contato> contatos = new ArrayList<>();
		contatos.add(contato);
		cliente.setContatos(contatos);
		
		entityManager.persist(tipoContato);
		entityManager.persist(cliente);		
		entityManager.persist(contato);
		
		entityManager.flush();
		
		Contato found = contatoRepository.findByValor(contato.getValor());
		String expected = found.getValor();
		String actual = contato.getValor();
		assertEquals(expected, actual);
	}
	
	@Test
	public void acharContatoPorId() {
		Contato contato = new Contato();
		contato.setValor("email");
			
		entityManager.persistAndFlush(contato);
		
		Contato found = contatoRepository.findById(contato.getId()).get();
		Long expected = found.getId();
		Long actual = contato.getId();
		assertEquals(expected, actual);
	}
	
	@Test
	public void retornarTodosOstContatosPassandoTipoDeContato() {
		Contato contato = new Contato();
		contato.setValor("meuemail@gmail");
		TipoContato tipoContato = new TipoContato();
		tipoContato.setNome("email");
		contato.setTipoContato(tipoContato);
		List<Contato> contatos = new ArrayList<Contato>();
		contatos.add(contato);
		Cliente cliente = new Cliente();
		cliente.setContatos(contatos);
		cliente.setCpf("1212");
		cliente.setDataNascimento(LocalDateTime.of(2000, 05, 14, 2, 40));
		cliente.setNome("Eita Projeto");
		contato.setCliente(cliente);
		entityManager.persist(cliente);
		entityManager.persist(tipoContato);
		entityManager.persist(contato);
		entityManager.flush();
		List<Contato> found = contatoRepository.findAllByTipoContato(tipoContato);
		List<Contato> expected = found;
		List<Contato> actual =  contatos;
		assertEquals(expected, actual);
	}
	
	@Test
	public void retornarTodosOstContatosPassandoTipoDeContato2() {
		Contato contato1 = new Contato();
		contato1.setValor("meuemail@gmail");
		TipoContato tipoContato = new TipoContato();
		tipoContato.setNome("email");
		contato1.setTipoContato(tipoContato);
		
		List<Contato> contatos = new ArrayList<Contato>();				
		Cliente cliente1 = new Cliente();
		cliente1.setContatos(contatos);
		cliente1.setCpf("333");
		cliente1.setDataNascimento(LocalDateTime.of(2000, 05, 14, 2, 40));
		cliente1.setNome("Contato 1");	
		
		Cliente cliente2 = new Cliente();
		cliente2.setContatos(contatos);
		cliente2.setCpf("1212");
		cliente2.setDataNascimento(LocalDateTime.of(2000, 05, 14, 2, 40));
		cliente2.setNome("Contato 1");
		
		Contato contato2 = new Contato();
		contato2.setValor("meuemail2@gmail");
		contato2.setTipoContato(tipoContato);
		
		contatos.add(contato1);
		contatos.add(contato2);
		
		contato1.setCliente(cliente1);
		contato2.setCliente(cliente2);
		entityManager.persist(tipoContato);
		entityManager.persist(cliente1);
		entityManager.persist(cliente2);
		entityManager.persist(contato2);
		entityManager.persist(contato1);
		
		entityManager.flush();
		
		List<Contato> found = contatoRepository.findAllByTipoContato(tipoContato);
		assertNotNull(found);
		assertFalse(!(found.size() != 0 && found.size() == 2));
	}
	
	@Test
	public void emailDeveSerUnico() {
		Contato contato1 = new Contato();
		contato1.setValor("meuemail1@gmail");
		TipoContato tipoContato = new TipoContato();
		tipoContato.setNome("email");
		contato1.setTipoContato(tipoContato);
		List<Contato> contatos = new ArrayList<Contato>();
		contatos.add(contato1);
		entityManager.persist(tipoContato);
		entityManager.persist(contato1);
		
		Cliente cliente = new Cliente();
		cliente.setContatos(contatos);
		cliente.setCpf("1212");
		cliente.setDataNascimento(LocalDateTime.of(2000, 05, 14, 2, 40));
		cliente.setNome("Eita Projeto");
		
		Contato contato2 = new Contato();
		contato2.setValor("meuemail@gmail");
		tipoContato.setNome("email");
		contato2.setTipoContato(tipoContato);
		contatos.add(contato2);
		entityManager.persist(tipoContato);		
		entityManager.persist(contato2);
		entityManager.persist(cliente);
		entityManager.flush();
		
		List<Contato> found = contatoRepository.findAllByTipoContato(tipoContato);
		Contato expected1 = found.get(0);
		Contato expected2 = found.get(1);
		assertThat(expected1.getValor()).isNotEqualTo(expected2.getValor());

	}
	
	@Test
	public void todosOsContatosPassandoUmCliente() {
		Cliente clientePaulo = new Cliente();		
		clientePaulo.setCpf("1212");
		clientePaulo.setDataNascimento(LocalDateTime.of(2000, 05, 14, 2, 40));
		clientePaulo.setNome("Eita Projeto");
		
		Contato contato1 = new Contato();
		contato1.setValor("123456");
		TipoContato tipoContato1 = new TipoContato();
		tipoContato1.setNome("celular");
		contato1.setTipoContato(tipoContato1);
		contato1.setCliente(clientePaulo);	
		
		Contato contato2 = new Contato();
		TipoContato tipoContato2 = new TipoContato();
		contato2.setValor("meuemail@gmail");
		tipoContato2.setNome("email");
		contato2.setTipoContato(tipoContato2);		
		contato2.setCliente(clientePaulo);
		
		List<Contato> contatos = new ArrayList<Contato>();
		contatos.add(contato1);		
		contatos.add(contato2);
		
		clientePaulo.setContatos(contatos);
		
		entityManager.persist(tipoContato1);
		entityManager.persist(tipoContato2);
		entityManager.persist(contato1);				
		entityManager.persist(contato2);
		entityManager.persist(clientePaulo);
		entityManager.flush();
		
		List<Contato> found = contatoRepository.findAllByCliente(clientePaulo);
		assertTrue(found.size() == 2);
		for(Contato contato : found) {
			assertTrue(contato == contato1 || contato==contato2);
		}
	}
	

	

}
