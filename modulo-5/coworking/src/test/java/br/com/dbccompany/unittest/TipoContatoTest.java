package br.com.dbccompany.unittest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import br.com.dbccompany.CoworkingApplicationTests;
import br.com.dbccompany.controller.TipoContatoController;
import br.com.dbccompany.entity.Contato;
import br.com.dbccompany.entity.TipoContato;
import br.com.dbccompany.repository.TipoContatoRepository;


public class TipoContatoTest extends CoworkingApplicationTests{
	
	private static final String TIPO_CONTATO_EMAIL = "email";
	private static final String TIPO_CONTATO_TELEFONE = "telefone";
	private static final Long TIPO_CONTATO_ID = Long.parseLong("1");
	
	@MockBean
	private TipoContatoRepository tipoContatoRepository;
	

	private MockMvc mvc;
	
	@Autowired
	private TipoContatoController controller;
	
	@Autowired
	private ApplicationContext context;
	
	TipoContato tipoContato = new TipoContato();
	
	@Before
	public void setUp(){
		Contato contato = new Contato();
		contato.setValor("123456");		
		tipoContato.setNome(TIPO_CONTATO_EMAIL);
		contato.setTipoContato(tipoContato);
		List<Contato> contatos = new ArrayList<>();
		contatos.add(contato);
		Mockito.when(tipoContatoRepository.findByNome(TIPO_CONTATO_TELEFONE)).thenReturn(tipoContato);
		this.mvc = MockMvcBuilders.standaloneSetup(controller).build();

	}
	
	
	@Test
	public void testGetNome() {
		
		TipoContato found = tipoContatoRepository.findByNome(TIPO_CONTATO_EMAIL);
		assertThat(found.getNome()).isEqualTo(TIPO_CONTATO_EMAIL);
	}

	@Test
	public void testSetNome() {
		TipoContato found = tipoContatoRepository.findByNome(TIPO_CONTATO_TELEFONE);
		assertEquals(TIPO_CONTATO_TELEFONE, found.getNome());
	}

	@Test
	public void testGetContatos() {		
		Contato contato = new Contato();
		contato.setValor("123456");
		TipoContato tipoContato = new TipoContato();
		tipoContato.setNome(TIPO_CONTATO_TELEFONE);
		contato.setTipoContato(tipoContato);
		List<Contato> contatos = new ArrayList<Contato>();
		contatos.add(contato);
		Contato expected = contatos.get(0);
		Contato actual = contato;
		assertEquals(expected, actual);;
	}
	
	@Test
	public void statusOk() throws Exception{
		this.mvc.perform(MockMvcRequestBuilders.get("/api/tipocontato/")).andExpect(MockMvcResultMatchers.status().isOk());
	}
	
	

}
