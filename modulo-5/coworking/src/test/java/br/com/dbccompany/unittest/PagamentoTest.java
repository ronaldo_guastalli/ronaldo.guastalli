package br.com.dbccompany.unittest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.LocalDateTime;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import br.com.dbccompany.entity.Cliente;
import br.com.dbccompany.entity.ClientePacote;
import br.com.dbccompany.entity.Contratacao;
import br.com.dbccompany.entity.Espaco;
import br.com.dbccompany.entity.EspacoPacote;
import br.com.dbccompany.entity.Pacote;
import br.com.dbccompany.entity.Pagamento;
import br.com.dbccompany.enuns.TipoContratacao;
import br.com.dbccompany.enuns.TipoPagamento;
import br.com.dbccompany.repository.PagamentoRepository;

@RunWith(MockitoJUnitRunner.class)
public class PagamentoTest {
	
	//PagamentoRepository pagamentoRepository = Mockito.mock(PagamentoRepository.class); //isso é igual @Mockito
	
	@Mock
	private PagamentoRepository pagamentoRepository;	
	
	@Before
	public void setUp() {	
		//Cliente
		Cliente cliente = new Cliente();
		cliente.setCpf("123.786.345-45");
		cliente.setDataNascimento(LocalDateTime.of(2000, 05, 14, 2, 40));
		cliente.setNome("Ronaldo Teste da Silva");
		//Pacote
		Pacote pacote = new Pacote();
		pacote.setValor(100.00);
		pacote.setClientePacotes(Arrays.asList(new ClientePacote[] {new ClientePacote(), new ClientePacote()}));
		pacote.setEspacoPacotes(Arrays.asList(new EspacoPacote[] {new EspacoPacote(), new EspacoPacote()}));
		//ClientePacote
		ClientePacote clientePacote = new ClientePacote();
		clientePacote.setQuantidade(10);
		clientePacote.setCliente(cliente);
		clientePacote.setPacote(pacote);		
		clientePacote.setPagamentos(Arrays.asList(new Pagamento[] {new Pagamento(), new Pagamento()}));
		//Espaco
		Espaco espaco = new Espaco();
		espaco.setNome("Sala para reuniões III");
		espaco.setQtdPessoa(10);
		espaco.setValor(125.89);
		espaco.setContratacoes(Arrays.asList(new Contratacao[] {new Contratacao(), new Contratacao()}));
		espaco.setEspacoPacotes(Arrays.asList(new EspacoPacote[] {new EspacoPacote(), new EspacoPacote()}));
		//Contratacao
		Contratacao contratacao = new Contratacao();
		contratacao.setCliente(cliente);
		contratacao.setDesconto(0.10);
		contratacao.setEspaco(espaco);
		contratacao.setPrazo(30);
		contratacao.setQuantidade(100);
		contratacao.setTipoContratacao(TipoContratacao.HORA);
		contratacao.setPagamentos(Arrays.asList(new Pagamento[] {new Pagamento(), new Pagamento(), new Pagamento()}));
		//Pagamento
		Pagamento pagamento = new Pagamento();
		pagamento.setTipoPagamento(TipoPagamento.CREDITO);
		pagamento.setClientePacote(clientePacote);
		pagamento.setContratacao(contratacao);
		pagamento.setId(1l);
		
		Mockito.when(pagamentoRepository.findByTipoPagamento(TipoPagamento.CREDITO)).thenReturn(pagamento);
		Mockito.when(pagamentoRepository.findByClientePacote(clientePacote)).thenReturn(pagamento);
		Mockito.when(pagamentoRepository.findAll()).thenReturn(contratacao.getPagamentos(), clientePacote.getPagamentos());
		Mockito.when(pagamentoRepository.save(pagamento));
	}
	
	@Test
	public void buscaPorTipoDePagamento() {
		Pagamento found = pagamentoRepository.findByTipoPagamento(TipoPagamento.CREDITO);
		Long idPagamento = 1l;
		Long expected = idPagamento;
		Long actual = found.getId();
		assertThat(actual).isEqualTo(expected);
	}
	
	@Test
	public void seTipoDePagamentoForNull() {
		Pagamento p = new Pagamento();
		p.setTipoPagamento(null);
		p.getTipoPagamento();
		Pagamento found = pagamentoRepository.save(p);
		String msg = "O tipo de pagamento deve ser informado.";
//		Long expected = msg;
//		Long actual = found;
//		assertTrue(condition);
		
	}

}
