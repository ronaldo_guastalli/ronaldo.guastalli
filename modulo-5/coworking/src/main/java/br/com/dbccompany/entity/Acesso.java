package br.com.dbccompany.entity;

import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "acesso")
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Acesso.class)
public class Acesso {

	@Id
	@SequenceGenerator(allocationSize = 1, name = "acesso_seq", sequenceName = "acesso_seq")
	@GeneratedValue(generator = "acesso_seq", strategy = GenerationType.SEQUENCE)
	private Long id;

	@Column(name="is_entrada", nullable=false)
	private Boolean isEntrada;

	@Column(name="data")
	private LocalDateTime data;

	@Column(name="is_excecao")
	private Boolean isExcecao;

	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.MERGE)
	@JoinColumns({
		@JoinColumn(name="id_cliente",
				referencedColumnName="id_cliente"),
		@JoinColumn(name="id_espaco",
		referencedColumnName="id_espaco")
	})
	private SaldoCliente saldoCliente;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getIsEntrada() {
		return isEntrada;
	}

	public void setIsEntrada(Boolean isEntrada) {
		this.isEntrada = isEntrada;
	}	

	public LocalDateTime getData() {
		return data;
	}

	public void setData(LocalDateTime data) {
		this.data = data;
	}

	public Boolean getIsExcecao() {
		return isExcecao;
	}

	public void setIsExcecao(Boolean isExcecao) {
		this.isExcecao = isExcecao;
	}

	public SaldoCliente getSaldoCliente() {
		return saldoCliente;
	}

	public void setSaldoCliente(SaldoCliente saldoCliente) {
		this.saldoCliente = saldoCliente;
	}
	
	

}
