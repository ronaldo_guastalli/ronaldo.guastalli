package br.com.dbccompany.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.entity.Contratacao;
import br.com.dbccompany.entity.Espaco;
import br.com.dbccompany.entity.EspacoPacote;

public interface EspacoRepository extends CrudRepository<Espaco, Long> {

	public Espaco findByNome(String nome);
	public Espaco findByQtdPessoa(Integer qtdPessoa);
	public List<Espaco> findByContratacoes(List<Contratacao> contratacoes);	

}