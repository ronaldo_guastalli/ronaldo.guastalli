/**
 * 
 */
package br.com.dbccompany.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.entity.SaldoCliente;
import br.com.dbccompany.entity.SaldoClienteId;

/**
 * @author ronaldoguastalli
 *
 */
public interface IServicePadrao<T> {

	List<T> findAll();

	@Transactional(rollbackFor = Exception.class)
	T save(T t) throws Exception;

	T findById(Long id);

	@Transactional(rollbackFor = Exception.class)
	T update(Long id, T t);

	void deleteById(Long id);


}
