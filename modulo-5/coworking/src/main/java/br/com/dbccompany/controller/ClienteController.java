package br.com.dbccompany.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.entity.Cliente;
import br.com.dbccompany.service.ClienteService;

@Controller
@RequestMapping("/api/cliente")
public class ClienteController {

	@Autowired
	ClienteService clienteService;

	@GetMapping(value = "/")
	@ResponseBody
	public List<Cliente> findAll() {
		return clienteService.findAll();
	}

	@PostMapping(path = "/novo")
	@ResponseBody
	public Cliente save(@RequestBody Cliente cliente) throws Exception {
		return clienteService.save(cliente);
	}

	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Cliente update(@PathVariable Long id, @RequestBody Cliente cliente) throws Exception {
		cliente.setId(id);
		return clienteService.save(cliente);
	}

	@DeleteMapping(value = "/deletar/{id}")
	public ResponseEntity<Cliente> deleteById(@PathVariable Long id) {
		clienteService.deleteById(id);
		return ResponseEntity.noContent().build();
	}
	
	@GetMapping(value = "/cliente/{nome}")
	@ResponseBody
	public Cliente buscarClientePorNome(@PathVariable String nome) {
		return clienteService.findByNome(nome);
	}
	
	@GetMapping(value = "/cliente/cpf/{cpf}")
	@ResponseBody
	public Cliente buscarClientePorCpf(@PathVariable String cpf) {
		return clienteService.findByCpf(cpf);
	}
	
	@GetMapping(value = "/cliente/datanascimento/{dataNascimento}")
	@ResponseBody
	public Cliente buscarClientePorDataNascimento(@PathVariable String dataNascimento) {
		return clienteService.findByCpf(dataNascimento);
	}

}
