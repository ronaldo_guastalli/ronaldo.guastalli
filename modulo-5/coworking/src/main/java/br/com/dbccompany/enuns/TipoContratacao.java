package br.com.dbccompany.enuns;

public enum TipoContratacao {

	MINUTO, HORA, TURNO, DIARIA, SEMANA, MES;

}
