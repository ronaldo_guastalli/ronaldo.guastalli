package br.com.dbccompany.enuns;

public enum TipoPagamento {

	DEBITO, CREDITO, DINHEIRO, TRANSFERENCIA;

}
