package br.com.dbccompany.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.entity.ClientePacote;
import br.com.dbccompany.entity.EspacoPacote;
import br.com.dbccompany.entity.Pacote;
import br.com.dbccompany.enuns.TipoPagamento;
import br.com.dbccompany.entity.Pacote;

public interface PacoteRepository extends CrudRepository<Pacote, Long> {

	public Pacote findByValor(Double valor);
	public List<Pacote> findAllByClientePacotes(List<ClientePacote> clientePacotes);
	public List<Pacote> findAllByEspacoPacotes(List<EspacoPacote> espacoPacotes);

}