package br.com.dbccompany.jsp;

import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import br.com.dbccompany.security.UserDetailsImplementacao;

public class AppConfig extends WebSecurityConfigurerAdapter {

	  @Override
	  protected void configure(HttpSecurity http) throws Exception {
	      http.authorizeRequests()
	          .antMatchers("/login").hasRole("ADMIN")
	          .anyRequest()
	          .authenticated()
	          .and()
	          .formLogin();
	  }

	  @Override
	  public void configure(AuthenticationManagerBuilder builder)
	          throws Exception {
	      builder.userDetailsService(new UserDetailsImplementacao());
	  }

	  @Bean
	  public ViewResolver viewResolver() {
	      InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
	      viewResolver.setPrefix("br/com/dbccompany/jsp/");
	      viewResolver.setSuffix(".jsp");
	      return viewResolver;
	  }
	}