package br.com.dbccompany.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.entity.Cliente;
import br.com.dbccompany.entity.Contratacao;
import br.com.dbccompany.enuns.TipoContratacao;
import br.com.dbccompany.repository.ClienteRepository;
import br.com.dbccompany.repository.ContratacaoRepository;
import br.com.dbccompany.repository.EspacoPacoteRepository;
import br.com.dbccompany.repository.EspacoRepository;
import br.com.dbccompany.service.util.UtilService;

@Service
public class ContratacaoService implements IServicePadrao<Contratacao> {

	@Autowired
	private ContratacaoRepository contratacaoRepository;

	@Autowired
	private EspacoRepository espacoRepository;
	
	@Autowired
	private ClienteRepository clienteRepository;

	@Autowired
	private EspacoPacoteRepository espacoPacoteRepository;

	@Override
	public List<Contratacao> findAll() {
		return (List<Contratacao>) contratacaoRepository.findAll();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Contratacao save(Contratacao contratacao) {
		return contratacaoRepository.save(contratacao);
	}

	@Transactional(rollbackFor = Exception.class)
	public Double saveOrcamento(Contratacao contratacao) throws Exception {
		if(contratacao.getEspaco().getId() == null) {
			contratacao.setEspaco(contratacao.getEspaco());
			espacoRepository.save(contratacao.getEspaco());
		}
		if(contratacao.getCliente().getId() == null) {
			contratacao.setCliente(contratacao.getCliente());
			clienteRepository.save(contratacao.getCliente());
		}
		if (contratacao.getQuantidade() != null && contratacao.getTipoContratacao() != null) {
			Double valorEspaco = espacoRepository.findById(contratacao.getEspaco().getId()).get().getValor();
			Integer qtd = contratacao.getQuantidade();
			TipoContratacao tipoContratacao = contratacao.getTipoContratacao();
			Double valorDesconto = contratacao.getDesconto();
			contratacaoRepository.save(contratacao);
			return UtilService.calculaValorDoOrcamento(valorEspaco, qtd, tipoContratacao, valorDesconto);
		}
		throw new Exception("não infomado quantidade e tipo de contratação.");
	}

	@Override
	public Contratacao findById(Long id) {
		return contratacaoRepository.findById(id).orElse(null);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Contratacao update(Long id, Contratacao contratacao) {
		contratacao.setId(id);
		return contratacaoRepository.save(contratacao);
	}

	@Override
	public void deleteById(Long id) {
		contratacaoRepository.deleteById(id);
	}

	public Contratacao findByTipoContratacao(TipoContratacao tipoContratacao) {
		return contratacaoRepository.findByTipoContratacao(tipoContratacao);
	}

	public Contratacao findByQuantidade(Integer quantidade) {
		return contratacaoRepository.findByQuantidade(quantidade);
	}

	public Contratacao findByDesconto(Double desconto) {
		return contratacaoRepository.findByDesconto(desconto);
	}

	public Contratacao findByPrazo(Integer prazo) {
		return contratacaoRepository.findByPrazo(prazo);
	}

	public List<Contratacao> findAllByClientes(Cliente cliente) {
		return contratacaoRepository.findByCliente(cliente);
	}

}
