package br.com.dbccompany.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.entity.ClientePacote;
import br.com.dbccompany.entity.Pacote;

public interface ClientePacoteRepository extends CrudRepository<ClientePacote, Long> {

	public ClientePacote findByQuantidade(Integer quantidade);
	public ClientePacote findByPacote(Pacote pacote);
	public ClientePacote findByCliente( ClientePacote cliente);	

}