package br.com.dbccompany.repository;

import java.sql.Date;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.dbccompany.entity.SaldoCliente;
import br.com.dbccompany.entity.SaldoClienteId;
import br.com.dbccompany.enuns.TipoContratacao;

@Repository
public interface SaldoClienteRepository extends CrudRepository<SaldoCliente, SaldoClienteId> {

	public SaldoCliente findByTipoContratacao(TipoContratacao tipoContratacao);
	public SaldoCliente findByQuantidade(Integer quantidade);
	public SaldoCliente findByVencimento(LocalDateTime vencimento);
	public List<SaldoCliente> findAllByAcessos(List<SaldoCliente> acessos);
	//public SaldoCliente findBySaldoClienteId(SaldoClienteId saldoClienteId);
	

}