package br.com.dbccompany.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.entity.Contratacao;
import br.com.dbccompany.service.ContratacaoService;

@Controller
@RequestMapping("/api/contratacao")
public class ContratacaoController {

	@Autowired
	ContratacaoService contratacaService;

	@GetMapping(value = "/")
	@ResponseBody
	public List<Contratacao> findAll() {
		return contratacaService.findAll();
	}

	@PostMapping(path = "/novo")
	@ResponseBody
	public Double save(@RequestBody Contratacao contratacao) throws Exception {
		return contratacaService.saveOrcamento(contratacao);
	}

	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Contratacao update(@PathVariable Long id, @RequestBody Contratacao contratacao) {
		contratacao.setId(id);
		return contratacaService.save(contratacao);
	}

	@DeleteMapping(value = "/deletar/{id}")
	public ResponseEntity<Contratacao> deleteById(@PathVariable Long id) {
		contratacaService.deleteById(id);
		return ResponseEntity.noContent().build();

	}
	

}
