package br.com.dbccompany.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class SaldoClienteId implements Serializable {

	private static final long serialVersionUID = 1L;


	@Column(name = "id_cliente")
	private Long idCliente;


	@Column(name = "id_espaco")
	private Long idEspaco;


	public SaldoClienteId(Long idCliente, Long idEspaco) {
		this.idCliente = idCliente;
		this.idEspaco = idEspaco;
	}

	public SaldoClienteId() {
	}

	public Long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	public Long getIdEspaco() {
		return idEspaco;
	}

	public void setIdEspaco(Long idEspaco) {
		this.idEspaco = idEspaco;
	}

}
