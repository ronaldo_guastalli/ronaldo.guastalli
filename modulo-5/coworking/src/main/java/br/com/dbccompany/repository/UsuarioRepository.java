package br.com.dbccompany.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.entity.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Long> {

	public Usuario findByNome(String nome);
	public Usuario findByEmail(String email);
	public Usuario findByLogin(String login);
	public Usuario findBySenha(String senha);
	

}