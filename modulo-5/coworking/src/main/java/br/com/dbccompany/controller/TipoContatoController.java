package br.com.dbccompany.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.entity.Contato;
import br.com.dbccompany.entity.TipoContato;
import br.com.dbccompany.service.TipoContatoService;

@Controller
@RequestMapping("/api/tipocontato")
public class TipoContatoController {

	@Autowired
	TipoContatoService tipoContatoService;

	@GetMapping(value = "/")
	@ResponseBody
	public List<TipoContato> findAll() {
		return tipoContatoService.findAll();
	}

	@PostMapping(path = "/novo", produces = "application/json")
	@ResponseBody
	public TipoContato save(@RequestBody TipoContato tipoContato) {
		return tipoContatoService.save(tipoContato);
	}

	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public TipoContato update(@PathVariable Long id, @RequestBody TipoContato tipoContato) {
		tipoContato.setId(id);
		return tipoContatoService.save(tipoContato);
	}

	@DeleteMapping(value = "/deletar/{id}")
	public ResponseEntity<TipoContato> deleteById(@PathVariable Long id) {
		tipoContatoService.deleteById(id);
		return ResponseEntity.noContent().build();

	}

	@RequestMapping(value = "/buscar", params = { "nome" }, method = RequestMethod.GET)
	@ResponseBody
	public TipoContato buscarTipoDeContatoPorNome(@RequestParam("nome") String nome) {
		return tipoContatoService.findByNome(nome);

	}
	
	@GetMapping(value = "/contatos")
	@ResponseBody
	public List<TipoContato> findAllContatos(@RequestBody List<Contato> contatos) {
		return tipoContatoService.findAllContatos(contatos);
	}

}
