package br.com.dbccompany.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.entity.Contato;
import br.com.dbccompany.entity.TipoContato;
import br.com.dbccompany.repository.TipoContatoRepository;

@Service
public class TipoContatoService implements IServicePadrao<TipoContato> {

	@Autowired
	private TipoContatoRepository tipoContatoRepository;

	@Override
	public List<TipoContato> findAll() {
		return (List<TipoContato>) tipoContatoRepository.findAll();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public TipoContato save(TipoContato t) {
		return (TipoContato) tipoContatoRepository.save(t);
	}

	@Override
	public TipoContato findById(Long id) {
		return tipoContatoRepository.findById(id).orElse(null);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public TipoContato update(Long id, TipoContato t) {
		t.setId(id);
		return tipoContatoRepository.save(t);
	}

	@Override
	public void deleteById(Long id) {
		tipoContatoRepository.deleteById(id);

	}
	
	public TipoContato findByNome(String nome) {
		if(tipoContatoRepository.findByNome(nome) == null)
			return null;
		return tipoContatoRepository.findByNome(nome);
	}
	
	public List<TipoContato> findAllContatos(List<Contato> contatos){
		return tipoContatoRepository.findAllByContatos(contatos);
	}

}
