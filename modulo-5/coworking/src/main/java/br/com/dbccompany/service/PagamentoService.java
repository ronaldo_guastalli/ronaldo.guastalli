package br.com.dbccompany.service;

import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.entity.Cliente;
import br.com.dbccompany.entity.ClientePacote;
import br.com.dbccompany.entity.Contratacao;
import br.com.dbccompany.entity.Espaco;
import br.com.dbccompany.entity.EspacoPacote;
import br.com.dbccompany.entity.Pacote;
import br.com.dbccompany.entity.Pagamento;
import br.com.dbccompany.entity.SaldoCliente;
import br.com.dbccompany.entity.SaldoClienteId;
import br.com.dbccompany.enuns.TipoPagamento;
import br.com.dbccompany.repository.ClientePacoteRepository;
import br.com.dbccompany.repository.ClienteRepository;
import br.com.dbccompany.repository.ContratacaoRepository;
import br.com.dbccompany.repository.EspacoRepository;
import br.com.dbccompany.repository.PacoteRepository;
import br.com.dbccompany.repository.PagamentoRepository;
import br.com.dbccompany.repository.SaldoClienteRepository;

@Service
public class PagamentoService implements IServicePadrao<Pagamento> {

	@Autowired
	private PagamentoRepository pagamentoRepository;

	@Autowired
	private PacoteRepository pacoteRepository;

	@Autowired
	private ClienteRepository clienteRepository;

	@Autowired
	private ClientePacoteRepository clientePacoteRepository;

	@Autowired
	private SaldoClienteRepository saldoClienteRepository;

	@Autowired
	private ContratacaoRepository contratacaoRepository;

	@Autowired
	private EspacoRepository espacoRepository;

	@Override
	public List<Pagamento> findAll() {
		return (List<Pagamento>) pagamentoRepository.findAll();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Pagamento save(Pagamento pagamento) throws Exception {
		if (pagamento.getId() == null) {
			if(pagamento.getTipoPagamento() != null)
				pagamento.setTipoPagamento(pagamento.getTipoPagamento());
			else throw new Exception("O tipo de pagamento deve ser informado.");
			if (pagamento.getClientePacote() != null) {
				criarOuAlterarUmClientePacote(pagamento);
				saldoClientePacote(pagamento);
			}
			if (pagamento.getContratacao() != null) {
				criarOuAlterarUmaContratacao(pagamento);
				saldoClienteContratacao(pagamento);
			}
		} else {
			pagamento.setId(pagamento.getId());
			return pagamentoRepository.save(pagamento);
		}
		return pagamentoRepository.save(pagamento);
	}

	@Override
	public Pagamento findById(Long id) {
		return pagamentoRepository.findById(id).orElse(null);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Pagamento update(Long id, Pagamento pagamento) {
		pagamento.setId(id);
		return pagamentoRepository.save(pagamento);
	}

	@Override
	public void deleteById(Long id) {
		pagamentoRepository.deleteById(id);
	}

	public Pagamento findByTipoPagamento(TipoPagamento tipoPagamento) {
		return pagamentoRepository.findByTipoPagamento(tipoPagamento);
	}

	public Pagamento findByClientePacote(ClientePacote clientePacote) {
		return pagamentoRepository.findByClientePacote(clientePacote);
	}

	public void criarOuAlterarUmaContratacao(Pagamento pagamento) {
		if (pagamento.getContratacao() != null) {
			if (pagamento.getContratacao().getId() == null && pagamento.getContratacao().getTipoContratacao() != null
					&& pagamento.getContratacao().getQuantidade() != null) {
				// para cliente
				if (pagamento.getContratacao().getCliente().getId() == null) {
					pagamento.getContratacao().getCliente().setCpf(pagamento.getContratacao().getCliente().getCpf());
					pagamento.getContratacao().getCliente()
							.setDataNascimento(pagamento.getContratacao().getCliente().getDataNascimento());
					pagamento.getContratacao().getCliente().setNome(pagamento.getContratacao().getCliente().getNome());
					clienteRepository.save(pagamento.getContratacao().getCliente());
				} else {
					Cliente cliente = clienteRepository.findById(pagamento.getContratacao().getCliente().getId()).get();
					pagamento.getContratacao().setCliente(cliente);
				}
				// para espaco
				if (pagamento.getContratacao().getEspaco().getId() == null) {
					pagamento.getContratacao().getEspaco().setNome(pagamento.getContratacao().getEspaco().getNome());
					pagamento.getContratacao().getEspaco()
							.setQtdPessoa(pagamento.getContratacao().getEspaco().getQtdPessoa());
					pagamento.getContratacao().getEspaco().setValor(pagamento.getContratacao().getEspaco().getValor());
					espacoRepository.save(pagamento.getContratacao().getEspaco());
				} else {
					Espaco espaco = espacoRepository.findById(pagamento.getContratacao().getEspaco().getId()).get();
					pagamento.getContratacao().setEspaco(espaco);
				}
				// para contratacao
				if (pagamento.getContratacao().getId() == null) {
					pagamento.getContratacao().setTipoContratacao(pagamento.getContratacao().getTipoContratacao());
					pagamento.getContratacao().setQuantidade(pagamento.getContratacao().getQuantidade());
					pagamento.getContratacao().setDesconto(pagamento.getContratacao().getDesconto());
					pagamento.getContratacao().setPrazo(pagamento.getContratacao().getPrazo());
					contratacaoRepository.save(pagamento.getContratacao());
				} else {
					Contratacao contratacao = contratacaoRepository.findById(pagamento.getContratacao().getId()).get();
					pagamento.setContratacao(contratacao);
				}
			}
		} else {
			pagamento.setContratacao(null);
		}
		pagamentoRepository.save(pagamento);
		// return pagamento;
	}

	public void criarOuAlterarUmClientePacote(Pagamento pagamento) {
		if (pagamento.getClientePacote() != null) {
			if (pagamento.getClientePacote().getId() == null && pagamento.getClientePacote().getQuantidade() != null
					&& pagamento.getClientePacote().getPacote() != null
					&& pagamento.getClientePacote().getCliente() != null) {
				// criar ou utilizar pacote
				if (pagamento.getClientePacote().getPacote().getId() == null) {
					pagamento.getClientePacote().getPacote()
							.setValor(pagamento.getClientePacote().getPacote().getValor());
					pagamento.getClientePacote().getPacote()
							.setClientePacotes(pagamento.getClientePacote().getPacote().getClientePacotes());
					pagamento.getClientePacote().getPacote()
							.setEspacoPacotes(pagamento.getClientePacote().getPacote().getEspacoPacotes());
					pacoteRepository.save(pagamento.getClientePacote().getPacote());
				} else {
					Pacote pacote = pacoteRepository.findById(pagamento.getClientePacote().getPacote().getId()).get();
					pagamento.getClientePacote().setPacote(pacote);
				}
				// criar ou utilizar cliente
				if (pagamento.getClientePacote().getCliente().getId() == null) {
					pagamento.getClientePacote().getCliente()
							.setCpf(pagamento.getClientePacote().getCliente().getCpf());
					pagamento.getClientePacote().getCliente()
							.setDataNascimento(pagamento.getClientePacote().getCliente().getDataNascimento());
					pagamento.getClientePacote().getCliente()
							.setNome(pagamento.getClientePacote().getCliente().getNome());
					clienteRepository.save(pagamento.getClientePacote().getCliente());
				} else {
					Cliente cliente = clienteRepository.findById(pagamento.getClientePacote().getCliente().getId())
							.get();
					pagamento.getClientePacote().setCliente(cliente);
				}
				// criar ou utilizar cliente pacote
				if (pagamento.getClientePacote().getId() == null) {
					pagamento.getClientePacote().setQuantidade(pagamento.getClientePacote().getQuantidade());
					clientePacoteRepository.save(pagamento.getClientePacote());
				} else {
					ClientePacote clientePacote = clientePacoteRepository.findById(pagamento.getId()).get();
					clientePacoteRepository.save(clientePacote);
				}
			} else if (pagamento.getClientePacote().getId() != null) {
				ClientePacote clientePacote = clientePacoteRepository.findById(pagamento.getClientePacote().getId())
						.get();
				pagamento.getClientePacote().setId(pagamento.getClientePacote().getId());
				clientePacoteRepository.save(clientePacote);
			} else {
				pagamento.setClientePacote(null);
			}
		}
		pagamentoRepository.save(pagamento);
//		return pagamento;

	}

	public void saldoClienteContratacao(Pagamento pagamento) {
		SaldoClienteId saldoClienteId = new SaldoClienteId(pagamento.getContratacao().getCliente().getId(),
				pagamento.getContratacao().getEspaco().getId());
		Boolean saldoClienteNaoExiste = !saldoClienteRepository.existsById(saldoClienteId);
		if (saldoClienteNaoExiste) {
			SaldoCliente novoSaldoCliente = new SaldoCliente();
			novoSaldoCliente.setSaldoClienteId(saldoClienteId);
			novoSaldoCliente.setQuantidade(pagamento.getContratacao().getQuantidade());
			novoSaldoCliente.setTipoContratacao(pagamento.getContratacao().getTipoContratacao());
			LocalDateTime date = LocalDateTime.now();
			novoSaldoCliente.setVencimento(date);
			saldoClienteRepository.save(novoSaldoCliente);
		} else {
			// atualizar
			SaldoCliente atualizarSaldoCliente = saldoClienteRepository.findById(saldoClienteId).get();
			atualizarSaldoCliente.setSaldoClienteId(saldoClienteId);
			atualizarSaldoCliente.setQuantidade(pagamento.getContratacao().getQuantidade());
			saldoClienteRepository.save(atualizarSaldoCliente);
		}
	}
	
	@Transactional(rollbackFor=Exception.class)
	public void saldoClientePacote(Pagamento pagamento) {
		// cliente pacote
		ClientePacote clientePacote = clientePacoteRepository.findById(pagamento.getClientePacote().getId()).get();
		Cliente cliente = clienteRepository.findById(clientePacote.getCliente().getId()).get();
		Pacote pacote = pacoteRepository.findById(clientePacote.getPacote().getId()).get();
		Integer quantidade = clientePacote.getQuantidade();
		Long idCliente = cliente.getId();
		List<EspacoPacote> espacoPacotes = pacote.getEspacoPacotes();
		Long idEspaco;

		for (EspacoPacote espacoPacote : espacoPacotes) {
			idEspaco = espacoPacote.getEspaco().getId();
			SaldoCliente saldoCliente = verificarOuGerarSaldoCliente(idCliente, idEspaco);
			saldoCliente.setTipoContratacao(espacoPacote.getTipoContratacao());
			saldoCliente.setQuantidade(espacoPacote.getQuantidade());
			saldoCliente.setVencimento(prazoVencimento(quantidade,espacoPacote.getPrazo() ,saldoCliente));
			saldoClienteRepository.save(saldoCliente);
		}
	}

	public SaldoCliente verificarOuGerarSaldoCliente(Long idCliente, Long idEspaco) {
		return saldoClienteRepository.findById(new SaldoClienteId(idCliente, idEspaco))
				.orElse(new SaldoCliente(new SaldoClienteId(idCliente, idEspaco)));
	}
	
	public LocalDateTime prazoVencimento(Integer quantidade, Integer prazoVencimento, SaldoCliente saldoCliente) {
		if(saldoCliente.getVencimento() == null) {
			saldoCliente.setVencimento(LocalDateTime.now().plusDays(prazoVencimento * quantidade));
		}else {
			saldoCliente.setVencimento(saldoCliente.getVencimento().plusDays(prazoVencimento * quantidade));
		}		
		return saldoCliente.getVencimento();
	}

}
