package br.com.dbccompany.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "cliente_pacote")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = ClientePacote.class)
public class ClientePacote {

	@Id
	@SequenceGenerator(allocationSize = 1, name = "cliente_pacote_seq", sequenceName = "cliente_pacote_seq")
	@GeneratedValue(generator = "cliente_pacote_seq", strategy = GenerationType.SEQUENCE)
	private Long id;

	@Column(name="quantidade", nullable=false)
	private Integer quantidade;

	@ManyToOne
	@JoinColumn(name="id_pacote")
	private Pacote pacote;

	@ManyToOne
	@JoinColumn(name="id_cliente")
	private Cliente cliente;

	@OneToMany(mappedBy="clientePacote", fetch=FetchType.LAZY, cascade= {CascadeType.ALL})
	private List<Pagamento> pagamentos = new ArrayList<Pagamento>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public Pacote getPacote() {
		return pacote;
	}

	public void setPacote(Pacote pacote) {
		this.pacote = pacote;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public List<Pagamento> getPagamentos() {
		return pagamentos;
	}

	public void setPagamentos(List<Pagamento> pagamentos) {
		this.pagamentos = pagamentos;
	}
	
	

}
