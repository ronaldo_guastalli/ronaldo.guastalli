package br.com.dbccompany.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.entity.ClientePacote;
import br.com.dbccompany.entity.Pagamento;
import br.com.dbccompany.enuns.TipoPagamento;

public interface PagamentoRepository extends CrudRepository<Pagamento, Long> {

	public Pagamento findByTipoPagamento(TipoPagamento tipoPagamento);
	public Pagamento findByClientePacote(ClientePacote clientePacote);
	

}