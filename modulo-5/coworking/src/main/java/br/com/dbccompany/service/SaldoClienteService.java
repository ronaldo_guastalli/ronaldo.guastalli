package br.com.dbccompany.service;

import java.sql.Date;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.entity.SaldoCliente;
import br.com.dbccompany.entity.SaldoClienteId;
import br.com.dbccompany.enuns.TipoContratacao;
import br.com.dbccompany.repository.SaldoClienteRepository;

@Service
public class SaldoClienteService {

	@Autowired
	private SaldoClienteRepository saldoClienteRepository;


	public List<SaldoCliente> findAll() {
		return (List<SaldoCliente>) saldoClienteRepository.findAll();
	}
	
	public List<SaldoCliente> findAllBySaldoCliente(Long idCliente, Long idEspaco, SaldoClienteId saldoCliente) {
		saldoCliente.setIdCliente(idCliente);
		saldoCliente.setIdEspaco(idEspaco);
		return (List<SaldoCliente>) saldoClienteRepository.findAll();
		//return (List<SaldoCliente>) saldoClienteRepository.findAllById(null);
	}

	
	@Transactional(rollbackFor = Exception.class)
	public SaldoCliente save(SaldoCliente saldoCliente) {
		return (SaldoCliente) saldoClienteRepository.save(saldoCliente);
	}

	
	public SaldoCliente findById(SaldoClienteId id) {
		return saldoClienteRepository.findById(id).orElse(null);
	}

	@Transactional(rollbackFor = Exception.class)
	public SaldoCliente updateIds(Long idCliente, Long idEspaco, SaldoClienteId saldoCliente) {
		saldoCliente.setIdCliente(idCliente);
		saldoCliente.setIdEspaco(idEspaco);
		return (SaldoCliente) saldoClienteRepository.saveAll((Iterable<SaldoCliente>) saldoCliente);
	}

	
	public void deleteById(SaldoClienteId id) {
		saldoClienteRepository.deleteById(id);
	}
	
	public SaldoCliente findByTipoContratacao(TipoContratacao tipoContratacao){
		return saldoClienteRepository.findByTipoContratacao(tipoContratacao);
	}
	public SaldoCliente findByQuantidade(Integer quantidade){
		return saldoClienteRepository.findByQuantidade(quantidade);
	}
	public SaldoCliente findByVencimento(LocalDateTime vencimento){
		return saldoClienteRepository.findByVencimento(vencimento);
	}
	public List<SaldoCliente> findByVencimento(List<SaldoCliente> acessos){
		return saldoClienteRepository.findAllByAcessos(acessos);
	}


	public SaldoCliente update(Long id, SaldoCliente t) {
		return null;
	}
	
	



}
