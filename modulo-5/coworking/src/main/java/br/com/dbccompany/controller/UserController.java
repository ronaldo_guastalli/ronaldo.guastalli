package br.com.dbccompany.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.tomcat.util.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.entity.MD5Cryp;
import br.com.dbccompany.entity.Users;
import br.com.dbccompany.security.TokenAuthenticationService;
import br.com.dbccompany.service.UserService;

@Controller
@RequestMapping("/api/user")
public class UserController {

	@Autowired
	UserService userService;

	@GetMapping(value = "/")
	@ResponseBody
	public List<Users> findAll() {
		return userService.findAll();
	}

	@RequestMapping("/login")
	public String handleRequest(HttpServletRequest request, Model model) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		model.addAttribute("uri", request.getRequestURI()).addAttribute("user", auth.getName()).addAttribute("roles",
				auth.getAuthorities());
		return "login";
	}


	@PostMapping(path = "/novo")
	@ResponseBody
	public ResponseEntity<?> save(@RequestBody Users user) {
		try {
			// BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(16);
			MD5Cryp encoder = new MD5Cryp();
			user.setSenha(encoder.encode(user.getSenha()));
			userService.save(user);

			String token = TokenAuthenticationService.getToken(user.getSenha());
			return ResponseEntity.ok(token);
		} catch (Throwable t) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(ExceptionUtils.unwrapInvocationTargetException(t));
		}

//		return userService.save(user);
	}

	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Users update(@PathVariable Long id, @RequestBody Users user) {
		user.setId(id);
		return userService.save(user);
	}

	@DeleteMapping(value = "/deletar/{id}")
	public ResponseEntity<Users> deleteById(@PathVariable Long id) {
		userService.deleteById(id);
		return ResponseEntity.noContent().build();

	}

	@RequestMapping(value = "/buscar", params = { "nome" }, method = RequestMethod.GET)
	@ResponseBody
	public Users buscarTipoDeContatoPorNome(@RequestParam("nome") String nome) {
		return userService.findByNome(nome);

	}

}
