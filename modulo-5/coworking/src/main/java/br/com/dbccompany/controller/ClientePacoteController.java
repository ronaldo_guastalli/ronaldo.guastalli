package br.com.dbccompany.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.entity.ClientePacote;
import br.com.dbccompany.service.ClientePacoteService;

@Controller
@RequestMapping("/api/clientepacote")
public class ClientePacoteController {

	@Autowired
	ClientePacoteService clientePacoteService;

	@GetMapping(value = "/")
	@ResponseBody
	public List<ClientePacote> findAll() {
		return clientePacoteService.findAll();
	}

	@PostMapping(path = "/novo")
	@ResponseBody
	public ClientePacote save(@RequestBody ClientePacote clientePacote) {
		return clientePacoteService.save(clientePacote);
	}

	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public ClientePacote update(@PathVariable Long id, @RequestBody ClientePacote clientePacote) {
		clientePacote.setId(id);
		return clientePacoteService.save(clientePacote);
	}

	@DeleteMapping(value = "/deletar/{id}")
	public ResponseEntity<ClientePacote> deleteById(@PathVariable Long id) {
		clientePacoteService.deleteById(id);
		return ResponseEntity.noContent().build();
	}


}
