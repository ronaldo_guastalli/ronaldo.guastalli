package br.com.dbccompany.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.dbccompany.enuns.TipoContratacao;

@Entity
@Table(name = "contratacao")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Contratacao.class)
public class Contratacao {

	@Id
	@SequenceGenerator(allocationSize = 1, name = "contratacao_seq", sequenceName = "contratacao_seq")
	@GeneratedValue(generator = "contratacao_seq", strategy = GenerationType.SEQUENCE)
	private Long id;

	@Enumerated(EnumType.STRING)
	@Column(name = "tipo_contratacao")
	private TipoContratacao tipoContratacao;

	@Column(name = "quantidade", precision = 10, nullable = false)
	private Integer quantidade;

	@Column(name = "desconto", precision = 10, scale = 2)
	private Double desconto;

	@Column(name = "prazo", precision = 10, nullable = false)
	private Integer prazo;

	@ManyToOne
	@JoinColumn(name="id_cliente")
	@JsonDeserialize
	private Cliente cliente;

	@ManyToOne
	@JoinColumn(name="id_espaco")
	@JsonDeserialize
	private Espaco espaco;
	
	@OneToMany(mappedBy="contratacao", fetch=FetchType.LAZY, cascade= {CascadeType.MERGE, CascadeType.PERSIST}, orphanRemoval=true)
	private List<Pagamento> pagamentos = new ArrayList<>();

	public Long getId() {	
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TipoContratacao getTipoContratacao() {
		return tipoContratacao;
	}

	public void setTipoContratacao(TipoContratacao tipoContratacao) {
		this.tipoContratacao = tipoContratacao;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public Double getDesconto() {
		return desconto;
	}

	public void setDesconto(Double desconto) {
		this.desconto = desconto;
	}

	public Integer getPrazo() {
		return prazo;
	}

	public void setPrazo(Integer prazo) {
		this.prazo = prazo;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Espaco getEspaco() {
		return espaco;
	}

	public void setEspaco(Espaco espaco) {
		this.espaco = espaco;
	}

	public List<Pagamento> getPagamentos() {
		return pagamentos;
	}

	public void setPagamentos(List<Pagamento> pagamentos) {
		this.pagamentos = pagamentos;
	}

	
	

	

}
