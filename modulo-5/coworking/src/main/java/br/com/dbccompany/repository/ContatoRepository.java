package br.com.dbccompany.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.entity.Cliente;
import br.com.dbccompany.entity.Contato;
import br.com.dbccompany.entity.TipoContato;

public interface ContatoRepository extends CrudRepository<Contato, Long> {

	public Contato findByValor(String valor);
	public List<Contato> findAllByTipoContato(TipoContato contato);
	public List<Contato> findAllByCliente(Cliente cliente);
	
	//todosOsContatosPassandoUmCliente

}