package br.com.dbccompany.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.entity.Users;
import br.com.dbccompany.entity.UserRole;
import br.com.dbccompany.entity.UserRoleId;
import br.com.dbccompany.repository.UserRepository;

@Service
public class UserService implements IServicePadrao<Users> {

	@Autowired
	private UserRepository userRepository;

	@Override
	public List<Users> findAll() {
		return (List<Users>) userRepository.findAll();
	}

//	@Override
//	@Transactional(rollbackFor = Exception.class)
//	public User save(User user) {
//		return (User) usuarioRepository.save(user);
//	}

	@Override
	public Users findById(Long id) {
		return userRepository.findById(id).orElse(null);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Users update(Long id, Users user) {
		user.setId(id);
		return userRepository.save(user);
	}

	@Override
	public void deleteById(Long id) {
		userRepository.deleteById(id);
	}
	
	public Users findByNome(String nome){
		return userRepository.findByNome(nome);
	}
	
	public Users findByEmail(String email){
		return userRepository.findByEmail(email);
	}
	public Users findByLogin(String login){
		return userRepository.findByLogin(login);
	}
	public Users findBySenha(String senha){
		return userRepository.findBySenha(senha);
	}
	
	public Users save(Users user) {
        if (user.getRoles().isEmpty()) {
            UserRoleId roleId = new UserRoleId(user, "USER");
            UserRole role = new UserRole();
            role.setId(roleId);
            user.getRoles().add(role);
        }

        return userRepository.save(user);
    }

	
	



}
