package br.com.dbccompany.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.entity.ClientePacote;
import br.com.dbccompany.entity.EspacoPacote;
import br.com.dbccompany.entity.Pacote;
import br.com.dbccompany.repository.ClientePacoteRepository;
import br.com.dbccompany.repository.PacoteRepository;

@Service
public class PacoteService implements IServicePadrao<Pacote> {

	@Autowired
	private PacoteRepository pacoteRepository;
	
	@Autowired
	private ClientePacoteRepository clientePacoteRepository;

	@Override
	public List<Pacote> findAll() {
		return (List<Pacote>) pacoteRepository.findAll();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Pacote save(Pacote pacote) {	
		return (Pacote) pacoteRepository.save(pacote);
	}

	@Override
	public Pacote findById(Long id) {
		return pacoteRepository.findById(id).orElse(null);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Pacote update(Long id, Pacote pacote) {
		pacote.setId(id);
		return pacoteRepository.save(pacote);
	}

	@Override
	public void deleteById(Long id) {
		pacoteRepository.deleteById(id);
	}
	
	public Pacote findByValor(Double valor){
		return pacoteRepository.findByValor(valor);
	}
	public List<Pacote> findAllByClientePacotes(List<ClientePacote> clientePacotes){
		return pacoteRepository.findAllByClientePacotes(clientePacotes);
	}
	public List<Pacote> findAllByEspacoPacotes(List<EspacoPacote> espacoPacotes){
		return pacoteRepository.findAllByEspacoPacotes(espacoPacotes);
	}	



}
