package br.com.dbccompany.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.entity.Cliente;
import br.com.dbccompany.entity.Contato;
import br.com.dbccompany.entity.TipoContato;
import br.com.dbccompany.service.ContatoService;

@Controller
@RequestMapping("/api/contato")
public class ContatoController {

	@Autowired
	ContatoService contatoService;

	@GetMapping(value = "/")
	@ResponseBody
	public List<Contato> findAll() {
		return contatoService.findAll();
	}

	@PostMapping(path = "/novo")
	@ResponseBody
	public Contato save(@RequestBody Contato contato) {
		return contatoService.save(contato);
	}

	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Contato update(@PathVariable Long id, @RequestBody Contato contato) {
		contato.setId(id);
		return contatoService.save(contato);
	}

	@DeleteMapping(value = "/deletar/{id}")
	public ResponseEntity<Contato> deleteById(@PathVariable Long id) {
		contatoService.deleteById(id);
		return ResponseEntity.noContent().build();

	}
	
	@GetMapping(value="/buscarcontato/{valor}")
	@ResponseBody
	public Contato buscarTipoContatoPorValorDoContato(@PathVariable("valor") String valor) {
		return contatoService.findByValor(valor);
	}
	
	@GetMapping(value = "/buscarcontatos/tipodecontato")
	@ResponseBody
	public List<Contato> listarContatosDeUmTipoDeContato(@RequestBody TipoContato tipoContato) {
		return  contatoService.findAllByTipoContato(tipoContato);
	}
	
	@GetMapping(value = "/buscarcontatos/porclientes")
	@ResponseBody
	public List<Contato> encontrarTodosOsContatosDeListaClientes(@RequestBody Cliente cliente) {
		return contatoService.findAllByCliente(cliente);
	}
	

}
