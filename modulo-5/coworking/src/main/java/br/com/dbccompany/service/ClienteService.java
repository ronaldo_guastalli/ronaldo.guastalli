package br.com.dbccompany.service;

import java.sql.Date;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.entity.Cliente;
import br.com.dbccompany.entity.Contato;
import br.com.dbccompany.repository.ClienteRepository;
import br.com.dbccompany.repository.ContatoRepository;

@Service
public class ClienteService implements IServicePadrao<Cliente> {

	@Autowired
	private ClienteRepository clienteRepository;

	@Autowired
	private ContatoRepository contatoRepository;

	@Override
	public List<Cliente> findAll() {
		return (List<Cliente>) clienteRepository.findAll();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Cliente save(Cliente cliente) throws Exception {
		Integer qtdEmail = 0;
		Integer qtdTelefone = 0;
		List<Contato> contatos = cliente.getContatos();
		for (Contato contato : contatos) {
			Contato contatoCompleto = contatoRepository.findById(contato.getId()).get();
			qtdEmail += contatoCompleto.getTipoContato().getNome().equalsIgnoreCase("email") ? 1 : 0;
			qtdTelefone += contatoCompleto.getTipoContato().getNome().equalsIgnoreCase("telefone") ? 1 : 0;
		}

		if (qtdEmail > 0 && qtdTelefone > 0) {
			return clienteRepository.save(cliente);
		}
		throw new Exception("É obrigatorio 1 email e 1 telefone");

	}

	@Override
	public Cliente findById(Long id) {
		return clienteRepository.findById(id).orElse(null);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Cliente update(Long id, Cliente t) {
		t.setId(id);
		return clienteRepository.save(t);
	}

	@Override
	public void deleteById(Long id) {
		clienteRepository.deleteById(id);
	}

	public Cliente findByNome(String nome) {
		return clienteRepository.findByNome(nome);
	}

	public Cliente findByCpf(String cpf) {
		return clienteRepository.findByCpf(cpf);
	}

	public Cliente findByDataNascimento(LocalDateTime dataNascimento) {
		return clienteRepository.findByDataNascimento(dataNascimento);
	}

}
