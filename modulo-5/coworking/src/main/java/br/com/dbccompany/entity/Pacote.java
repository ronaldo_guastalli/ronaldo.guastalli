package br.com.dbccompany.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "pacote")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Pacote.class)
public class Pacote {

	@Id
	@SequenceGenerator(allocationSize = 1, name = "pacote_seq", sequenceName = "pacote_seq")
	@GeneratedValue(generator = "pacote_seq", strategy = GenerationType.SEQUENCE)
	private Long id;

	@Column(name="valor", nullable=false, precision=10, scale=2)
	private Double valor;

	@OneToMany(mappedBy="pacote", fetch=FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST}, orphanRemoval = true)
	private List<ClientePacote> clientePacotes = new ArrayList<ClientePacote>();

	@OneToMany(mappedBy="pacote", fetch=FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST}, orphanRemoval = true)
	private List<EspacoPacote> espacoPacotes = new ArrayList<EspacoPacote>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public List<ClientePacote> getClientePacotes() {
		return clientePacotes;
	}

	public void setClientePacotes(List<ClientePacote> clientePacotes) {
		this.clientePacotes = clientePacotes;
	}

	public List<EspacoPacote> getEspacoPacotes() {
		return espacoPacotes;
	}

	public void setEspacoPacotes(List<EspacoPacote> espacoPacotes) {
		this.espacoPacotes = espacoPacotes;
	}
	
	

}
