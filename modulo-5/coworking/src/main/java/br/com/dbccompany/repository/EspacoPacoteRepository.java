package br.com.dbccompany.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.entity.EspacoPacote;
import br.com.dbccompany.entity.Pacote;
import br.com.dbccompany.enuns.TipoContratacao;

public interface EspacoPacoteRepository extends CrudRepository<EspacoPacote, Long> {

	public EspacoPacote findByQuantidade(Integer quantidade);
	public EspacoPacote findByPrazo(Integer prazo);
	public EspacoPacote findByTipoContratacao(TipoContratacao tipoContratacao);
	public EspacoPacote findByPacote(Pacote pacote);
	

}