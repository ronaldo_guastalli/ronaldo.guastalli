package br.com.dbccompany.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.entity.EspacoPacote;
import br.com.dbccompany.service.EspacoPacoteService;

@Controller
@RequestMapping("/api/espacopacote")
public class EspacoPacoteController {

	@Autowired
	EspacoPacoteService espacoPacoteService;

	@GetMapping(value = "/")
	@ResponseBody
	public List<EspacoPacote> findAll() {
		return espacoPacoteService.findAll();
	}

	@PostMapping(path = "/novo", produces = "application/json")
	@ResponseBody
	public EspacoPacote save(@RequestBody EspacoPacote espacoPacote) throws Exception {
		return espacoPacoteService.save(espacoPacote);
	}

	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public EspacoPacote update(@PathVariable Long id, @RequestBody EspacoPacote espacoPacote) throws Exception {
		espacoPacote.setId(id);
		return espacoPacoteService.save(espacoPacote);
	}

	@DeleteMapping(value = "/deletar/{id}")
	public ResponseEntity<EspacoPacote> deleteById(@PathVariable Long id) {
		espacoPacoteService.deleteById(id);
		return ResponseEntity.noContent().build();

	}

}
