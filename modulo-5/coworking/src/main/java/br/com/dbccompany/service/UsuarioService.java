package br.com.dbccompany.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.entity.Usuario;
import br.com.dbccompany.repository.UsuarioRepository;

@Service
public class UsuarioService implements IServicePadrao<Usuario> {

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Override
	public List<Usuario> findAll() {
		return (List<Usuario>) usuarioRepository.findAll();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Usuario save(Usuario usuario) {
		return (Usuario) usuarioRepository.save(usuario);
	}

	@Override
	public Usuario findById(Long id) {
		return usuarioRepository.findById(id).orElse(null);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Usuario update(Long id, Usuario usuario) {
		usuario.setId(id);
		return usuarioRepository.save(usuario);
	}

	@Override
	public void deleteById(Long id) {
		usuarioRepository.deleteById(id);
	}
	
	public Usuario findByNome(String nome){
		return usuarioRepository.findByNome(nome);
	}
	
	public Usuario findByEmail(String email){
		return usuarioRepository.findByEmail(email);
	}
	public Usuario findByLogin(String login){
		return usuarioRepository.findByLogin(login);
	}
	public Usuario findBySenha(String senha){
		return usuarioRepository.findBySenha(senha);
	}

	
	



}
