package br.com.dbccompany.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "tipo_contato")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = TipoContato.class)
public class TipoContato {

	@Id
	@SequenceGenerator(allocationSize = 1, name = "tipo_contato_seq", sequenceName = "tipo_contato_seq")
	@GeneratedValue(generator = "tipo_contato_seq", strategy = GenerationType.SEQUENCE)
	private Long id;

	@Column(name = "nome", nullable = false, length = 150, unique = true)
	private String nome;

	@OneToMany(mappedBy = "tipoContato", fetch=FetchType.LAZY, cascade={CascadeType.MERGE, CascadeType.PERSIST}, orphanRemoval=true)
	private List<Contato> contatos = new ArrayList<Contato>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Contato> getContatos() {
		return contatos;
	}

	public void setContatos(List<Contato> contatos) {
		this.contatos = contatos;
	}

}
