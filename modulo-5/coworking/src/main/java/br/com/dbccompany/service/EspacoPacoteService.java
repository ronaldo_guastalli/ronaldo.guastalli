package br.com.dbccompany.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.entity.EspacoPacote;
import br.com.dbccompany.entity.Pacote;
import br.com.dbccompany.enuns.TipoContratacao;
import br.com.dbccompany.repository.EspacoPacoteRepository;
import br.com.dbccompany.repository.EspacoRepository;
import br.com.dbccompany.repository.PacoteRepository;
import br.com.dbccompany.service.msgExceptions.MessageExceptions;

@Service
public class EspacoPacoteService implements IServicePadrao<EspacoPacote> {

	@Autowired
	private EspacoPacoteRepository espacoPacoteRepository;
	
	@Autowired
	private EspacoRepository espacoRepository;
	
	@Autowired
	private PacoteRepository pacoteRepository;

	@Override
	public List<EspacoPacote> findAll() {
		return (List<EspacoPacote>) espacoPacoteRepository.findAll();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public EspacoPacote save(EspacoPacote espacoPacote) throws Exception {
		if(espacoPacote.getTipoContratacao() == null) {
			throw new Exception(MessageExceptions.PACOTE_ESPACO_SEM_TIPO_CONTRATACAO);
		}else {
			if(espacoPacote.getPacote().getId() == null) {
				espacoPacote.setEspaco(espacoPacote.getEspaco());
				espacoRepository.save(espacoPacote.getEspaco());			
			}
			if(espacoPacote.getPacote().getId() == null) {
				espacoPacote.setPacote(espacoPacote.getPacote());
				pacoteRepository.save(espacoPacote.getPacote());
			}
		}
		
		return (EspacoPacote) espacoPacoteRepository.save(espacoPacote);
	}

	@Override
	public EspacoPacote findById(Long id) {
		return espacoPacoteRepository.findById(id).orElse(null);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public EspacoPacote update(Long id, EspacoPacote espacoPacote) {
		espacoPacote.setId(id);
		return espacoPacoteRepository.save(espacoPacote);
	}

	@Override
	public void deleteById(Long id) {
		espacoPacoteRepository.deleteById(id);
	}
	
	public EspacoPacote findByQuantidade(Integer quantidade){
		return espacoPacoteRepository.findByQuantidade(quantidade);
	}
	public EspacoPacote findByPrazo(Integer prazo){
		return espacoPacoteRepository.findByPrazo(prazo);
	}
	public EspacoPacote findByTipoContratacao(TipoContratacao tipoContratacao){
		return espacoPacoteRepository.findByTipoContratacao(tipoContratacao);
	}

	public EspacoPacote findByPacote(Pacote pacote){
		return espacoPacoteRepository.findByPacote(pacote);
	}
	

}
