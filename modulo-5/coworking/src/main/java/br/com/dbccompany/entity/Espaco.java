package br.com.dbccompany.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "espaco")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Espaco.class)
public class Espaco {

	@Id
	@SequenceGenerator(allocationSize = 1, name = "espaco_seq", sequenceName = "espaco_seq")
	@GeneratedValue(generator = "espaco_seq", strategy = GenerationType.SEQUENCE)
	private Long id;

	@Column(name = "nome", nullable = false, length = 150)
	private String nome;

	@Column(name="qtd_pessoa")
	private Integer qtdPessoa;

	@Column(name="valor", precision=10, scale=2)
	private Double valor;

	@OneToMany(mappedBy="espaco", fetch=FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST}, orphanRemoval = true)
	private List<Contratacao> contratacoes =new ArrayList<Contratacao>();

	@OneToMany(mappedBy="espaco", fetch=FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST}, orphanRemoval = true)	
	private List<EspacoPacote> espacoPacotes = new ArrayList<EspacoPacote>();

//	@OneToMany(mappedBy="saldoClienteId.idEspaco")
//	private List<SaldoCliente> saldoClientes = new ArrayList<SaldoCliente>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getQtdPessoa() {
		return qtdPessoa;
	}

	public void setQtdPessoa(Integer qtdPessoa) {
		this.qtdPessoa = qtdPessoa;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public List<Contratacao> getContratacoes() {
		return contratacoes;
	}

	public void setContratacoes(List<Contratacao> contratacoes) {
		this.contratacoes = contratacoes;
	}

	public List<EspacoPacote> getEspacoPacotes() {
		return espacoPacotes;
	}

	public void setEspacoPacotes(List<EspacoPacote> espacoPacotes) {
		this.espacoPacotes = espacoPacotes;
	}

//	public List<SaldoCliente> getSaldoClientes() {
//		return saldoClientes;
//	}
//
//	public void setSaldoClientes(List<SaldoCliente> saldoClientes) {
//		this.saldoClientes = saldoClientes;
//	}
	
	

}
