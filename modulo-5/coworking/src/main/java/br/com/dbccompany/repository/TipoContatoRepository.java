package br.com.dbccompany.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.entity.Contato;
import br.com.dbccompany.entity.TipoContato;

public interface TipoContatoRepository extends CrudRepository<TipoContato, Long> {

	public TipoContato findByNome(String nome);
	public List<TipoContato> findAllByContatos(List<Contato> contatos);

}
