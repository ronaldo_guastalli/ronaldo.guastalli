package br.com.dbccompany.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.entity.ClientePacote;
import br.com.dbccompany.entity.Pacote;
import br.com.dbccompany.repository.ClientePacoteRepository;
import br.com.dbccompany.repository.ClienteRepository;
import br.com.dbccompany.repository.PacoteRepository;

@Service
public class ClientePacoteService implements IServicePadrao<ClientePacote> {

	@Autowired
	private ClientePacoteRepository clientePacoteRepository;

	@Autowired
	private PacoteRepository pacoteRepository;

	@Autowired
	private ClienteRepository clienteRepository;

	@Override
	public List<ClientePacote> findAll() {
		return (List<ClientePacote>) clientePacoteRepository.findAll();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ClientePacote save(ClientePacote clientePacote) {
		if (clientePacote.getCliente().getId() == null) {
			clientePacote.setCliente(clientePacote.getCliente());
			clienteRepository.save(clientePacote.getCliente());
		}
		if (clientePacote.getPacote().getId() == null) {
			clientePacote.setPacote(clientePacote.getPacote());
			pacoteRepository.save(clientePacote.getPacote());
		}
		return (ClientePacote) clientePacoteRepository.save(clientePacote);
	}

	@Override
	public ClientePacote findById(Long id) {
		return clientePacoteRepository.findById(id).orElse(null);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ClientePacote update(Long id, ClientePacote clientePacote) {
		clientePacote.setId(id);
		return clientePacoteRepository.save(clientePacote);
	}

	@Override
	public void deleteById(Long id) {
		clientePacoteRepository.deleteById(id);
	}

	public ClientePacote findByQuantidade(Integer quantidade) {
		return clientePacoteRepository.findByQuantidade(quantidade);
	}

	public ClientePacote findByPacote(Pacote pacote) {
		return clientePacoteRepository.findByPacote(pacote);
	}

	public ClientePacote findByCliente(ClientePacote cliente) {
		return clientePacoteRepository.findByCliente(cliente);
	}

}
