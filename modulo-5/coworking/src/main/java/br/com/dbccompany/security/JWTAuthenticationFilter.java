package br.com.dbccompany.security;

import static br.com.dbccompany.security.TokenAuthenticationService.HEADER_STRING;

import java.io.IOException;
import java.util.Calendar;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import io.jsonwebtoken.ExpiredJwtException;

@Component
public class JWTAuthenticationFilter extends GenericFilterBean {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(JWTAuthenticationFilter.class);
	
	@Autowired
    private TokenAuthenticationService tokenService;

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
//		Authentication authentication = TokenAuthenticationService.getAuthentication((HttpServletRequest) request);
//
//		SecurityContextHolder.getContext().setAuthentication(authentication);
//		chain.doFilter(request, response);
		
		try {
            String token = ((HttpServletRequest) request).getHeader(HEADER_STRING);
            if(token == null) {
                token = ((HttpServletRequest) request).getParameter(HEADER_STRING);
            }
            LOGGER.info("Request from " + token + " at " + Calendar.getInstance().getTime());

            Authentication authentication = tokenService.getAuthentication((HttpServletRequest) request);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            chain.doFilter(request, response);
        } catch (ExpiredJwtException e) {
            HttpServletResponse httpResponse = (HttpServletResponse) response;
            httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        }

	}

}
