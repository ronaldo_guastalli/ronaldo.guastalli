package br.com.dbccompany.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.entity.Users;

public interface UserRepository extends CrudRepository<Users, Long> {

	public Users findByNome(String nome);
	public Users findByEmail(String email);
	public Users findByLogin(String login);
	public Users findBySenha(String senha);
	

}