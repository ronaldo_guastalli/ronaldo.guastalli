package br.com.dbccompany.repository;

import java.sql.Date;
import java.time.LocalDateTime;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.entity.Cliente;
import br.com.dbccompany.entity.Contratacao;

public interface ClienteRepository extends CrudRepository<Cliente, Long> {

	public Cliente findByNome(String nome);
	public Cliente findByCpf(String cpf);
	public Cliente findByDataNascimento(LocalDateTime dataNascimento);
	

}