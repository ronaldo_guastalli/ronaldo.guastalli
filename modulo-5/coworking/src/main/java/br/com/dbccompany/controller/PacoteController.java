package br.com.dbccompany.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.entity.Pacote;
import br.com.dbccompany.service.PacoteService;

@Controller
@RequestMapping("/api/pacote")
public class PacoteController {

	@Autowired
	PacoteService pacoteService;

	@GetMapping(value = "/")
	@ResponseBody
	public List<Pacote> findAll() {
		return pacoteService.findAll();
	}

	@PostMapping(path = "/novo")
	@ResponseBody
	public Pacote save(@RequestBody Pacote pacote) {
		return pacoteService.save(pacote);
	}

	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Pacote update(@PathVariable Long id, @RequestBody Pacote pacote) {
		pacote.setId(id);
		return pacoteService.save(pacote);
	}

	@DeleteMapping(value = "/deletar/{id}")
	public ResponseEntity<Pacote> deleteById(@PathVariable Long id) {
		pacoteService.deleteById(id);
		return ResponseEntity.noContent().build();

	}

}
