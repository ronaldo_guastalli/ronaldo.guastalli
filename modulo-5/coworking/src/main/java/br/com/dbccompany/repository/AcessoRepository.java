package br.com.dbccompany.repository;

import java.sql.Date;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.dbccompany.entity.Acesso;
import br.com.dbccompany.entity.SaldoCliente;

@Repository
public interface AcessoRepository extends CrudRepository<Acesso, Long> {

	public Acesso findByIsEntrada(Boolean isEntrada);
	public Acesso findByData(LocalDateTime data);
	public Acesso findByIsExcecao(Boolean isExcecao);
	public Acesso findBySaldoCliente(SaldoCliente saldoCliente);

	

}