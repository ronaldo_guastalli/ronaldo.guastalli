package br.com.dbccompany.entity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import br.com.dbccompany.enuns.TipoContratacao;

@Entity
@Table(name = "saldo_cliente")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = SaldoCliente.class)
public class SaldoCliente {

	@EmbeddedId
	private SaldoClienteId saldoClienteId;

	@Enumerated(EnumType.STRING)
	@Column(name = "tipo_contratacao")
	private TipoContratacao tipoContratacao;

	@Column(name = "quantidade", precision = 10, nullable = false)
	private Integer quantidade;

	@Column(name = "vencimento", nullable = false)
	private LocalDateTime vencimento;

	@OneToMany(mappedBy = "saldoCliente", fetch=FetchType.EAGER, cascade= {CascadeType.MERGE})
	private List<Acesso> acessos = new ArrayList<Acesso>();

	public SaldoCliente() {}

	public SaldoCliente(SaldoClienteId saldoClienteId) {
		saldoClienteId.setIdCliente(saldoClienteId.getIdCliente());
		saldoClienteId.setIdEspaco(saldoClienteId.getIdEspaco());
		
	}

	public SaldoClienteId getSaldoClienteId() {
		return saldoClienteId;
	}

	public void setSaldoClienteId(SaldoClienteId saldoClienteId) {
		this.saldoClienteId = saldoClienteId;
	}

	public TipoContratacao getTipoContratacao() {
		return tipoContratacao;
	}

	public void setTipoContratacao(TipoContratacao tipoContratacao) {
		this.tipoContratacao = tipoContratacao;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}	

	

	public LocalDateTime getVencimento() {
		return vencimento;
	}

	public void setVencimento(LocalDateTime vencimento) {
		this.vencimento = vencimento;
	}

	public List<Acesso> getAcessos() {
		return acessos;
	}

	public void setAcessos(List<Acesso> acessos) {
		this.acessos = acessos;
	}

	

}