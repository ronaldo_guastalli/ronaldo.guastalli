package br.com.dbccompany.service.util;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.dbccompany.entity.Acesso;
import br.com.dbccompany.entity.SaldoCliente;
import br.com.dbccompany.entity.SaldoClienteId;
import br.com.dbccompany.enuns.TipoContratacao;
import br.com.dbccompany.repository.ClientePacoteRepository;
import br.com.dbccompany.repository.ClienteRepository;
import br.com.dbccompany.repository.ContratacaoRepository;
import br.com.dbccompany.repository.EspacoRepository;
import br.com.dbccompany.repository.PacoteRepository;
import br.com.dbccompany.repository.PagamentoRepository;
import br.com.dbccompany.repository.SaldoClienteRepository;

@Service
public class UtilService {

	@Autowired
	private PagamentoRepository pagamentoRepository;

	@Autowired
	private PacoteRepository pacoteRepository;

	@Autowired
	private ClienteRepository clienteRepository;

	@Autowired
	private ClientePacoteRepository clientePacoteRepository;

	@Autowired
	private SaldoClienteRepository saldoClienteRepository;

	@Autowired
	private ContratacaoRepository contratacaoRepository;

	@Autowired
	private EspacoRepository espacoRepository;

	public static Double calculaValorDoOrcamento(Double valorEspaco, Integer qtd, TipoContratacao tipoContratacao,
			Double valorDesconto) {
		Double valorOrcamento = 0.0;
		switch (tipoContratacao) {
		case MINUTO:
			valorOrcamento = (qtd * valorEspaco * (1 - valorDesconto)) / 60;
			break;
		case HORA:
			valorOrcamento = qtd * valorEspaco * (1 - valorDesconto);
			break;
		case DIARIA:
			valorOrcamento = (qtd * valorEspaco * (1 - valorDesconto)) * 24;
			break;
		case SEMANA:
			valorOrcamento = (qtd * valorEspaco * (1 - valorDesconto)) * 44;
			break;
		case TURNO:
			valorOrcamento = (qtd * valorEspaco * (1 - valorDesconto)) * 5;
			break;
		case MES:
			valorOrcamento = (qtd * valorEspaco * (1 - valorDesconto)) * 220;
			break;
		default:
			valorOrcamento = valorEspaco * qtd;
			break;
		}
		return valorOrcamento;
	}

	
}
