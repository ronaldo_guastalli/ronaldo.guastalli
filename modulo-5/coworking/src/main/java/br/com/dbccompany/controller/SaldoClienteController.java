package br.com.dbccompany.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.entity.SaldoCliente;
import br.com.dbccompany.entity.SaldoClienteId;
import br.com.dbccompany.service.SaldoClienteService;

@Controller
@RequestMapping("/api/saldocliente")
public class SaldoClienteController {

	@Autowired
	SaldoClienteService saldoClienteService;
	
	

//	@GetMapping(value = "/")
//	@ResponseBody
//	public List<SaldoCliente> findAll(@RequestBody Long idCliente, @RequestBody Long idEspaco , @RequestBody SaldoClienteId saldoCliente) {
//		return saldoClienteService.findAllId(idCliente, idEspaco, saldoCliente);
//	}
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<SaldoCliente> findAll() {
		return saldoClienteService.findAll();
	}

	@PostMapping(path = "/novo")
	@ResponseBody
	public SaldoCliente save(@RequestBody SaldoCliente saldoCliente) {
		return saldoClienteService.save(saldoCliente);
	}

	@PutMapping(value = "/editar/{idCliente}/{idEspaco}")
	@ResponseBody
	public SaldoCliente update(@PathVariable Long idCliente, @PathVariable Long idEspaco , @RequestBody SaldoClienteId saldoCliente) {
		saldoCliente.setIdCliente(idCliente);
		saldoCliente.setIdEspaco(idEspaco);
		return saldoClienteService.updateIds(idCliente, idEspaco, saldoCliente);
	}

	@DeleteMapping(value = "/deletar/{id}")
	public ResponseEntity<SaldoCliente> deleteById(@PathVariable SaldoClienteId id) {
		saldoClienteService.deleteById(id);
		return ResponseEntity.noContent().build();

	}

}
