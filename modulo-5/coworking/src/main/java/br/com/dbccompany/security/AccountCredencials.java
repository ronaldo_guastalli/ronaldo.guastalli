package br.com.dbccompany.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

public class AccountCredencials {
	private String username;
	private String password;
	private String token;
    private Collection<? extends GrantedAuthority> roles;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public Collection<? extends GrantedAuthority> getRoles() {
		return roles;
	}
	public void setRoles(Collection<? extends GrantedAuthority> roles) {
		this.roles = roles;
	}
	
	

}
