package br.com.dbccompany.security;

import static java.util.stream.Collectors.toSet;

import java.util.Set;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.entity.Users;
import br.com.dbccompany.repository.UserRepository;

@Service("userDetailsService")
public class UserDetailsImplementacao implements UserDetailsService {

	@Autowired
	private UserRepository usuariosRepository;

	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
		Users usuario = usuariosRepository.findByLogin(login);

		if (usuario == null) {
			throw new UsernameNotFoundException(login + "não registrado! ");
		}

		Set<GrantedAuthority> permissoes = Stream.of(new SimpleGrantedAuthority(usuario.getNome())).collect(toSet());

		return new User(usuario.getLogin(), usuario.getSenha(), permissoes);
	}

}
