package br.com.dbccompany.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.entity.Cliente;
import br.com.dbccompany.entity.Contato;
import br.com.dbccompany.entity.TipoContato;
import br.com.dbccompany.repository.ContatoRepository;

@Service
public class ContatoService implements IServicePadrao<Contato> {

	@Autowired
	private ContatoRepository contatoRepository;

	@Override
	public List<Contato> findAll() {
		return (List<Contato>) contatoRepository.findAll();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Contato save(Contato t) {
		return (Contato) contatoRepository.save(t);
	}

	@Override
	public Contato findById(Long id) {
		return contatoRepository.findById(id).orElse(null);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Contato update(Long id, Contato t) {
		t.setId(id);
		return contatoRepository.save(t);
	}

	@Override
	public void deleteById(Long id) {
		contatoRepository.deleteById(id);
	}

	public Contato findByValor(String valor) {
		return contatoRepository.findByValor(valor);
	}

	public List<Contato> findAllByTipoContato(TipoContato contato) {
		return contatoRepository.findAllByTipoContato(contato);
	}

	public List<Contato> findAllByCliente(Cliente cliente) {
		return contatoRepository.findAllByCliente(cliente);
	}

}
