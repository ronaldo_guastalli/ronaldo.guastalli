package br.com.dbccompany.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.entity.Espaco;
import br.com.dbccompany.service.EspacoService;

@Controller
@RequestMapping("/api/espaco")
public class EspacoController {

	@Autowired
	EspacoService espacoService;

	@GetMapping(value = "/")
	@ResponseBody
	public List<Espaco> findAll() {
		return espacoService.findAll();
	}

	@PostMapping(path = "/novo", produces = "application/json")
	@ResponseBody
	public Espaco save(@RequestBody Espaco espaco) {
		return espacoService.save(espaco);
	}

	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Espaco update(@PathVariable Long id, @RequestBody Espaco espaco) {
		espaco.setId(id);
		return espacoService.save(espaco);
	}

	@DeleteMapping(value = "/deletar/{id}")
	public ResponseEntity<Espaco> deleteById(@PathVariable Long id) {
		espacoService.deleteById(id);
		return ResponseEntity.noContent().build();

	}

}
