package br.com.dbccompany.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.entity.Acesso;
import br.com.dbccompany.service.AcessoService;

@Controller
@RequestMapping("/api/acesso")
public class AcessoController {

	@Autowired
	AcessoService acessoService;

	@GetMapping(value = "/")
	@ResponseBody
	public List<Acesso> findAll() {
		return acessoService.findAll();
	}
	
	@PostMapping(path = "/novo", produces = "application/json")
	@ResponseBody
	public String salvarNovoAcesso(@RequestBody Acesso acesso) {
		return acessoService.salvarNovoAcesso(acesso);
	}

	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Acesso update(@PathVariable Long id, @RequestBody Acesso acesso) {
		acesso.setId(id);
		return acessoService.save(acesso);
	}

	@DeleteMapping(value = "/deletar/{id}")
	public ResponseEntity<Acesso> deleteById(@PathVariable Long id) {
		acessoService.deleteById(id);
		return ResponseEntity.noContent().build();
	}


}
