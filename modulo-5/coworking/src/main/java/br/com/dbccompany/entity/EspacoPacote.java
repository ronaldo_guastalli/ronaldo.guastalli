package br.com.dbccompany.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import br.com.dbccompany.enuns.TipoContratacao;

@Entity
@Table(name = "espaco_pacote")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = EspacoPacote.class)
public class EspacoPacote {

	@Id
	@SequenceGenerator(allocationSize = 1, name = "espaco_pacote_seq", sequenceName = "espaco_pacote_seq")
	@GeneratedValue(generator = "espaco_pacote_seq", strategy = GenerationType.SEQUENCE)
	private Long id;
	
	@Column(name="quantidade", nullable=false)
	private Integer quantidade;

	@Column(name="prazo", nullable=false)
	private Integer prazo;

	@Enumerated(EnumType.STRING)
	@Column(name="tipo_contratacao")
	private TipoContratacao tipoContratacao;

	@ManyToOne
	@JoinColumn(name="id_pacote")
	private Pacote pacote;

	@ManyToOne
	@JoinColumn(name="id_espaco")
	private Espaco espaco;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public Integer getPrazo() {
		return prazo;
	}

	public void setPrazo(Integer prazo) {
		this.prazo = prazo;
	}

	public TipoContratacao getTipoContratacao() {
		return tipoContratacao;
	}

	public void setTipoContratacao(TipoContratacao tipoContratacao) {
		this.tipoContratacao = tipoContratacao;
	}

	public Pacote getPacote() {
		return pacote;
	}

	public void setPacote(Pacote pacote) {
		this.pacote = pacote;
	}

	public Espaco getEspaco() {
		return espaco;
	}

	public void setEspaco(Espaco espaco) {
		this.espaco = espaco;
	}
	
	

}
