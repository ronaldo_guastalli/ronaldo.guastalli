package br.com.dbccompany.service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.entity.Acesso;
import br.com.dbccompany.entity.Espaco;
import br.com.dbccompany.entity.SaldoCliente;
import br.com.dbccompany.entity.SaldoClienteId;
import br.com.dbccompany.repository.AcessoRepository;
import br.com.dbccompany.repository.EspacoRepository;
import br.com.dbccompany.repository.SaldoClienteRepository;

@Service
public class AcessoService implements IServicePadrao<Acesso> {

	@Autowired
	private AcessoRepository acessoRepository;

	@Autowired
	private SaldoClienteRepository saldoClienteRepository;

	@Autowired
	private EspacoRepository espacoRepository;

	@Override
	public List<Acesso> findAll() {
		return (List<Acesso>) acessoRepository.findAll();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Acesso save(Acesso acesso) {
		return (Acesso) acessoRepository.save(acesso);
	}

	@Transactional(rollbackFor = Exception.class)
	public String salvarNovoAcesso(Acesso acesso) {
		Acesso acessoAoEspaco = acessoRepository.findBySaldoCliente(acesso.getSaldoCliente());
		LocalDateTime dataAtual = LocalDateTime.now();
		LocalDateTime vencimentoDoSaldo = acessoAoEspaco.getSaldoCliente().getVencimento();
		Boolean saldoInvalidoOuVencido = false;
		LocalDateTime dataSaida;
		String msg = "";
		if (acesso.getIsEntrada()) {
			if (acesso.getSaldoCliente().getQuantidade() >= 0) {
				dataExiste(acesso);
				if (vencimentoDoSaldo.isAfter(dataAtual)) {
					acesso.getSaldoCliente().setQuantidade(0);
					saldoInvalidoOuVencido = true;
					msg = "Saldo vencido.";
				}
				acessoRepository.save(acesso);
			} else {
				msg = "Saldo insuficiente.";
			}
		} else {
			dataSaida = LocalDateTime.now();
			Duration duracaoNoEspaco = Duration.between(acesso.getData(), dataSaida);
			descontarSaldo(duracaoNoEspaco, acesso);
			msg = "saida com sucesso.";
			acessoRepository.save(acesso);
		}
		return msg;
	}

	@Override
	public Acesso findById(Long id) {
		return acessoRepository.findById(id).orElse(null);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Acesso update(Long id, Acesso acesso) {
		acesso.setId(id);
		return acessoRepository.save(acesso);
	}

	@Override
	public void deleteById(Long id) {
		acessoRepository.deleteById(id);
	}

	public Acesso findByIsEntrada(Boolean isEntrada) {
		return acessoRepository.findByIsEntrada(isEntrada);
	}

	public Acesso findByData(LocalDateTime data) {
		return acessoRepository.findByData(data);
	}

	public Acesso findByIsExcecao(Boolean isExcecao) {
		return acessoRepository.findByIsExcecao(isExcecao);
	}

	public Acesso findBySaldoCliente(SaldoCliente saldoCliente) {
		return acessoRepository.findBySaldoCliente(saldoCliente);
	}

	public void dataExiste(Acesso acesso) {
		if (acesso.getData() == null) {
			acesso.setData(LocalDateTime.now());
		}
	}
	
	

	public String mensagemDeEntrada(Acesso acesso) {
		Long idCliente = acesso.getSaldoCliente().getSaldoClienteId().getIdCliente();
		Long idEspaco = acesso.getSaldoCliente().getSaldoClienteId().getIdEspaco();

		SaldoCliente saldoCliente = saldoClienteRepository.findById(acesso.getSaldoCliente().getSaldoClienteId()).get();
		Integer saldoClienteNaEntrada = saldoCliente.getQuantidade();
		Espaco espacoEntrada = espacoRepository.findById(idEspaco).get();
		Double valorDoEspaco = espacoEntrada.getValor();
		if (acesso.getIsEntrada() == true) {
			if (saldoClienteNaEntrada >= valorDoEspaco) {
				return String.format("R$ %d", saldoClienteNaEntrada);
			}
		}
		return "Saldo insuficiente.";
	}

	public SaldoCliente verificarOuGerarSaldoCliente(Long idCliente, Long idEspaco) {
		return saldoClienteRepository.findById(new SaldoClienteId(idCliente, idEspaco))
				.orElse(new SaldoCliente(new SaldoClienteId(idCliente, idEspaco)));
	}
	
	public void descontarSaldo(Duration duracaoNoEspaco, Acesso acesso) {
		Integer novoValor;
		Integer duracao = (int) duracaoNoEspaco.getSeconds()/60; //minutos
		switch (acesso.getSaldoCliente().getTipoContratacao()) {
		case MINUTO:
			novoValor = acesso.getSaldoCliente().getQuantidade() - duracao;
			break;
		case HORA:
			novoValor = acesso.getSaldoCliente().getQuantidade() - duracao/60;
			break;
		case DIARIA:
			novoValor = acesso.getSaldoCliente().getQuantidade() - (int)duracao/24*60;
			break;
		case SEMANA:
			novoValor = acesso.getSaldoCliente().getQuantidade();
			break;
		case TURNO:
			novoValor = acesso.getSaldoCliente().getQuantidade() - duracao/5*60;
			break;
		case MES:
			novoValor = acesso.getSaldoCliente().getQuantidade();
			break;
		default:
			novoValor = acesso.getSaldoCliente().getQuantidade();
			break;
		}
	}

}
