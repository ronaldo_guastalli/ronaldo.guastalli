package br.com.dbccompany.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.entity.Cliente;
import br.com.dbccompany.entity.Contratacao;
import br.com.dbccompany.enuns.TipoContratacao;

public interface ContratacaoRepository extends CrudRepository<Contratacao, Long> {

	public Contratacao findByTipoContratacao(TipoContratacao tipoContratacao);
	public Contratacao findByQuantidade(Integer quantidade);
	public Contratacao findByDesconto(Double desconto);
	public Contratacao findByPrazo(Integer prazo);
	public List<Contratacao> findByCliente(Cliente cliente);


}