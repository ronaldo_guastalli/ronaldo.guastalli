package br.com.dbccompany.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.entity.Espaco;
import br.com.dbccompany.repository.EspacoRepository;

@Service
public class EspacoService implements IServicePadrao<Espaco> {

	@Autowired
	private EspacoRepository espacoRepository;

	@Override
	public List<Espaco> findAll() {
		return (List<Espaco>) espacoRepository.findAll();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Espaco save(Espaco espaco) {
		return (Espaco) espacoRepository.save(espaco);
	}

	@Override
	public Espaco findById(Long id) {
		return espacoRepository.findById(id).orElse(null);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Espaco update(Long id, Espaco espaco) {
		espaco.setId(id);
		return espacoRepository.save(espaco);
	}

	@Override
	public void deleteById(Long id) {
		espacoRepository.deleteById(id);
	}

	public Espaco findByNome(String nome) {
		return espacoRepository.findByNome(nome);
	}

	public Espaco findByQtdPessoa(Integer qtdPessoa) {
		return espacoRepository.findByQtdPessoa(qtdPessoa);
	}

}
