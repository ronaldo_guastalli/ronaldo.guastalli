package br.com.dbccompany.security;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.dbccompany.entity.Users;
import br.com.dbccompany.repository.UserRepository;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class TokenAuthenticationService {

	public static final long EXPIRATION_TIME = 860_000_000;
	public static final String SECRET = "@Dbc2019";
	public static final String TOKEN_PREFIX = "Bearer";
	public static final String HEADER_STRING = "Authorization";
	
	@Autowired
    private DataSource datasource;
	
	@Autowired
    private UserRepository userRepo;

	static void addAuthentication(HttpServletResponse response, String username) {
		String JWT = Jwts.builder().setSubject(username)
				.setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
				.signWith(SignatureAlgorithm.HS512, SECRET).compact();

		response.addHeader(HEADER_STRING, TOKEN_PREFIX + " " + JWT);
	}

//	static Authentication getAuthentication(HttpServletRequest http) {
//		String token = http.getHeader(HEADER_STRING);
//
//		if (token != null) {
//			// Fazer Parser
//			String user = Jwts.parser().setSigningKey(SECRET).parseClaimsJws(token.replace(TOKEN_PREFIX, "")).getBody()
//					.getSubject();
//
//			if (user != null) {
//				return new UsernamePasswordAuthenticationToken(user, null, Collections.emptyList());
//			}
//		}
//		return null;
//	}
	
	//meuuuuuuuu	
	public static String getToken(String username) {
        String JWT = Jwts.builder().setSubject(username)
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS512, SECRET).compact();

        String token = TOKEN_PREFIX + " " + JWT;
        return token;
    }
	
	public static void addAuthentication(HttpServletResponse res, Authentication auth) {
        String token = getToken(auth.getName());
        res.addHeader(HEADER_STRING, token);

        try {
            AccountCredencials c = new AccountCredencials();
            c.setToken(token);
            c.setRoles(auth.getAuthorities());

            ObjectMapper mapper = new ObjectMapper();

            res.getOutputStream().print(mapper.writeValueAsString(c));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
	
	public static Authentication getAuthenticationByToken(String token) {
        String user = null;
        try {
            user = Jwts.parser().setSigningKey(SECRET).parseClaimsJws(token.replace(TOKEN_PREFIX, "")).getBody()
                    .getSubject();
        } catch (Throwable t) {
            return null;
        }

        return user != null ? new UsernamePasswordAuthenticationToken(user, null, null) : null;
    }

    public Authentication getAuthentication(HttpServletRequest request) {
        String token = request.getHeader(HEADER_STRING);
        if(token == null) {
            token = request.getParameter(HEADER_STRING);
        }

        if (token != null) {
            Authentication auth = getAuthenticationByToken(token);
            if (auth == null)
                return null;

            return getAuthentication(auth.getName());
        }

        return null;
    }

    //autenticação
    public Authentication getAuthentication(String username) {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            conn = datasource.getConnection();
            stmt = conn.prepareStatement(WebSecurityConfig.ROLES_QUERY);
            stmt.setString(1, username);
            rs = stmt.executeQuery();

            List<GrantedAuthority> authorities = new ArrayList<>();

            while (rs.next()) {
                String roleName = rs.getString(2);
                authorities.add(new SimpleGrantedAuthority(roleName));
            }

            UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(username, null,
                    authorities);

            Users user = userRepo.findByEmail(username);
            authToken.setDetails(user);

            return authToken;
        } catch (SQLException e) {
            throw new InternalAuthenticationServiceException("Error retrieving user roles", e);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }

            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }

            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

}
