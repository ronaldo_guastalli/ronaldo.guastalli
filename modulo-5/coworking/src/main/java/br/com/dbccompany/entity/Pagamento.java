package br.com.dbccompany.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import br.com.dbccompany.enuns.TipoPagamento;

@Entity
@Table(name = "pagamento")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Pagamento.class)
public class Pagamento {

	@Id
	@SequenceGenerator(allocationSize = 1, name = "pagamento_seq", sequenceName = "pagamento_seq")
	@GeneratedValue(generator = "pagamento_seq", strategy = GenerationType.SEQUENCE)
	private Long id;

	@Enumerated(EnumType.STRING)
	@Column(name="tipo_pagamento")
	private TipoPagamento tipoPagamento;

	@ManyToOne
	@JoinColumn(name="id_cliente_pacote", nullable=true)
	private ClientePacote clientePacote;
	
	@ManyToOne
	@JoinColumn(name="id_contratacao", nullable=true)
	private Contratacao contratacao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TipoPagamento getTipoPagamento() {
		return tipoPagamento;
	}

	public void setTipoPagamento(TipoPagamento tipoPagamento) {
		this.tipoPagamento = tipoPagamento;
	}

	public ClientePacote getClientePacote() {
		return clientePacote;
	}

	public void setClientePacote(ClientePacote clientePacote) {
		this.clientePacote = clientePacote;
	}

	public Contratacao getContratacao() {
		return contratacao;
	}

	public void setContratacao(Contratacao contratacao) {
		this.contratacao = contratacao;
	}
	
	
	

}
