package br.com.dbccompany.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.entity.Usuario;
import br.com.dbccompany.service.UsuarioService;

@Controller
@RequestMapping("/api/usuario")
public class UsuarioController {

	@Autowired
	UsuarioService usuarioService;

	@GetMapping(value = "/")
	@ResponseBody
	public List<Usuario> findAll() {
		return usuarioService.findAll();
	}

	@PostMapping(path = "/novo", produces = "application/json")
	@ResponseBody
	public Usuario save(@RequestBody Usuario usuario) {
		return usuarioService.save(usuario);
	}

	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Usuario update(@PathVariable Long id, @RequestBody Usuario usuario) {
		usuario.setId(id);
		return usuarioService.save(usuario);
	}

	@DeleteMapping(value = "/deletar/{id}")
	public ResponseEntity<Usuario> deleteById(@PathVariable Long id) {
		usuarioService.deleteById(id);
		return ResponseEntity.noContent().build();

	}

	@RequestMapping(value = "/buscar", params = { "nome" }, method = RequestMethod.GET)
	@ResponseBody
	public Usuario buscarTipoDeContatoPorNome(@RequestParam("nome") String nome) {
		return usuarioService.findByNome(nome);

	}

}
