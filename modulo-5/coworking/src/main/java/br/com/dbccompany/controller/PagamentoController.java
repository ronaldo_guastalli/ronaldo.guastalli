package br.com.dbccompany.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.entity.Pagamento;
import br.com.dbccompany.service.PagamentoService;

@Controller
@RequestMapping("/api/pagamento")
public class PagamentoController {

	@Autowired
	PagamentoService pagamentoService;

	@GetMapping(value = "/")
	@ResponseBody
	public List<Pagamento> findAll() {
		return pagamentoService.findAll();
	}

	@PostMapping(path = "/novo")
	@ResponseBody
	public Pagamento save(@RequestBody Pagamento pagamento) throws Exception {
		return pagamentoService.save(pagamento);
	}

	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Pagamento update(@PathVariable Long id, @RequestBody Pagamento pagamento) throws Exception {
		pagamento.setId(id);
		return pagamentoService.save(pagamento);
	}

	@DeleteMapping(value = "/deletar/{id}")
	public ResponseEntity<Pagamento> deleteById(@PathVariable Long id) {
		pagamentoService.deleteById(id);
		return ResponseEntity.noContent().build();

	}

}
