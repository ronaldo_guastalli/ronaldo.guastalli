package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "usuario_colaborador")
public class UsuarioColaborador {
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;
   
    @ManyToOne
    @JoinColumn(name = "id_usuario")
    private Usuario usuario;
    
    @ManyToOne
    @JoinColumn(name = "id_funcao")
    private Funcao funcao;
    
    @ManyToOne
    @JoinColumn(name="id_nivel_aprovacao")
    private NivelAprovacao nivelAprovacao;
    
    @ManyToMany(mappedBy="usuarioAprovadores")
	private List<Emprestimo> emprestimos = new ArrayList<>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Funcao getFuncao() {
		return funcao;
	}

	public void setFuncao(Funcao funcao) {
		this.funcao = funcao;
	}

	public NivelAprovacao getNivelAprovacao() {
		return nivelAprovacao;
	}

	public void setNivelAprovacao(NivelAprovacao nivelAprovacao) {
		this.nivelAprovacao = nivelAprovacao;
	}

	public List<Emprestimo> getEmprestimos() {
		return emprestimos;
	}

	public void pushEmprestimos(Emprestimo... emprestimos) {
		this.emprestimos.addAll(Arrays.asList(emprestimos));
	}
	
	
}
