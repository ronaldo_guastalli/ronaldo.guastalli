package br.com.dbccompany.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@PrimaryKeyJoinColumn(name="id_conta")
public class ContaCorrente extends Conta {    
    
    @Column(name="limite", nullable=false, precision=10, scale=2)
    private Double limite;

	public Double getLimite() {
		return limite;
	}

	public void setLimite(Double limite) {
		this.limite = limite;
	}

	 
    
    
  
}

