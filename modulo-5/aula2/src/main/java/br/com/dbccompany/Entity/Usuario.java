package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "usuario")
public class Usuario {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name="nome")
    private String nome;

    private String senha;

    @ManyToMany
    @JoinTable(name = "usuario_endereco",
            joinColumns
            = {@JoinColumn(name = "id_usuario")},
            inverseJoinColumns
            = {@JoinColumn(name = "id_endereco")})
    private List<Endereco> enderecos = new ArrayList<>();
    
    @OneToMany(mappedBy = "usuario")
    private List<Documento> documentos = new ArrayList<>();
    
    @OneToMany(mappedBy = "usuario")
    private List<Contato> contatos = new ArrayList<>();
    
    @OneToMany(mappedBy = "usuario")
    private List<UsuarioCliente> usuariosCliente = new ArrayList<>();
    
    @ManyToOne
    @JoinColumn(name="id_usuario_tipo")
    private UsuarioTipo usuarioTipo;
    
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public List<Endereco> getEnderecos() {
        return enderecos;
    }

    public void pushEnderecos(Endereco... enderecos) {
        this.enderecos.addAll(Arrays.asList(enderecos));
    }

    public List<Documento> getDocumentos() {
        return documentos;
    }

    public void pushDocumentos(Documento... documentos) {
        this.documentos.addAll(Arrays.asList(documentos));
    }

    public List<Contato> getContatos() {
        return contatos;
    }

    public void pushContatos(Contato... contatos) {
        this.contatos.addAll(Arrays.asList(contatos));
    }   

    public List<UsuarioCliente> getClientes() {
        return usuariosCliente;
    }

    public void pushClientes(UsuarioCliente... clientes) {
        this.usuariosCliente.addAll(Arrays.asList(clientes));
    }     

    public UsuarioTipo getUsuarioTipo() {
		return usuarioTipo;
	}

	public void setUsuarioTipo(UsuarioTipo usuarioTipo) {
		this.usuarioTipo = usuarioTipo;
	}


    

    
    
    
    
    


}
