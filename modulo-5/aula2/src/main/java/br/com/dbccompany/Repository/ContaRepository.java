package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Conta;
import br.com.dbccompany.Entity.ContaTipo;
import br.com.dbccompany.Entity.UsuarioCliente;



public interface ContaRepository extends CrudRepository<Conta, Integer> {
	
	Conta findByNumero(String numero);
	List<Conta> findByContaTipo(ContaTipo contaTipo);
	Conta findByContasCliente(UsuarioCliente usuarioCliente);
	//Conta findByContasCliente(List<UsuarioCliente> usuarioCliente);
	List<Conta> findByContasCliente(List<UsuarioCliente> usuarioCliente);
	
	
	
}
