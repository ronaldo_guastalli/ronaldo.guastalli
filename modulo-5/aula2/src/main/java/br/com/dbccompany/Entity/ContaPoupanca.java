package br.com.dbccompany.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@PrimaryKeyJoinColumn(name="id_conta")
public class ContaPoupanca extends Conta {    
    
    @Column(name="deposito_mensal", nullable=false, precision=10, scale=2)
    private Double depositoMensal;
    
    @Column(name="data_aniversario", nullable=false, length=100)
    private String dataAniversario;
    
    @Column(name="taxa_rendimento", nullable=false, precision=10, scale=2)
    private Double taxaRendimento;

	public Double getDepositoMensal() {
		return depositoMensal;
	}

	public void setDepositoMensal(Double depositoMensal) {
		this.depositoMensal = depositoMensal;
	}

	public String getDataAniversario() {
		return dataAniversario;
	}

	public void setDataAniversario(String dataAniversario) {
		this.dataAniversario = dataAniversario;
	}

	public Double getTaxaRendimento() {
		return taxaRendimento;
	}

	public void setTaxaRendimento(Double taxaRendimento) {
		this.taxaRendimento = taxaRendimento;
	} 
    
    
  
}

