package br.com.dbccompany.Json;

import java.util.ArrayList;
import java.util.List;

import br.com.dbccompany.Entity.Banco;
import br.com.dbccompany.Entity.Conta;
import br.com.dbccompany.Entity.Endereco;

public class AgenciaJson {

    private Banco bancoJson;
    private String nomeJson;
    private String numero;
    private Endereco endereco;
    private List<Conta> contas = new ArrayList<>();
    
}
