package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="situacao_emprestimo")
public class SituacaoEmprestimo {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private Integer id;
	
	@Column(name="situacao")
	private String situacao;
	
	@OneToMany(mappedBy="situacaoEmprestimo")
	private List<Emprestimo> emprestimos = new ArrayList<>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSituacao() {
		return situacao;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	public List<Emprestimo> getEmprestimos() {
		return emprestimos;
	}

	public void pushEmprestimos(Emprestimo... emprestimos) {
		this.emprestimos.addAll(Arrays.asList(emprestimos));
	}
	
	

}
