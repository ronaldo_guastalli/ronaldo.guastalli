
package br.com.dbccompany.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "contato")
public class Contato {
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    @Column(name="valor")
    private String valor;
    
    @ManyToOne
    @JoinColumn(name = "id_usuario")
    private Usuario usuario;
    
    @ManyToOne
    @JoinColumn(name = "id_contato_tipo")
    private ContatoTipo contatoTipo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public ContatoTipo getContatoTipo() {
        return contatoTipo;
    }

    public void setContatoTipo(ContatoTipo contatoTipo) {
        this.contatoTipo = contatoTipo;
    }

    @Override
    public String toString() {
        return "Contato{" + "id=" + id + ", valor=" + valor + ", usuario=" + usuario + ", contatoTipo=" + contatoTipo + '}';
    } 
    
    
    
}
