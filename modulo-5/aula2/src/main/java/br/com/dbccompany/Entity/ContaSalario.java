package br.com.dbccompany.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@PrimaryKeyJoinColumn(name="id_conta")
public class ContaSalario extends Conta {    
    
	@Column(nullable = false, precision = 10, scale = 2)
    private Double salario;
	
	@Column(nullable = false)
	private boolean portabilidadeAutomatica;

	public Double getSalario() {
		return salario;
	}

	public void setSalario(Double salario) {
		this.salario = salario;
	}

	public boolean isPortabilidadeAutomatica() {
		return portabilidadeAutomatica;
	}

	public void setPortabilidadeAutomatica(boolean portabilidadeAutomatica) {
		this.portabilidadeAutomatica = portabilidadeAutomatica;
	}  
	
	
    
  
}

