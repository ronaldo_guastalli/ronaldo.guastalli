package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Banco;
import br.com.dbccompany.Repository.BancoRepository;

@Service
public class BancoService {

	@Autowired
	private BancoRepository bancoRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Banco salvar(Banco banco) {
		return bancoRepository.save(banco);
	}
	
	private Optional<Banco> buscarBanco(Integer id) {
		return bancoRepository.findById(id);
	}
	
//	private Optional<Banco> buscarBanco(Integer id) {
//		Optional<Banco> obj = bancoRepository.findById(id);
//		return obj.orElse(null);
//	}
	
	@Transactional(rollbackFor = Exception.class)
	public Banco editarBanco(Integer Id, Banco banco) {
		banco.setId(Id);
		return bancoRepository.save(banco);
	}
	
	public List<Banco> allBancos(){
		return (List<Banco>) bancoRepository.findAll();
	}
	
	public Banco buscarPorCodigo(long codigo) {
		return bancoRepository.findByCodigo(codigo);
	}
}


