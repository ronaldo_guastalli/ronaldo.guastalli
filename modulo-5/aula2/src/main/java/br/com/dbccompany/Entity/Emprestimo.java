package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="emprestimo")
public class Emprestimo {	
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private Integer id;
	
	@Column(name="nome")
	private String nome;
	
	@Column(name="valor", precision=2, nullable=false)
	private Double valor;
	
	@ManyToOne
	@JoinColumn(name="id_conta")
	private Conta conta;
	
	@ManyToOne
	@JoinColumn(name="id_situacao")
	private SituacaoEmprestimo situacaoEmprestimo;
	
	@OneToMany(mappedBy="emprestimo")
	private List<Movimentacao> movimentacoes = new ArrayList<>();
	
	@ManyToMany
	@JoinTable(name="aprovadores_emprestimo",
		joinColumns = {@JoinColumn(name="id_emprestimo")},
		inverseJoinColumns = {@JoinColumn(name="id_usuario_colaborador")})
	private List<UsuarioColaborador> usuarioAprovadores = new ArrayList<>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public Conta getConta() {
		return conta;
	}

	public void setConta(Conta conta) {
		this.conta = conta;
	}

	public SituacaoEmprestimo getSituacaoEmprestimo() {
		return situacaoEmprestimo;
	}

	public void setSituacaoEmprestimo(SituacaoEmprestimo situacaoEmprestimo) {
		this.situacaoEmprestimo = situacaoEmprestimo;
	}

	public List<UsuarioColaborador> getUsuarioAprovadores() {
		return usuarioAprovadores;
	}

	public void pushUsuarioAprovadores(UsuarioColaborador... usuarioAprovadores) {
		this.usuarioAprovadores.addAll(Arrays.asList(usuarioAprovadores));
	}

	public List<Movimentacao> getMovimentacoes() {
		return movimentacoes;
	}

	public void pushMovimentacoes(Movimentacao... movimentacoes) {
		this.movimentacoes.addAll(Arrays.asList(movimentacoes));
	}
	
	
	
	
	

}
