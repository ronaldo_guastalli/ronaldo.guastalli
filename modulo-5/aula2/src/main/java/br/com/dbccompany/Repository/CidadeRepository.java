package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Cidade;
import br.com.dbccompany.Entity.Estado;


public interface CidadeRepository extends CrudRepository<Cidade, Integer> {
	
	Cidade findByNome(String nome);
	Cidade findByEstado(Estado estado);		
	
	
	
	
}
