package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Agencia;
import br.com.dbccompany.Entity.Banco;
import br.com.dbccompany.Entity.Conta;
import br.com.dbccompany.Entity.Endereco;



public interface AgenciaRepository extends CrudRepository<Agencia, Integer> {
	
	List<Agencia> findByBanco(Banco banco);
	Agencia findByNome(String nome);
	Agencia findByNumero(String numero);
	Agencia findByContas(Conta conta);
	Agencia findByEndereco(Endereco endereco);
	
	
}
