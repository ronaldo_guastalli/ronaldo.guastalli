package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Agencia;
import br.com.dbccompany.Entity.Agencia;
import br.com.dbccompany.Repository.AgenciaRepository;

@Service
public class AgenciaService {

	@Autowired
	private AgenciaRepository agenciaRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Agencia salvar(Agencia agencia) {
		return (Agencia) agenciaRepository.save(agencia);
	}
	
	private Optional<Agencia> buscarAgnecia(Integer id) {
		return agenciaRepository.findById(id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Agencia editarAgencia(Integer Id, Agencia agencia) {
		agencia.setId(Id);
		return agenciaRepository.save(agencia);
	}
	
	public List<Agencia> allAgencias(){
		return (List<Agencia>) agenciaRepository.findAll();
	}
	
	public Agencia buscarPorNumero(String numero) {
		return agenciaRepository.findByNumero(numero);
	}

}


