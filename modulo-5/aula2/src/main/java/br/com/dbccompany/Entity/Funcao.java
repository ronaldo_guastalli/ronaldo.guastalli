package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "funcao")
public class Funcao {
	
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;
    
	@Column(name="nome")
    private String nome;
    
    @OneToMany(mappedBy="funcao")
    private List<UsuarioColaborador> usuarioColaboradores = new ArrayList<>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<UsuarioColaborador> getUsuarioColaboradores() {
		return usuarioColaboradores;
	}

	public void pushUsuarioColaboradores(UsuarioColaborador... usuarioColaboradores) {
		this.usuarioColaboradores.addAll(Arrays.asList(usuarioColaboradores));
	}
    
    

}
